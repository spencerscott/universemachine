#!/usr/bin/perl -w
use lib qw(perl ../perl ../../perl);
use GraphSetup;

my $scriptdir = $cwd.'/scripts';
my $paramsdir = "$dir/data/smhm/params";
opendir PARAMS, $paramsdir or die "Unable to open directory $paramsdir: $!!\n";
my @params = sort { tex_description($a) cmp tex_description($b) } grep { /^smhm/ } readdir PARAMS;
closedir PARAMS;

close STDOUT;

open STDOUT, ">$paperdir/smhm_fits.tex";

print '\small',"\n";
print '\begin{tabular}{cccc'.('c'x10).'}'."\n";
print '\hline'."\n";
print 'SM & Q/SF & Cen/Sat & IHL & $\epsilon_0$ & $\epsilon_a$ & $\epsilon_\mathrm{lna}$ & $\epsilon_z$ & $M_0$ & $M_a$ & $M_\mathrm{lna}$ & $M_z$ & $\alpha_0$ & $\alpha_a$ \\\\ '."\n".' & & & & $\alpha_\mathrm{lna}$ & $\alpha_z$ & $\beta_0$ & $\beta_a$ & $\beta_z$ & $\delta_0$ & $\gamma_0$ & $\gamma_a$ & $\gamma_z$ & $\chi^2$\\\\ '."\n";
#.'& & & & $\gamma_0$ & $\gamma_a$ & $\gamma_z$ & $\chi^2$\\\\ '."\n"
print '\hline'."\n";
my $seen_true = 0;
for my $p (@params) {
    if ($p =~ /true/ and !$seen_true) {
	$seen_true = 1;
	print '\hline'."\n";
    }
    my $t = tex_description($p);
    print $t, " ";
    system("python", "$scriptdir/gen_smhm.py", "tex", "$paramsdir/$p");
}
print '\hline'."\n";
#print "@params\n";
print '\end{tabular}'."\n";
print '\normalsize',"\n";

close STDOUT;

sub tex_description {
    my $fn = shift;
    my $csa = 'All';
    $csa = 'Cen.' if ($fn =~ /_cen_/);
    $csa = 'Sat.' if ($fn =~ /_sat_/);
    my $icl = 'Excl.';
    $icl = 'Incl.' if ($fn =~ /_icl_/);
    my $qsf = 'All';
    $qsf = 'Q' if ($fn =~ /_q_/);
    $qsf = 'SF' if ($fn =~ /_sf_/);
    $sm = 'Obs.';
    $sm = 'True' if ($fn =~ /_true_/);
    return "$sm & $qsf & $csa & $icl";
}
