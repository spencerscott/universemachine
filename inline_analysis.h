#ifndef _INLINE_ANALYSIS_H_
#define _INLINE_ANALYSIS_H_

#include <inttypes.h>

double _pp_calc_corr(int64_t i, int64_t thresh, int64_t subtype, int64_t rbin, double scale, int64_t post);
double _pp_calc_xcorr(int64_t i, int64_t thresh, int64_t subtype, int64_t rbin, double scale, int64_t post);
void inline_postprocessing(void);

#endif /* _INLINE_ANALYSIS_H_ */
