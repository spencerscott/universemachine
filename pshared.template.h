//shared(a,b)
//a = variable name
//b = size (in units of floats)

shared(smhm_scales, SMHM_NUM_ZS);
shared(wl_scales, NUM_WL_ZS);

shared(smhm, SMHM_NUM_ZS*SMHM_NBINS);
shared(smhm_cen, SMHM_NUM_ZS*SMHM_NBINS);
shared(smhm_sat, SMHM_NUM_ZS*SMHM_NBINS);
shared(smhm_sf, SMHM_NUM_ZS*SMHM_NBINS);
shared(smhm_q, SMHM_NUM_ZS*SMHM_NBINS);

shared(smhm_true, SMHM_NUM_ZS*SMHM_NBINS);
shared(smhm_true_cen, SMHM_NUM_ZS*SMHM_NBINS);
shared(smhm_true_sat, SMHM_NUM_ZS*SMHM_NBINS);
shared(smhm_true_sf, SMHM_NUM_ZS*SMHM_NBINS);
shared(smhm_true_q, SMHM_NUM_ZS*SMHM_NBINS);

shared(smhm_cen_sf, SMHM_NUM_ZS*SMHM_NBINS);
shared(smhm_cen_q, SMHM_NUM_ZS*SMHM_NBINS);
shared(smhm_true_cen_sf, SMHM_NUM_ZS*SMHM_NBINS);
shared(smhm_true_cen_q, SMHM_NUM_ZS*SMHM_NBINS);

shared(smhm_true_icl, SMHM_NUM_ZS*SMHM_NBINS);
shared(smhm_true_cen_icl, SMHM_NUM_ZS*SMHM_NBINS);


shared(smhm_med, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_scatter, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_cen, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_cen_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_cen_ratio, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_cen_bestfit_ratio, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_scatter_cen, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_sat, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_sat_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_sat_ratio, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_sat_bestfit_ratio, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_scatter_sat, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_sf, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_sf_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_sf_ratio, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_sf_bestfit_ratio, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_scatter_sf, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_q, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_q_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_q_ratio, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_q_bestfit_ratio, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_scatter_q, SMHM_NUM_ZS*SMHM_FIT_NBINS);

shared(smhm_true_med, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_med_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_scatter, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_med_cen, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_med_cen_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_scatter_cen, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_med_sat, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_med_sat_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_scatter_sat, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_med_sf, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_med_sf_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_scatter_sf, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_med_q, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_med_q_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_scatter_q, SMHM_NUM_ZS*SMHM_FIT_NBINS);

shared(smhm_med_cen_sf, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_cen_sf_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_scatter_cen_sf, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_cen_q, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_med_cen_q_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_scatter_cen_q, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_med_cen_sf, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_med_cen_sf_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_scatter_cen_sf, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_med_cen_q, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_med_cen_q_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_scatter_cen_q, SMHM_NUM_ZS*SMHM_FIT_NBINS);

shared(smhm_true_icl_med, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_icl_med_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_icl_scatter, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_cen_icl_med, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_cen_icl_med_bestfit, SMHM_NUM_ZS*SMHM_FIT_NBINS);
shared(smhm_true_cen_icl_scatter, SMHM_NUM_ZS*SMHM_FIT_NBINS);

shared(smhm_med_cen_sat_ratio, SMHM_NUM_ZS*SMHM_FIT_NBINS+1);
shared(smhm_med_sf_q_ratio, SMHM_NUM_ZS*SMHM_FIT_NBINS+1);
shared(smhm_med_bestfit_cen_sat_ratio, SMHM_NUM_ZS*SMHM_FIT_NBINS+1);
shared(smhm_med_bestfit_sf_q_ratio, SMHM_NUM_ZS*SMHM_FIT_NBINS+1);

#include "smhm_params.template.h"

shared(smhm_scatter_pdf, SMHM_NUM_ZS*SMHM_FIT_NBINS*SMHM_SMHM_NBINS);

shared(wl_pcounts, NUM_WL_THRESHES*NUM_WL_TYPES*NUM_WL_BINS*NUM_WL_ZS);
shared(wl_hcounts, NUM_WL_THRESHES*NUM_WL_TYPES*NUM_WL_ZS);
shared(wl_post, NUM_WL_THRESHES*NUM_WL_TYPES*NUM_WL_BINS*NUM_WL_ZS);
shared(wl_post_ratios, NUM_WL_THRESHES*NUM_WL_TYPES*NUM_WL_BINS*NUM_WL_ZS);

shared(ssfr_dist, SMHM_NUM_ZS*SSFR_NBINS);
shared(ssfr_dist_cen, SMHM_NUM_ZS*SSFR_NBINS);
shared(ssfr_dist_sat, SMHM_NUM_ZS*SSFR_NBINS);

shared(total_log_sm, num_scales*HM_M_NBINS);
shared(total_log_smhm_ratio, num_scales*HM_M_NBINS);
shared(total_log_true_sm, num_scales*HM_M_NBINS);
shared(total_log_true_sm_icl, num_scales*HM_M_NBINS);
shared(total_log_true_smhm_ratio, num_scales*HM_M_NBINS);
shared(total_log_true_smhm_icl_ratio, num_scales*HM_M_NBINS);
shared(mean_log_sm, num_scales*HM_M_NBINS);
shared(mean_log_smhm_ratio, num_scales*HM_M_NBINS);
shared(mean_log_true_sm, num_scales*HM_M_NBINS);
shared(mean_log_true_sm_icl, num_scales*HM_M_NBINS);
shared(mean_log_true_smhm_ratio, num_scales*HM_M_NBINS);
shared(mean_log_true_smhm_icl_ratio, num_scales*HM_M_NBINS);

shared(hostm_dist, num_scales*HM_M_NBINS);
shared(total_sm_all_enclosed, num_scales*HM_M_NBINS);
shared(mean_sm_all_enclosed, num_scales*HM_M_NBINS);
shared(total_sm_central_enclosed, num_scales*HM_M_NBINS);
shared(mean_sm_central_enclosed, num_scales*HM_M_NBINS);
shared(total_sm_icl_central_enclosed, num_scales*HM_M_NBINS);
shared(mean_sm_icl_central_enclosed, num_scales*HM_M_NBINS);

shared(hm_dist, num_scales*HM_M_NBINS);
shared(hm_cen_dist, num_scales*HM_M_NBINS);
shared(hm_sat_ratio, num_scales*HM_M_NBINS);
shared(hm_q, num_scales*HM_M_NBINS);
shared(hm_q_ratio, num_scales*HM_M_NBINS);
shared(hm_q_cen, num_scales*HM_M_NBINS);
shared(hm_q_sat, num_scales*HM_M_NBINS);
shared(hm_q_cen_ratio, num_scales*HM_M_NBINS);
shared(hm_q_sat_ratio, num_scales*HM_M_NBINS);
shared(hm_sat_mw, num_scales*HM_M_NBINS);
shared(hm_sat_gr, num_scales*HM_M_NBINS);
shared(hm_sat_cl, num_scales*HM_M_NBINS);
shared(hm_q_sat_mw, num_scales*HM_M_NBINS);
shared(hm_q_sat_gr, num_scales*HM_M_NBINS);
shared(hm_q_sat_cl, num_scales*HM_M_NBINS);
shared(hm_q_sat_mw_ratio, num_scales*HM_M_NBINS);
shared(hm_q_sat_gr_ratio, num_scales*HM_M_NBINS);
shared(hm_q_sat_cl_ratio, num_scales*HM_M_NBINS);
shared(hm_q_cen_sat_diff, num_scales*SM_RED_NBINS);
shared(hm_q_cen_sat_mw_diff, num_scales*SM_RED_NBINS);
shared(hm_q_cen_sat_gr_diff, num_scales*SM_RED_NBINS);
shared(hm_q_cen_sat_cl_diff, num_scales*SM_RED_NBINS);

shared(hm_fq_sat_mw_infall, num_scales*HM_M_NBINS);
shared(hm_fq_sat_gr_infall, num_scales*HM_M_NBINS);
shared(hm_fq_sat_cl_infall, num_scales*HM_M_NBINS);
shared(hm_fq_sat_mw_infall_ratio, num_scales*HM_M_NBINS);
shared(hm_fq_sat_gr_infall_ratio, num_scales*HM_M_NBINS);
shared(hm_fq_sat_cl_infall_ratio, num_scales*HM_M_NBINS);

shared(total_hm, num_scales*SM_RED_NBINS);
shared(total_log_hm, num_scales*SM_RED_NBINS);
shared(total_wl_hm, num_scales*SM_RED_NBINS);
shared(total_cen_hm, num_scales*SM_RED_NBINS);
shared(total_cen_log_hm, num_scales*SM_RED_NBINS);
shared(total_cen_wl_hm, num_scales*SM_RED_NBINS);
shared(total_cen_sf_hm, num_scales*SM_RED_NBINS);
shared(total_cen_sf_log_hm, num_scales*SM_RED_NBINS);
shared(total_cen_sf_wl_hm, num_scales*SM_RED_NBINS);
shared(total_cen_q_hm, num_scales*SM_RED_NBINS);
shared(total_cen_q_log_hm, num_scales*SM_RED_NBINS);
shared(total_cen_q_wl_hm, num_scales*SM_RED_NBINS);
shared(total_sat_hm, num_scales*SM_RED_NBINS);
shared(total_sat_log_hm, num_scales*SM_RED_NBINS);
shared(total_sat_wl_hm, num_scales*SM_RED_NBINS);
shared(total_sf_hm, num_scales*SM_RED_NBINS);
shared(total_sf_log_hm, num_scales*SM_RED_NBINS);
shared(total_sf_wl_hm, num_scales*SM_RED_NBINS);
shared(total_q_hm, num_scales*SM_RED_NBINS);
shared(total_q_log_hm, num_scales*SM_RED_NBINS);
shared(total_q_wl_hm, num_scales*SM_RED_NBINS);
shared(mean_hm, num_scales*SM_RED_NBINS);
shared(mean_log_hm, num_scales*SM_RED_NBINS);
shared(mean_wl_hm, num_scales*SM_RED_NBINS);
shared(mean_cen_hm, num_scales*SM_RED_NBINS);
shared(mean_cen_log_hm, num_scales*SM_RED_NBINS);
shared(mean_cen_wl_hm, num_scales*SM_RED_NBINS);
shared(mean_cen_sf_hm, num_scales*SM_RED_NBINS);
shared(mean_cen_sf_log_hm, num_scales*SM_RED_NBINS);
shared(mean_cen_sf_wl_hm, num_scales*SM_RED_NBINS);
shared(mean_cen_q_hm, num_scales*SM_RED_NBINS);
shared(mean_cen_q_log_hm, num_scales*SM_RED_NBINS);
shared(mean_cen_q_wl_hm, num_scales*SM_RED_NBINS);
shared(mean_sat_hm, num_scales*SM_RED_NBINS);
shared(mean_sat_log_hm, num_scales*SM_RED_NBINS);
shared(mean_sat_wl_hm, num_scales*SM_RED_NBINS);
shared(mean_sf_hm, num_scales*SM_RED_NBINS);
shared(mean_sf_log_hm, num_scales*SM_RED_NBINS);
shared(mean_sf_wl_hm, num_scales*SM_RED_NBINS);
shared(mean_q_hm, num_scales*SM_RED_NBINS);
shared(mean_q_log_hm, num_scales*SM_RED_NBINS);
shared(mean_q_wl_hm, num_scales*SM_RED_NBINS);


shared(sm_red, num_scales*SM_RED_NBINS);
shared(sm_red_q2, num_scales*SM_RED_NBINS);
shared(sm_cen, num_scales*SM_RED_NBINS);
shared(sm_sat_ratio, num_scales*SM_RED_NBINS);
shared(sm_fq, num_scales*SM_RED_NBINS);
shared(sm_fq2, num_scales*SM_RED_NBINS);
shared(sm_fq3, num_scales*SM_RED_NBINS);
shared(sm_ssfr, num_scales*SM_RED_NBINS);
shared(sm_q_cen, num_scales*SM_RED_NBINS);
shared(sm_q_sat, num_scales*SM_RED_NBINS);
shared(sm_q_cen_ratio, num_scales*SM_RED_NBINS);
shared(sm_q_sat_ratio, num_scales*SM_RED_NBINS);
shared(sm_sat_mw, num_scales*SM_RED_NBINS);
shared(sm_sat_gr, num_scales*SM_RED_NBINS);
shared(sm_sat_cl, num_scales*SM_RED_NBINS);
shared(sm_q_sat_mw, num_scales*SM_RED_NBINS);
shared(sm_q_sat_gr, num_scales*SM_RED_NBINS);
shared(sm_q_sat_cl, num_scales*SM_RED_NBINS);
shared(sm_q_sat_mw_ratio, num_scales*SM_RED_NBINS);
shared(sm_q_sat_gr_ratio, num_scales*SM_RED_NBINS);
shared(sm_q_sat_cl_ratio, num_scales*SM_RED_NBINS);
shared(sm_q_cen_sat_diff, num_scales*SM_RED_NBINS);
shared(sm_q_cen_sat_mw_diff, num_scales*SM_RED_NBINS);
shared(sm_q_cen_sat_gr_diff, num_scales*SM_RED_NBINS);
shared(sm_q_cen_sat_cl_diff, num_scales*SM_RED_NBINS);

shared(sm_fq_sat_mw_infall, num_scales*SM_RED_NBINS);
shared(sm_fq_sat_gr_infall, num_scales*SM_RED_NBINS);
shared(sm_fq_sat_cl_infall, num_scales*SM_RED_NBINS);
shared(sm_fq_sat_mw_infall_ratio, num_scales*SM_RED_NBINS);
shared(sm_fq_sat_gr_infall_ratio, num_scales*SM_RED_NBINS);
shared(sm_fq_sat_cl_infall_ratio, num_scales*SM_RED_NBINS);

shared(hm_sfh, num_scales*num_scales*HM_M_NBINS);
shared(hm_sfh_sf, num_scales*num_scales*HM_M_NBINS);
shared(hm_sfh_q, num_scales*num_scales*HM_M_NBINS);
shared(hm_sfh_cen, num_scales*num_scales*HM_M_NBINS);
shared(hm_sfh_sat, num_scales*num_scales*HM_M_NBINS);

shared(hm_sfh_ratio, num_scales*num_scales*HM_M_NBINS);
shared(hm_sfh_sf_ratio, num_scales*num_scales*HM_M_NBINS);
shared(hm_sfh_q_ratio, num_scales*num_scales*HM_M_NBINS);
shared(hm_sfh_cen_ratio, num_scales*num_scales*HM_M_NBINS);
shared(hm_sfh_sat_ratio, num_scales*num_scales*HM_M_NBINS);

shared(sm_sfh, num_scales*num_scales*SM_RED_NBINS);
shared(sm_sfh_sf, num_scales*num_scales*SM_RED_NBINS);
shared(sm_sfh_q, num_scales*num_scales*SM_RED_NBINS);
shared(sm_sfh_cen, num_scales*num_scales*SM_RED_NBINS);
shared(sm_sfh_sat, num_scales*num_scales*SM_RED_NBINS);

shared(sm_sfh_ratio, num_scales*num_scales*SM_RED_NBINS);
shared(sm_sfh_sf_ratio, num_scales*num_scales*SM_RED_NBINS);
shared(sm_sfh_q_ratio, num_scales*num_scales*SM_RED_NBINS);
shared(sm_sfh_cen_ratio, num_scales*num_scales*SM_RED_NBINS);
shared(sm_sfh_sat_ratio, num_scales*num_scales*SM_RED_NBINS);

shared(sm_td_sat_mw_infall, num_scales*TS_NBINS*SM_RED_NBINS);
shared(sm_td_sat_gr_infall, num_scales*TS_NBINS*SM_RED_NBINS);
shared(sm_td_sat_cl_infall, num_scales*TS_NBINS*SM_RED_NBINS);
shared(sm_td_sat_mw_infall_med, num_scales*SM_RED_NBINS*3);
shared(sm_td_sat_gr_infall_med, num_scales*SM_RED_NBINS*3);
shared(sm_td_sat_cl_infall_med, num_scales*SM_RED_NBINS*3);

shared(sm_ti_sat_mw_infall, num_scales*num_scales*SM_RED_NBINS);
shared(sm_ti_sat_gr_infall, num_scales*num_scales*SM_RED_NBINS);
shared(sm_ti_sat_cl_infall, num_scales*num_scales*SM_RED_NBINS);
shared(sm_ti_sat_mw_infall_med, num_scales*SM_RED_NBINS*3);
shared(sm_ti_sat_gr_infall_med, num_scales*SM_RED_NBINS*3);
shared(sm_ti_sat_cl_infall_med, num_scales*SM_RED_NBINS*3);

shared(sm_ssfr_sat_mw_infall, SMHM_NUM_ZS*SSFR_NBINS);
shared(sm_ssfr_sat_gr_infall, SMHM_NUM_ZS*SSFR_NBINS);
shared(sm_ssfr_sat_cl_infall, SMHM_NUM_ZS*SSFR_NBINS);
shared(sm_ssfr_sat_mw_infall_med, SMHM_NUM_ZS*SM_RED_NBINS*3);
shared(sm_ssfr_sat_gr_infall_med, SMHM_NUM_ZS*SM_RED_NBINS*3);
shared(sm_ssfr_sat_cl_infall_med, SMHM_NUM_ZS*SM_RED_NBINS*3);


shared(sm_mp_q_qf, num_scales*SM_RED_NBINS);
shared(sm_mp_sf_qf, num_scales*SM_RED_NBINS);
shared(sm_mp_q_counts, num_scales*SM_RED_NBINS);
shared(sm_mp_sf_counts, num_scales*SM_RED_NBINS);
shared(sm_mp_q_qf_ratio, num_scales*SM_RED_NBINS);
shared(sm_mp_sf_qf_ratio, num_scales*SM_RED_NBINS);

shared(mp_q_qf, num_scales*HM_M_NBINS);
shared(mp_sf_qf, num_scales*HM_M_NBINS);
shared(mp_q_counts, num_scales*HM_M_NBINS);
shared(mp_sf_counts, num_scales*HM_M_NBINS);
shared(mp_q_qf_ratio, num_scales*HM_M_NBINS);
shared(mp_sf_qf_ratio, num_scales*HM_M_NBINS);

shared(hm_ex_situ, num_scales*HM_M_NBINS);
shared(hm_ex_situ_ratio, num_scales*HM_M_NBINS);
shared(sm_ex_situ, num_scales*SM_RED_NBINS);
shared(sm_ex_situ_ratio, num_scales*SM_RED_NBINS);

shared(hm_rejuv, num_scales*HM_M_NBINS);
shared(sm_rejuv, num_scales*SM_RED_NBINS);
shared(hm_rejuv_ratio, num_scales*HM_M_NBINS);
shared(sm_rejuv_ratio, num_scales*SM_RED_NBINS);


shared(fcorrs_post, num_scales*NUM_CORR_BINS_PER_STEP);
shared(fxcorrs_post, num_scales*NUM_XCORR_BINS_PER_STEP);
shared(fcounts_post, num_scales*NUM_CORR_TYPES*NUM_CORR_THRESHES);
shared(cfs_post, num_scales*NUM_CORR_BINS_PER_STEP);
shared(xcfs_post, num_scales*NUM_XCORR_BINS_PER_STEP);
shared(cf_ratios_post, num_scales*NUM_CORR_BINS_PER_STEP);

shared(true_csfr, num_scales);
shared(uv_csfr_ratio, num_scales);

shared(uv_sfr_counts, num_scales*UV_SFR_NBINS);
shared(uv_sfr_tot_sfr, num_scales*UV_SFR_NBINS);
shared(uv_sfr_av_sfr, num_scales*UV_SFR_NBINS);
shared(uv_sfr_17, num_scales);
