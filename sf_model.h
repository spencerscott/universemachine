#ifndef _SF_MODEL_H_
#define _SF_MODEL_H_
#include <inttypes.h>

#define OBS_SCATTER_SFR_SF 0.3
#define Q_SSFR -11.8
#define OBS_SCATTER_SFR_Q 0.42

#define RANK_LVMP 2.5

#define KAPPA_PRIOR 0.24
#define MU_PRIOR 0.14
#define MU_A_PRIOR 0.24
#define SCATTER_PRIOR 0.03
#define SCATTER_CENTER 0.21

#define SIGMA_CENTER 0.07
#define SIGMA_Z_CENTER 0.05
#define SIGMA_Z_PRIOR 0.015
#define MAX_OBS_SM_SIG 0.3

#define DUST_SCALING 2.0
#define ICL_DIST_TOL 0.05

#define ORPHAN_THRESH1 2.5   //Log10(Vmax@Mpeak)
#define ORPHAN_THRESH2 3.0   //Log10(Vmax@Mpeak)
#define ORPHAN_LVMP (0.5*(ORPHAN_THRESH1+ORPHAN_THRESH2))
#define ORPHAN_LVMP_WIDTH (0.5*(ORPHAN_THRESH2-ORPHAN_THRESH1))



struct sf_model {
  float v_1, alpha, beta, gamma, delta, epsilon; //SFR(Vmp)
  float ssfr_q, sig_q, sig_sf, sig_sf_uncorr; //Dispersion in SFR
  float r_cen, r_width, r_min, ra, old_ra; //Rank conversions
  float fq_min, q_lvmp, q_sig_lvmp;  //Quenched fractions
  float icl_dist;
  float obs_sm_sig, obs_sfr_sig; //Nuisance params
  float obs_sm_offset, obs_sfr_offset;
  float obs_sm_offset_lin, obs_sfr_offset_lin;
  float obs_sm_scatter_corr, obs_sfr_scatter_corr;
  float obs_ssfr_scatter, obs_ssfr_scatter_corr;
  float obs_min_corr_sm;
  float dust_m, dust_w;
  float uv_dust_thresh, uv_dust_slope;
  float orphan_thresh_min, orphan_thresh_change;
  float moustakas_constant;
};

#define NUM_PARAMS 46

struct sf_model_allz {
  double params[NUM_PARAMS+2];
};

struct sf_model calc_sf_model(struct sf_model_allz f, double a);
int64_t test_invalid(struct sf_model_allz m);
double scatter_correction(double sigma);
void assert_model_no_systematics(struct sf_model_allz *a);
void assert_model_default(struct sf_model_allz *a);

#define EFF_0(x)     ((x).params[0])
#define EFF_0_A(x)   ((x).params[1])
#define EFF_0_A2(x)  ((x).params[2])
#define EFF_0_A3(x)  ((x).params[3])
#define V_1(x)       ((x).params[4])
#define V_1_A(x)     ((x).params[5])
#define V_1_A2(x)    ((x).params[6])
#define V_1_A3(x)    ((x).params[7])
#define ALPHA(x)     ((x).params[8])
#define ALPHA_A(x)   ((x).params[9])
#define ALPHA_A2(x)  ((x).params[10])
#define ALPHA_A3(x)  ((x).params[11])
#define BETA(x)      ((x).params[12])
#define BETA_A(x)    ((x).params[13])
#define BETA_A2(x)   ((x).params[14])
#define DELTA(x)     ((x).params[15])
#define GAMMA(x)     ((x).params[16])
#define GAMMA_A(x)   ((x).params[17])
#define GAMMA_A2(x)  ((x).params[18])

#define INTR_SCATTER_SF(x)   ((x).params[19])
#define INTR_SCATTER_SF_A(x) ((x).params[20])
#define INTR_SCATTER_SF_UNCORR(x)  ((x).params[21])

#define R_CENTER(x)        ((x).params[22])
#define R_CENTER_A(x)      ((x).params[23])
#define R_WIDTH(x)         ((x).params[24])
#define R_MIN(x)           ((x).params[25])
#define RDECAY(x)          ((x).params[26])

#define Q_LVMP(x)        ((x).params[27])
#define Q_LVMP_A(x)      ((x).params[28])
#define Q_LVMP_Z(x)      ((x).params[29])
#define Q_SIG_LVMP(x)    ((x).params[30])
#define Q_SIG_LVMP_A(x)  ((x).params[31])
#define Q_SIG_LVMP_Z(x)  ((x).params[32])
#define Q_MIN(x)         ((x).params[33])
#define Q_MIN_A(x)       ((x).params[34])

#define ICL_DIST(x)      ((x).params[35])

#define OBS_SM_SIG(x)    ((x).params[36])
#define OBS_SM_SIG_Z(x)  ((x).params[37])
#define MU(x)            ((x).params[38])
#define MU_A(x)          ((x).params[39])
#define KAPPA_A(x)       ((x).params[40])
#define DUST_M(x)        ((x).params[41])
#define DUST_M_Z(x)      ((x).params[42])
#define DUST_WIDTH(x)    ((x).params[43])

#define ORPHAN_THRESH_V1(x) ((x).params[44])
#define ORPHAN_THRESH_V2(x) ((x).params[45])

#define INVALID(x)   ((x).params[46])
#define CHI2(x)      ((x).params[47])
#ifdef __APPLE__
#define INVALIDATE(x,y) { INVALID(*x) = 1;  fprintf(stderr, "%s\n", y); }
#else
#define INVALIDATE(x,y) { INVALID(*x) = 1; }
#endif


#endif /* _MAKE_SF_CATALOG_H_ */
