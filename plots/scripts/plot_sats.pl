#!/usr/bin/perl -w
use lib qw(../perl ../../perl perl);
use GraphSetup;

our $datadir = "$dir/data/qfs";
our $datadir_infall = "$dir/data/infall_stats";
our $plotdir = "$dir/graphs/sats";
File::Path::make_path($plotdir, "$plotdir/AGR", "$plotdir/EPS", "$plotdir/PNG", "$plotdir/PDF");

my @zs = (0.1, 1, 2);
my @scales = get_scales_at_redshifts("$datadir", "qf_hm", @zs);
my %zs = map { $scales[$_] => $zs[$_] } (0..$#zs);
open OUT, ">$plotdir/scales.txt";
print OUT "#Redshift Scale_Factor_Of_Snapshot_Used Actual_Redshift\n";
print OUT "$zs[$_] $scales[$_] ", 1/$scales[$_]-1,"\n" for (0..$#zs);
close OUT;

sub exclude_zeros {
    my $type = shift;
    return sub {
	my @a = split(" ", $_[0]);
	my $column = $type + 13;
	return "" if ($a[$column] < 5);
	my ($sm, $v, $u, $d) = (10**$a[0], map { $a[$_+$type*3] } (1..3));
	return "$sm $v $u $d";
    };
}

sub exclude_zeros_qf_infall {
    my $type = shift;
    return sub {
	my @a = split(" ", $_[0]);
	my $column = $type + 17;
	return "" if ($a[$column] < 5);
	my ($sm, $v, $u, $d) = (10**$a[0], map { $a[$_+3+$type*3] } (1..3));
	return "$sm $v $u $d";
    };
}


sub exclude_zeros_qf {
    my $type = shift;
    return sub {
	my @a = split(" ", $_[0]);
	my $column = $type + 19;
	return "" if ($a[$column] < 5);
	my ($sm, $v, $u, $d) = (10**$a[0], map { $a[$_+$type*3] } (1..3));
	return "$sm $v $u $d";
    };
}

sub exclude_zeros_infall {
    my ($type1, $type2, $ssfr, $offset) = @_;
    $type2 = ($type2) ? 3 : 1;  
    return sub {
	my @a = split(" ", $_[0]);
	my $column = $type1 + 31;
	return "" if ($a[$column] < 5);
	my ($sm, $v, $u, $d) = (10**$a[0], map { $a[1+$_*$type2+$type1*9] } (0..2));
	if ($type2==3) { $u -= $v; $d = $v-$d; }
	if (defined $offset) {
	    $sm *= 10**(($offset-1)/30);
	}
	return XmGrace::tolin(1..3)->("$sm $v $u $d") if ($ssfr);
	return "$sm $v $u $d";
    };
}

for my $a (@scales) {
    my $fn = sprintf("fq_infall_z%g", $zs{$a});
    $fn =~ tr/./_/;
    my @fcolors = (9,10,8);
    my @colors = (3,4,2);
    my @lstyles = (2,3,5);
    my @legends = ($mw_sats, $gr_sats, $cl_sats);
    my @cs_legends = ("Centrals", "Satellites");
    my @cs_colors = (3,2);
    my @cs_fcolors = (9,8);
    new XmGrace(xlabel => $sm_label,
		ylabel => 'Fraction of Quenched Satellites\n        Quenched after Infall',
		legendxy => "0.3, 0.3",
		XmGrace::logscale('x', 1e9, 3e11),
		XmGrace::linscale('y', 0, 1, 0.2, 1),
		fading_colors => 1,
		transparent_colors => $hq,
		text => [{text => "z = $zs{$a}\\n",  pos => "1.1, 0.8"}],
		data => [(map {{data => "file:$datadir/qf_groupstats_infall_a$a.dat", metatype => "errorstrip", fcolor => $fcolors[$_], transform => exclude_zeros($_), smooth => 1}} reverse (0..2)),
			 (map {{data => "file:$datadir/qf_groupstats_infall_a$a.dat", lcolor => $colors[$_], legend => $legends[$_], smooth => 1, transform => exclude_zeros($_)}} reverse (0..2)),
		])->write_pdf("$plotdir/AGR/$fn.agr");

    $fn = sprintf("fq_cen_sat_z%g", $zs{$a});
    $fn =~ tr/./_/;
    new XmGrace(xlabel => $sm_label,
		ylabel => 'Quenched Fraction',
		legendxy => "0.9, 0.3",
		XmGrace::logscale('x', 1e9, 3e11),
		XmGrace::linscale('y', 0, 1, 0.2, 1),
		fading_colors => 1,
		transparent_colors => 1,
		text => [{text => "z = $zs{$a}\\n",  pos => "0.3, 0.8"}],
		data => [(map {{data => "file:$datadir/qf_groupstats_a$a.dat", metatype => "errorstrip", fcolor => $cs_fcolors[$_], transform => exclude_zeros_qf($_), smooth => 1}} reverse (0..1)),
			 (map {{data => "file:$datadir/qf_groupstats_a$a.dat", lcolor => $cs_colors[$_], legend => $cs_legends[$_], smooth => 1, transform => exclude_zeros_qf($_)}} reverse (0..1)),
		])->write_pdf("$plotdir/AGR/$fn.agr");

    $fn = sprintf("fq_hm_cen_sat_z%g", $zs{$a});
    $fn =~ tr/./_/;
    new XmGrace(xlabel => $hm_label,
		ylabel => 'Quenched Fraction',
		legendxy => "0.9, 0.8",
		XmGrace::logscale('x', 1e10, 1e15),
		XmGrace::linscale('y', 0, 1, 0.2, 1),
		fading_colors => 1,
		transparent_colors => 1,
		text => [{text => "z = $zs{$a}\\n",  pos => "0.3, 0.8"}],
		data => [(map {{data => "file:$datadir/qf_hm_groupstats_a$a.dat", metatype => "errorstrip", fcolor => $cs_fcolors[$_], transform => exclude_zeros_qf($_), smooth => 1}} reverse (0..1)),
			 (map {{data => "file:$datadir/qf_hm_groupstats_a$a.dat", lcolor => $cs_colors[$_], legend => $cs_legends[$_], smooth => 1, transform => exclude_zeros_qf($_)}} reverse (0..1)),
		])->write_pdf("$plotdir/AGR/$fn.agr");
    
    
    if ($zs{$a}<1) {
	$fn = sprintf("fq_tdelay_z%g", $zs{$a});
	$fn =~ tr/./_/;
	new XmGrace(xlabel => $sm_label,
		    ylabel => 'Average Quenching Delay Time\n        After Infall [Gyr]',
		    legendxy => "0.3, 0.82",
		    legend2xy => "0.3, 0.69",
		    legend3xy => "0.3, 0.708",
		    legend3vg => "0.007",
		    XmGrace::logscale('x', 1e9, 3e11),
		    XmGrace::linscale('y', 0, 11, 1, 1),
		    transparent_colors => $hq,
		    fading_colors => 1,
		    text => [{text => "z = $zs{$a}\\n",  pos => "1.1, 0.8"},
#			     {text => '1-\xs\f{} dispersion for individual galaxies', pos => "0.27, 0.195", color => 15, rot => 356}
		    ],
		    data => [ (map{{data => "file:$datadir_infall/quenching_delay_times_a$a.dat", metatype => "errorstrip", lstyle => $lstyles[$_], ftype => ($_==0) ? 1 : 0, fcolor => ($_==0) ? 15 : 0, lcolor => $fcolors[$_], lwidth => 2, transform => exclude_zeros_infall($_,1), smooth => 1}} (0..2)),
			     (map {{data => "file:$datadir_infall/quenching_delay_times_a$a.dat", metatype => "errorstrip", smooth => 1, fcolor => $fcolors[$_], transform => exclude_zeros_infall($_,0)}} reverse (0..2)),
			      (map {{data => "file:$datadir_infall/quenching_delay_times_a$a.dat", lcolor => $colors[$_], smooth => 1, transform => exclude_zeros_infall($_,0), legend => $legends[$_]}} reverse (0..2)),
			      {data => "0 0", lwidth => 8, lcolor => 15, legend2 => '\h{-0.1}\v{0.2}16\S\v{-0.3}th\N\v{0.2}-84\S\v{-0.3}th\N\v{0.2} percentile range for individual galaxies'},  #'\h{-0.1}\v{0.2}1-\xs\f{} dispersions for individual galaxies'},
			      {data => "0 0", lwidth => 2, lcolor => $fcolors[2], lstyle => $lstyles[2], legend3 => ' '},
			      {data => "0 0", lwidth => 2, lcolor => $fcolors[1], lstyle => $lstyles[1], legend3 => ' '},
		    ])->write_pdf("$plotdir/AGR/$fn.agr");

	$fn = sprintf("fq_tinfall_z%g", $zs{$a});
	$fn =~ tr/./_/;
	my ($l3x, $l3y) = #(0.27, 0.745);
	    (0.27, 0.220);
	new XmGrace(xlabel => $sm_label,
		    ylabel => 'Average Time Since Infall [Gyr]',
		    legendxy => "0.27, 0.83",
		    legend2xy => "0.7, 0.796",
		    legend3xy => "$l3x, $l3y",
		    XmGrace::logscale('x', 1e9, 3e11),
		    XmGrace::linscale('y', 0, 11, 1, 1),
		    transparent_colors => $hq,
		    fading_colors => 1,
		    text => [{text => "z = $zs{$a}\\n",  pos => "1.1, 0.8"},
#			     {text => '1-\xs\f{} dispersion for individual galaxies', pos => "0.27, 0.2", color => 15}
		    ],
		    lines => [{pos => join(",", $l3x+0.013, $l3y-0.02, $l3x+0.093, $l3y-0.02), color => $fcolors[1], width => 2, style => $lstyles[1]},
			      {pos => join(",", $l3x+0.013, $l3y-0.013, $l3x+0.093, $l3y-0.013), color => $fcolors[2], width => 2, style => $lstyles[2]},
		    ],
		    data => [
			(map {{data => "file:$datadir_infall/infall_delay_times_a$a.dat", metatype => "errorstrip", lstyle => $lstyles[$_], ftype => ($_==0) ? 1 : 0, fcolor => ($_==0) ? 15 : 0, lcolor => $fcolors[$_], lwidth => 2, transform => exclude_zeros_infall($_,1), smooth => 1}} (0..2)),
			     (map {{data => "file:$datadir_infall/infall_delay_times_a$a.dat", metatype => "errorstrip", smooth => 1, fcolor => $fcolors[$_], transform => exclude_zeros_infall($_,0)}} reverse (0..2)),
			(map {{data => "file:$datadir_infall/infall_delay_times_a$a.dat", lcolor => $colors[$_], smooth => 1, transform => exclude_zeros_infall($_,0), ($_>0) ? "legend" : "legend2",  $legends[$_]}} reverse (0..2)),
			{data => "0 0", lwidth => 8, lcolor => 15, legend3 => '\h{-0.1}\v{0.2}16\S\v{-0.3}th\N\v{0.2}-84\S\v{-0.3}th\N\v{0.2} percentile range for individual galaxies'},
#			{data => "0 0", lwidth => 2, lcolor => $fcolors[2], lstyle => $lstyles[2], legend3 => ' '},
#			{data => "0 0", lwidth => 2, lcolor => $fcolors[1], lstyle => $lstyles[1], legend3 => ' '},

#'1-\xs\f{} dispersions for individual galaxies'

		    ])->write_pdf("$plotdir/AGR/$fn.agr");

	$fn = sprintf("fq_infall_ssfr_z%g", $zs{$a});
	$fn =~ tr/./_/;
	new XmGrace(xlabel => $sm_label,
		    ylabel => 'Average SSFR at Infall [yr\S-1\N]',
		    legendxy => "0.3, 0.35",
		    legend2xy => "0.3, 0.22",
		    legend3xy => "0.3, 0.238",
		    legend3vg => "0.007",
		    XmGrace::logscale('x', 1e9, 3e11),
		    XmGrace::logscale('y', 1e-13, 2e-9),
		    transparent_colors => $hq,
		    fading_colors => 1,
		    text => [{text => "z = $zs{$a}\\n",  pos => "1.1, 0.8"},
			     #{text => '1-\xs\f{} dispersion for individual galaxies', pos => "0.27, 0.67", color => 15, rot => 2}
		    ],
		    data => [(map {{data => "file:$datadir_infall/infall_ssfrs_a$a.dat", metatype => "errorstrip",  lstyle => $lstyles[$_], ftype => ($_==0) ? 1 : 0, fcolor => ($_==0) ? 15 : 0, lcolor => $fcolors[$_], lwidth => 2, transform => exclude_zeros_infall($_,1,1), smooth => 1}} (0..2)),
			     (map {{data => "file:$datadir_infall/infall_ssfrs_a$a.dat", metatype => "errorstrip", smooth => 1, fcolor => $fcolors[$_], transform => exclude_zeros_infall($_,0,1)}} reverse (0..2)),
			     (map {{data => "file:$datadir_infall/infall_ssfrs_a$a.dat", lcolor => $colors[$_], smooth => 1, transform => exclude_zeros_infall($_,0,1), legend => $legends[$_]}} reverse (0..2)),
			     {data => "0 0", lwidth => 8, lcolor => 15, legend2 => '\h{-0.1}\v{0.2}16\S\v{-0.3}th\N\v{0.2}-84\S\v{-0.3}th\N\v{0.2} percentile range for individual galaxies'}, #'\h{-0.1}\v{0.2}1-\xs\f{} dispersions for individual galaxies'},
			     {data => "0 0", lwidth => 2, lcolor => $fcolors[2], lstyle => $lstyles[2], legend3 => ' '},
			     {data => "0 0", lwidth => 2, lcolor => $fcolors[1], lstyle => $lstyles[1], legend3 => ' '},
		    ])->write_pdf("$plotdir/AGR/$fn.agr");
    }

    $fn = sprintf("fq_quenched_infall_z%g", $zs{$a});
    $fn =~ tr/./_/;
    new XmGrace(xlabel => $sm_label,
		ylabel => 'Fraction of Satellites Quenched\n     Due to Infall (f\sq,sat\N - f\sq,cen\N)',
		legendxy => "0.3, 0.8",
		XmGrace::logscale('x', 1e9, 3e11),
		XmGrace::linscale('y', 0, 0.5, 0.1, 1),
		fading_colors => 1,
		transparent_colors => $hq,
		text => [{text => "z = $zs{$a}\\n",  pos => "1.1, 0.8"}],
		data => [(map {{data => "file:$datadir/qf_quenched_infall_a$a.dat", metatype => "errorstrip", fcolor => $fcolors[$_], transform => exclude_zeros_qf_infall($_), smooth => 1}} reverse (0..2)),
			 (map {{data => "file:$datadir/qf_quenched_infall_a$a.dat", lcolor => $colors[$_], legend => $legends[$_], smooth => 1, transform => exclude_zeros_qf_infall($_)}} reverse (0..2)),
		])->write_pdf("$plotdir/AGR/$fn.agr");
}

MultiThread::WaitForChildren();

if (-d $paperdir and -d "$paperdir/autoplots") {
    my @files = <$plotdir/PDF/fq_*.pdf>;
    system("cp", @files, "$paperdir/autoplots");
}
