#!/usr/bin/perl -w
use lib qw(../perl ../../perl perl);
use GraphSetup;

our $datadir = "$dir/data/smhm/median_raw";
our $plotdir = "$dir/graphs/scatter";
File::Path::make_path($plotdir, "$plotdir/AGR", "$plotdir/EPS", "$plotdir/PNG", "$plotdir/PDF");


my @zs = (0, 1, 2, 4);
my @scales = get_scales_at_redshifts("$datadir", "smhm", @zs);
my %zs = map { $scales[$_] => $zs[$_] } (0..$#zs);
open OUT, ">$plotdir/scales.txt";
print OUT "#Redshift Scale_Factor_Of_Snapshot_Used Actual_Redshift\n";
print OUT "$zs[$_] $scales[$_] ", 1/$scales[$_]-1,"\n" for (0..$#zs);
close OUT;

my $reddick13_data = <<'END_OF_DATA'
12.218248605748606 0.20957671957671958 0.022 0.023
12.448575718575718 0.19 0.04 0.04
12.941925675675677 0.1997883597883598 0.036 0.034
13.422575343200343 0.17981481481481482 0.028 0.028
14.231736111111111 0.1896031746031746 0.05 0.05
END_OF_DATA
    ;

sub exclude_zeros {
    my $z = shift;
    return sub {
	my @a = split(" ", $_[0]);
	return "" unless ($a[1]>0 and $a[0] < 14.6 and $a[0] > 10.2+$z/5 and $a[2]>0);
	$a[0] = 10**$a[0];    
	return "@a";
    }
}
sub exclude_zeros_sats {
    my $z = shift;
    return sub {
	my @a = split(" ", $_[0]);
	return "" unless ($a[1]>0 and $a[0] < 14.4 and $a[0] > 10.2+$z/5 and $a[2]>0);
	$a[0] = 10**$a[0];    
	return "@a";
    }
}

my $fn = sprintf("scatter_all");
$fn =~ tr/./_/;
@colors = (1,2,4,3);
@fcolors = (17,18,20,19);
new XmGrace(xlabel => $hm_label,
	    ylabel => $smscatter_label,
	    XmGrace::logscale('x', 1e10, 1e15),
	    XmGrace::linscale('y', 0, 0.5, 0.1, 1),
	    color_errorstrip => 1,
	    #transparent_colors => 1,
	    legendxy => "0.3, 0.34",
	    data => [(map {{data => "file[0,22,23,24]:$datadir/smhm_scatter_a$scales[$_].dat", metatype => "errorstrip", transform => exclude_zeros($_), fcolor => $fcolors[$_], smooth => 1}} reverse (0)),
		     (map {{data => "file[0,22,23,24]:$datadir/smhm_scatter_a$scales[$_].dat", legend => "z = $zs[$_]", type => ($_>0) ? "xydydy" : "xy", sskip => 1, transform => exclude_zeros($_), lcolor => $colors[$_], smooth => 1}} (0..3))
	    ])->write_pdf("$plotdir/AGR/$fn.agr");


$fn = sprintf("scatter_cen");
$fn =~ tr/./_/;
@colors = (1,2,4,3);
@fcolors = (17,18,20,19);
new XmGrace(xlabel => $hm_label,
	    ylabel => $smscatter_label,
	    XmGrace::logscale('x', 1e10, 1e15),
	    XmGrace::linscale('y', 0, 0.5, 0.1, 1),
	    color_errorstrip => 1,
	    #transparent_colors => 1,
	    legendxy => "0.3, 0.34",
	    text => [{text => "Central Galaxies Only", pos => "0.3, 0.8"}],
	    data => [(map {{data => "file[0,25,26,27]:$datadir/smhm_scatter_a$scales[$_].dat", metatype => "errorstrip", transform => exclude_zeros($_), fcolor => $fcolors[$_], smooth => 1}} reverse (0)),
		     (map {{data => "file[0,25,26,27]:$datadir/smhm_scatter_a$scales[$_].dat", legend => "z = $zs[$_]", type => ($_>0) ? "xydydy" : "xy", sskip => 1, transform => exclude_zeros($_), lcolor => $colors[$_], smooth => 1}} (0..3))
	    ])->write_pdf("$plotdir/AGR/$fn.agr");

@zs = (0);
@scales = get_scales_at_redshifts("$datadir", "smhm", @zs);
%zs = map { $scales[$_] => $zs[$_] } (0..$#zs);
$fn = sprintf("scatter_sf_q");
$fn =~ tr/./_/;
@colors = (1,2,4,3);
@fcolors = (17,18,20,19);
new XmGrace(xlabel => $hm_label,
	    ylabel => $smscatter_label,
	    XmGrace::logscale('x', 1e10, 1e15),
	    XmGrace::linscale('y', 0, 0.5, 0.1, 1),
	    color_errorstrip => 1,
	    #transparent_colors => 1,
	    legendxy => "0.3, 0.37",
	    text => [{text => "z = 0", pos => "1.07, 0.8"}],
	    data => [{data => "file[0,22,23,24]:$datadir/smhm_scatter_a$scales[0].dat", metatype => "errorstrip", transform => exclude_zeros(0), fcolor => $fcolors[0], smooth => 1},
		     {data => "file[0,22,23,24]:$datadir/smhm_scatter_a$scales[0].dat", legend => "All Galaxies", type => "xy", sskip => 1, transform => exclude_zeros(0), lcolor => 1, smooth => 1},
		     {data => "file[0,37,38,39]:$datadir/smhm_scatter_a$scales[0].dat", legend => "Star-Forming", type => "xydydy", sskip => 1, transform => exclude_zeros(0), lcolor => 3, smooth => 1},
		     {data => "file[0,40,41,42]:$datadir/smhm_scatter_a$scales[0].dat", legend => "Quenched", type => "xydydy", sskip => 1, transform => exclude_zeros(0), lcolor => 2, smooth => 1},
		     {data => $reddick13_data, legend => "Reddick et al. (2013)", type => "xydydy", metatype => "scatter", ssize => 1, scolor => 1, transform => XmGrace::tolin(0), lcolor => 1},
	    ])->write_pdf("$plotdir/AGR/$fn.agr");

$fn = sprintf("scatter_cen_sat");
$fn =~ tr/./_/;
@colors = (1,2,4,3);
@fcolors = (17,18,20,19);
new XmGrace(xlabel => $hm_label,
	    ylabel => $smscatter_label,
	    XmGrace::logscale('x', 1e10, 1e15),
	    XmGrace::linscale('y', 0, 0.5, 0.1, 1),
	    color_errorstrip => 1,
	    #transparent_colors => 1,
	    legendxy => "0.3, 0.37",
	    text => [{text => "z = 0", pos => "1.07, 0.8"}],
	    data => [{data => "file[0,25,26,27]:$datadir/smhm_scatter_a$scales[0].dat", metatype => "errorstrip", transform => exclude_zeros(0), fcolor => $fcolors[3], smooth => 1},
		     {data => "file[0,22,23,24]:$datadir/smhm_scatter_a$scales[0].dat", legend => "All Galaxies", type => "xydydy", sskip => 1, transform => exclude_zeros(0), lcolor => 1, smooth => 1},
		     {data => "file[0,25,26,27]:$datadir/smhm_scatter_a$scales[0].dat", legend => "Centrals", type => "xy", sskip => 1, transform => exclude_zeros(0), lcolor => 3, smooth => 1},
		     {data => "file[0,34,35,36]:$datadir/smhm_scatter_a$scales[0].dat", legend => "Satellites", type => "xydydy", sskip => 1, transform => exclude_zeros_sats(0), lcolor => 2, smooth => 1},
		     {data => $reddick13_data, legend => "Reddick et al. (2013)", type => "xydydy", metatype => "scatter", ssize => 1, scolor => 3, transform => XmGrace::tolin(0), lcolor => 3},
	    ])->write_pdf("$plotdir/AGR/$fn.agr");



$fn = sprintf("scatter_cen_q_sf");
$fn =~ tr/./_/;
@colors = (1,2,4,3);
@fcolors = (17,18,20,19);
new XmGrace(xlabel => $hm_label,
	    ylabel => $smscatter_label,
	    XmGrace::logscale('x', 1e10, 1e15),
	    XmGrace::linscale('y', 0, 0.5, 0.1, 1),
	    color_errorstrip => 1,
	    #transparent_colors => 1,
	    legendxy => "0.3, 0.37",
	    text => [{text => "z = 0", pos => "1.07, 0.77"}],
	    data => [{data => "file[0,22,23,24]:$datadir/smhm_scatter_a$scales[0].dat", metatype => "errorstrip", transform => exclude_zeros(0), fcolor => $fcolors[0], smooth => 1},
		     {data => "file[0,22,23,24]:$datadir/smhm_scatter_a$scales[0].dat", legend => "All Galaxies", type => "xy", sskip => 1, transform => exclude_zeros(0), lcolor => 1, smooth => 1},
		     {data => "file[0,28,29,30]:$datadir/smhm_scatter_a$scales[0].dat", legend => "Star-Forming Centrals", type => "xydydy", sskip => 1, transform => exclude_zeros(0), lcolor => 3, smooth => 1},
		     {data => "file[0,31,32,33]:$datadir/smhm_scatter_a$scales[0].dat", legend => "Quenched Centrals", type => "xydydy", sskip => 1, transform => exclude_zeros(0), lcolor => 2, smooth => 1},
		     {data => $reddick13_data, legend => "Reddick et al. (2013)", type => "xydydy", metatype => "scatter", ssize => 1, scolor => 1, transform => XmGrace::tolin(0), lcolor => 1},
	    ])->write_pdf("$plotdir/AGR/$fn.agr");
MultiThread::WaitForChildren();

if (-d $paperdir and -d "$paperdir/autoplots") {
    my @files = <$plotdir/PDF/scatter_*.pdf>;
    system("cp", @files, "$paperdir/autoplots");
}
