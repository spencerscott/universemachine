#!/usr/bin/perl -w
use lib qw(../perl ../../perl perl);
use GraphSetup;

our $datadir = "$dir/data/qfs";
our $plotdir = "$dir/graphs/mp_histories";
File::Path::make_path($plotdir, "$plotdir/AGR", "$plotdir/EPS", "$plotdir/PNG", "$plotdir/PDF");


my @zs = (0);
my @scales = get_scales_at_redshifts("$datadir", "qf_hm_histories", @zs);
my %zs = map { $scales[$_] => $zs[$_] } (0..$#zs);
open OUT, ">$plotdir/scales.txt";
print OUT "#Redshift Scale_Factor_Of_Snapshot_Used Actual_Redshift\n";
print OUT "$zs[$_] $scales[$_] ", 1/$scales[$_]-1,"\n" for (0..$#zs);
close OUT;

sub to_zp1 {
    my (@a) = split(" ", $_[0]);
    $a[0] = 1.0/$a[0];
    return "@a";
}

my $fn = sprintf("hm_quenched_sf");
$fn =~ tr/./_/;
new XmGrace(@x_ztime,
            ylabel => 'Main Progenitor Quenched Fraction',
            legendxy => "0.88, 0.78",
            lvgap => 0,
	    color_errorstrip => 1,
	    transparent_colors => $hq,
            text => [{text => "Star-Forming Galaxies", pos => "0.85, 0.8"}],
            ymin => 0, ymax => 1, yscale => "Normal", ymajortick => 0.2, yminorticks => 1,
            data => [
                (map {{data => "file[0,1,2,3]:$datadir/qf_hm_histories_hm$_.00_a$scales[0].dat", smooth=> ($_>13.5) ? 3 : 1, metatype => "errorstrip", transform => \&to_zp1}} (11..14)),
		(map {{data => "file[0,1,2,3]:$datadir/qf_hm_histories_hm$_.00_a$scales[0].dat", smooth=> ($_>13.5) ? 3 : 1, transform => \&to_zp1, ($_ < 12.5) ? "legend" : "legend" , 'M\sh\N = 10\S'.$_.'\N M'.$sun_txt}} (11..14))
])->write_pdf("$plotdir/AGR/$fn.agr");

$fn = sprintf("hm_quenched_q");
$fn =~ tr/./_/;
new XmGrace(@x_ztime,
            ylabel => 'Main Progenitor Quenched Fraction',
            legendxy => "0.88, 0.78",
            lvgap => 0,
	    color_errorstrip => 1,
	    transparent_colors => $hq,
            text => [{text => "Quenched Galaxies", pos => "0.85, 0.8"}],
            ymin => 0, ymax => 1, yscale => "Normal", ymajortick => 0.2, yminorticks => 1,
            data => [
                (map {{data => "file[0,4,5,6]:$datadir/qf_hm_histories_hm$_.00_a$scales[0].dat", smooth=> ($_>13.5) ? 3 : 1, metatype => "errorstrip", transform => \&to_zp1}} (11..14)),
		(map {{data => "file[0,4,5,6]:$datadir/qf_hm_histories_hm$_.00_a$scales[0].dat", smooth=> ($_>13.5) ? 3 : 1, transform => \&to_zp1, ($_ < 12.5) ? "legend" : "legend" , 'M\sh\N = 10\S'.$_.'\N M'.$sun_txt}} (11..14)),

])->write_pdf("$plotdir/AGR/$fn.agr");


MultiThread::WaitForChildren();

if (-d $paperdir and -d "$paperdir/autoplots") {
    my @files = <$plotdir/PDF/hm_quenched_*.pdf>;
    system("cp", @files, "$paperdir/autoplots");
}
