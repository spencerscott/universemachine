#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include <assert.h>
#include <sys/mman.h>
#include "config.h"
#include "config_vars.h"
#include "check_syscalls.h"
#include "sf_model.h"
#include "observations.h"
#include "universal_constants.h"

int float_compare(const void *a, const void *b) {
  const float *c = a;
  const float *d = b;
  if (*c < *d) return -1;
  if (*d < *c) return 1;
  return 0;
}

int double_compare(const void *a, const void *b) {
  const double *c = a;
  const double *d = b;
  if (*c < *d) return -1;
  if (*d < *c) return 1;
  return 0;
}

int main(int argc, char **argv) {
  int64_t i, j;
  if (argc < 3) {
    fprintf(stderr, "Usage: %s um.cfg /paths/to/model_outputs ...\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  do_config(argv[1]);
  if (!POSTPROCESSING_MODE) {
    fprintf(stderr, "[Error] Summary stats available for postprocessed outputs only (POSTPROCESSING_MODE=1).\n");
    exit(EXIT_FAILURE);
  }

  int64_t last_length=0,length=0,bestfit_index=-1;
  void **models = NULL;
  int64_t num_models = argc-2;
  float *data = NULL;
  double *ddata = NULL;
  check_realloc_s(models, sizeof(void *), num_models);
  check_realloc_s(data, sizeof(float), num_models);
  check_realloc_s(ddata, sizeof(double), num_models);
  for (i=2; i<argc; i++) {
    models[i-2] = check_mmap_file(argv[i], 'r', &length);
    if (last_length && (length!=last_length)) {
      fprintf(stderr, "[Error] Model %s and %s have different lengths! (%"PRId64" vs %"PRId64")\n", argv[i-1], argv[i], last_length, length);
      exit(EXIT_FAILURE);
    }
    last_length = length;
    if (bestfit_index < 0) bestfit_index = i-2;
    else {
      struct sf_model_allz *m=models[i-2], *b=models[bestfit_index];
      if (CHI2(*b) > CHI2(*m)) bestfit_index = i-2; 
      if (i==argc-1)
	fprintf(stderr, "Best-fit model: %s with chi^2 of %f\n", argv[bestfit_index+2], CHI2(*b));
    }
  }

  if (POSTPROCESSING_BESTFIT > -1 && POSTPROCESSING_BESTFIT < (argc-2)) {
    bestfit_index = POSTPROCESSING_BESTFIT;
    struct sf_model_allz *b=models[bestfit_index];
    fprintf(stderr, "Setting best-fit model to %s with chi^2 of %f\n", argv[bestfit_index+2], CHI2(*b));
  }
  
  char buffer[1024];
  snprintf(buffer, 1024, "%s/post_bestfit.dat", OUTBASE);
  FILE *bestfit = check_fopen(buffer, "w");
  check_fwrite(models[bestfit_index], 1, length, bestfit);
  fclose(bestfit);
  
  snprintf(buffer, 1024, "%s/post_median.dat", OUTBASE);
  FILE *median = check_fopen(buffer, "w");
  snprintf(buffer, 1024, "%s/post_up.dat", OUTBASE);
  FILE *up = check_fopen(buffer, "w");
  snprintf(buffer, 1024, "%s/post_dn.dat", OUTBASE);
  FILE *dn = check_fopen(buffer, "w");
  snprintf(buffer, 1024, "%s/post_up_2sigma.dat", OUTBASE);
  FILE *up2 = check_fopen(buffer, "w");
  snprintf(buffer, 1024, "%s/post_dn_2sigma.dat", OUTBASE);
  FILE *dn2 = check_fopen(buffer, "w");

#define MEDIAN(x) (x+(num_models/2))
#define UP(x)     (x+(int64_t)(num_models*((1.0+ONE_SIGMA)/2.0)))
#define DN(x)     (x+(int64_t)(num_models*((1.0-ONE_SIGMA)/2.0)))
#define UP2(x)     (x+(int64_t)(num_models*((1.0+TWO_SIGMA)/2.0)))
#define DN2(x)     (x+(int64_t)(num_models*((1.0-TWO_SIGMA)/2.0)))
#define WRITE_ALL(x,size,num)   { check_fwrite((x), (size), (num), median); check_fwrite((x), (size), (num), up); check_fwrite((x), (size), (num), dn); \
    check_fwrite((x), (size), (num), up2); check_fwrite(x, (size), (num), dn2); }
																			   

  
  //Models
  for (j=0; j<sizeof(struct sf_model_allz)/sizeof(double); j++) {
    for (i=0; i<num_models; i++) {
      struct sf_model_allz *tm = models[i];
      ddata[i] = tm->params[j];
    }
    qsort(ddata, num_models, sizeof(double), double_compare);
    check_fwrite(MEDIAN(ddata), sizeof(double), 1, median);
    check_fwrite(UP(ddata), sizeof(double), 1, up);
    check_fwrite(DN(ddata), sizeof(double), 1, dn);
    check_fwrite(UP2(ddata), sizeof(double), 1, up2);
    check_fwrite(DN2(ddata), sizeof(double), 1, dn2);
  }
  
  int64_t offset = sizeof(struct sf_model_allz);
  WRITE_ALL(models[bestfit_index]+offset, sizeof(int64_t), 3);
  int64_t num_scales = *((int64_t *)(models[bestfit_index]+offset));
  offset += sizeof(int64_t);
  int64_t sd_length = *((int64_t *)(models[bestfit_index]+offset));
  offset += sizeof(int64_t);
  int64_t num_obs = *((int64_t *)(models[bestfit_index]+offset));
  offset += sizeof(int64_t);
  WRITE_ALL(models[bestfit_index]+offset, sizeof(float), num_scales);
  offset += sizeof(float)*num_scales;
  fprintf(stderr, "Num scales: %"PRId64"\n", num_scales);
  fprintf(stderr, "Num shared data floats: %"PRId64"\n", sd_length / sizeof(float));
  fprintf(stderr, "Num obs: %"PRId64"\n", num_obs);
  
  sd_length /= sizeof(float);
  for (j=0; j<sd_length; j++) {
    for (i=0; i<num_models; i++) {
      float *td = models[i]+offset;
      data[i] = *td;
    }
    
    qsort(data, num_models, sizeof(float), float_compare);
    check_fwrite(MEDIAN(data), sizeof(float), 1, median);
    check_fwrite(UP(data), sizeof(float), 1, up);
    check_fwrite(DN(data), sizeof(float), 1, dn);
    check_fwrite(UP2(data), sizeof(float), 1, up2);
    check_fwrite(DN2(data), sizeof(float), 1, dn2);
    offset += sizeof(float);
  }
  
  struct observation o = {0};
  for (j=0; j<num_obs; j++) {
    memcpy(&o, models[bestfit_index]+offset, sizeof(struct observation));
    for (i=0; i<num_models; i++) {
      struct observation *to = models[i]+offset;
      ddata[i] = to->model_val;
    }
    qsort(ddata, num_models, sizeof(double), double_compare);
    double omed = *(MEDIAN(ddata));
    double oup = *(UP(ddata));
    double odn = *(DN(ddata));
    double oup2 = *(UP2(ddata));
    double odn2 = *(DN2(ddata));

    for (i=0; i<num_models; i++) {
      struct observation *to = models[i]+offset;
      ddata[i] = to->chi2;
    }
    qsort(ddata, num_models, sizeof(double), double_compare);
    
    o.model_val = omed;
    o.chi2 = *(MEDIAN(ddata));
    check_fwrite(&o, sizeof(struct observation), 1, median);
    o.model_val = oup;
    o.chi2 = *(UP(ddata));
    check_fwrite(&o, sizeof(struct observation), 1, up);
    o.model_val = odn;
    o.chi2 = *(DN(ddata));
    check_fwrite(&o, sizeof(struct observation), 1, dn);
    o.model_val = oup2;
    o.chi2 = *(UP2(ddata));
    check_fwrite(&o, sizeof(struct observation), 1, up2);
    o.model_val = odn2;
    o.chi2 = *(DN2(ddata));
    check_fwrite(&o, sizeof(struct observation), 1, dn2);
    offset += sizeof(struct observation);
  }
  
  fclose(median);
  fclose(up);
  fclose(dn);
  fclose(up2);
  fclose(dn2);
  for (i=0; i<num_models; i++) munmap(models[i], length);
  return 0;
}
