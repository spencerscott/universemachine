#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <sys/stat.h>
#include <stdarg.h>
#include "mt_rand.h"
#include "universe_time.h"
#include "distance.h"
#include "check_syscalls.h"
#include "mcmc_data.h"
#include "config.h"
#include "config_vars.h"
#include "version.h"
#include "universal_constants.h"
#include "sf_model.h"
#include "stats.h"


#define comparison(x)  ((x)->smhm_true_scatter_cen[16])

int main(int argc, char **argv) {
  //int64_t i,j, length;
  //char subtypes[4] = {'a', 's', 'q', 'c'};

  if (argc < 2) {
    fprintf(stderr, "%s", INFO_STRING);
    fprintf(stderr, "Usage: %s um.cfg postprocessed_outputs.*\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  do_config(argv[1]);
  if (!POSTPROCESSING_MODE) {
    fprintf(stderr, "[Error] POSTPROCESSING_MODE must be enabled to generate comparisons data.\n");
    exit(EXIT_FAILURE);
  }

  struct correl_stats *cs[NUM_PARAMS+2];
  int64_t i,j;
  for (j=0; j<NUM_PARAMS+2; j++) cs[j] = init_correl(0);
  
  for (i=2; i<argc; i++) {
    struct mcmc_data *data = load_mcmc_data(argv[i]);
    double val = data->smhm_true_scatter_cen[16];
    for (j=0; j<NUM_PARAMS+2; j++) {
      add_to_correl(cs[j], data->model->params[j], val);
    }
  }
  
  for (j=0; j<NUM_PARAMS+2; j++) {
    printf("Parameter %"PRId64"\n", j);
    print_correl(cs[j], stdout);
    print_rank_correl(cs[j], stdout);
  }
  return 0;
}
