#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <math.h>
#include <inttypes.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "stringparse.h"
#include "universal_constants.h"
#include "stats.h"
#include "mf_cache.h"

#undef UV_MIN
#undef UV_MAX
#undef UV_BPMAG
#undef UV_BINS
#define UV_MIN (-28)
#define UV_MAX (-6)
#define UV_BPMAG 5
#define UV_BINS ((UV_MAX-UV_MIN)*UV_BPMAG+1)

struct sf_model_allz *ms = NULL;
int64_t num_ms = 0;
double *uvlfs[UV_BINS]={0};
double *uvlf_slopes[UV_BINS]={0};
double *uvlfs_extrapolated[UV_BINS]={0};
double *frac_l[UV_BINS]={0};
double *frac_l_extrapolated[UV_BINS]={0};


struct sf_model *cs = NULL;

int sort_double(const void *a, const void *b) {
  const double *c = a;
  const double *d = b;
  if (*c < *d) return -1;
  if (*d < *c) return 1;
  return 0;
}

double doexp10(double x) {
  return exp(M_LN10*x);
}

double sfr_at_vmp(double lv, struct sf_model c) {
  double vd = lv - c.v_1;
  double vd2 = vd/c.delta;
  return (c.epsilon * (1.0/(doexp10(c.alpha*vd) + doexp10(c.beta*vd)) + c.gamma*exp(-0.5*vd2*vd2)));
}

double max_mass(double z) {
  return 13.5351-0.23712*z+2.0187*exp(-z/4.48394); //evolving from 1e-9 @ z=0 to 1e-7 @ z=10
}

double mass_to_vmax(double m, double z);

			 


void calc_models_at_z(double z, char *outdir) {
  //double sfr_lim = 0;
  char buffer[1024];
  int64_t j,i, k;
  double a = 1.0/(1.0+z);
  for (i=0; i<num_ms; i++) {
    cs[i] = calc_sf_model(ms[i], a);
    //double scatter = sqrt(cs[i].sig_sf*cs[i].sig_sf + cs[i].sig_sf_uncorr*cs[i].sig_sf_uncorr);
    //cs[i].obs_sfr_offset = cs[i].obs_sfr_offset_lin*exp(pow((scatter*log(10)),2.0)/2.0); //Scatter correction
    cs[i].obs_sfr_offset = cs[i].obs_sfr_offset_lin; //Scatter correction
  }

  double a4 = (z>4) ? a : (0.2);
  double kfuv = 5.1e-29*(1.0+exp(-20.79*a4+0.98));
  double sfr_thresh = log10(2.74117778e27*kfuv);
  //double erf_norm = 1.0/(sqrt(2)*0.3);
  double m_max = max_mass(z);
  for (k=0; k<UV_BINS; k++) {
    memset(uvlfs[k], 0, sizeof(double)*num_ms);
    memset(uvlfs_extrapolated[k], 0, sizeof(double)*num_ms);
    memset(uvlf_slopes[k], 0, sizeof(double)*num_ms);
    memset(frac_l[k], 0, sizeof(double)*num_ms);
    memset(frac_l_extrapolated[k], 0, sizeof(double)*num_ms);
  }

#define JMIN (160)
#define JMAX (300)
#define JBINS ((JMAX-JMIN)+1)
  double uvm[JBINS];
  double uvnd[JBINS];
  
  for (i=0; i<num_ms; i++) {
    memset(uvm, 0, sizeof(double)*JBINS);
    memset(uvnd, 0, sizeof(double)*JBINS);
    double min_uv=0, max_uv=0;
    double scatter = sqrt(cs[i].sig_sf*cs[i].sig_sf + cs[i].sig_sf_uncorr*cs[i].sig_sf_uncorr);
    double uv_scatter = sqrt(2.5*2.5*scatter*scatter + 0.3*0.3);
    double scatter_template[UV_BINS+1]={0};
    double tot_template = 0;
    int64_t max_k = 0;
    for (k=0; k<=UV_BINS; k++) {
      double offset_min = (double)(k-0.5)/(double)UV_BPMAG;
      double offset_max = (double)(k+0.5)/(double)UV_BPMAG;
      offset_min /= uv_scatter*sqrt(2);
      offset_max /= uv_scatter*sqrt(2);
      scatter_template[k] = 0.5*(erf(offset_max)-erf(offset_min));
      if (scatter_template[k] < 1e-10) {
	scatter_template[k] = 0;
	max_k = k;
	break;
      }
      tot_template += scatter_template[k];
      if (k>0) tot_template += scatter_template[k];
    }
    for (k=0; k<max_k; k++)
      scatter_template[k] /= tot_template;
    
    for (j=JMIN; j<=JMAX; j++) {
      double m = (double)j/20.0;
      if (m > m_max) break;
      double vmax = mass_to_vmax(m, z);
      double sfr2 = sfr_at_vmp(vmax, cs[i]);
      //double sfr = log10(sfr2);
      //double sfr3 = sfr;
      //if (sfr < sfr_lim) sfr = sfr_lim;
      double fq = 0.5+0.5*erf((vmax-cs[i].q_lvmp)/(cs[i].q_sig_lvmp*sqrt(2)));
      fq = cs[i].fq_min + (1.0-cs[i].fq_min)*fq;

      double fsf = 1.0 - fq;
      sfr2 *= fsf*cs[i].obs_sfr_offset; //Scatter correction
      double lsfr = (sfr2 > 0) ? log10(sfr2) : -100;

      uvm[j-JMIN] = -17.0 - 2.5*(lsfr-sfr_thresh);
      //double irx = exp(M_LN10*cs[i].uv_dust_slope*(uvm[j-JMIN]-cs[i].uv_dust_thresh)/-2.5);
      double a_uv = 0; //2.5*log1pf(irx)*(1.0/M_LN10);
      uvm[j-JMIN] += a_uv;
    }

    for (j=JMIN+1; j<JMAX; j++) {
      double m = (double)j/20.0;
      double nd = pow(10, mf_cache(a, m));
      if (uvm[j-JMIN+1] > uvm[j-JMIN]) break;
      double duv_dm = fabs(uvm[j-JMIN+1]-uvm[j-JMIN-1])*10.0;
      uvnd[j-JMIN] = nd / duv_dm;
    }

    //Note that j is set above
    j -= JMIN;
    max_uv = uvm[0];
    min_uv = uvm[j];
    double total_l = 0;
    for (k=0; k<UV_BINS; k++) {
      double uv = UV_MIN+(double)k/(double)UV_BPMAG;
      if (uv < min_uv) continue;
      if (uv >= max_uv) continue;
      while (uvm[j] < uv) j--;
      double uv1 = uvm[j];
      double uv2 = uvm[j+1];
      double nd1 = uvnd[j];
      double nd2 = uvnd[j+1];
      double f = (uv - uv1)/(uv2-uv1);
      uvlfs[k][i] = nd1 + f*(nd2-nd1);
      //total_l += uvlfs[k][i]*pow(10, uv/-2.5);
      //if (i==0) { printf("%g %g %g %g %g %g\n", uv, (double)j, uv1, uv2, nd1, nd2); }
    }

    //Add scatter
    int64_t l;
    double uvlf_scattered[UV_BINS]={0};
    for (k=0; k<UV_BINS; k++) {
      int64_t min_l = k-max_k+1;
      int64_t max_l = k+max_k;
      if (min_l<0) min_l = 0;
      if (max_l>UV_BINS) max_l = UV_BINS;
      for (l=min_l; l<max_l; l++)
	uvlf_scattered[l] += uvlfs[k][i]*scatter_template[llabs(k-l)];
    }

    //Correct for dust
    double uv_dustm[UV_BINS] = {0};
    double uv_ddm[UV_BINS] = {0};
    for (k=0; k<UV_BINS; k++) {
      double uv = UV_MIN+(double)k/(double)UV_BPMAG;
      double irx = exp(M_LN10*cs[i].uv_dust_slope*(uv-cs[i].uv_dust_thresh)/-2.5);
      double a_uv = 2.5*log1pf(irx)*(1.0/M_LN10);
      uv_dustm[k] = uv + a_uv;
      uv_ddm[k] = 1.0 + cs[i].uv_dust_slope*irx/(1.0+irx);
    }

    int64_t j=0;
    for (k=0; k<UV_BINS; k++) {
      double uv = UV_MIN+(double)k/(double)UV_BPMAG;
      while (uv_dustm[j]<uv && j<UV_BINS) j++;
      if (j==UV_BINS) break;
      double f = (uv - uv_dustm[j-1])/(uv_dustm[j]-uv_dustm[j-1]);
      uvlfs[k][i] = uvlf_scattered[j-1] + f*(uvlf_scattered[j]-uvlf_scattered[j-1]);
      uvlfs[k][i] *= uv_ddm[j-1] + f*(uv_ddm[j-1]-uv_ddm[j]);
      total_l += uvlfs[k][i]*pow(10, uv/-2.5);
    }
    for (; k<UV_BINS; k++) uvlfs[k][i] = 0;
    
    
    int64_t k17 = (-17-UV_MIN)*UV_BPMAG+0.5;
    double ratio = 0;
    double total_l2 = 0;
    double total_l2_extr = 0;
    if (uvlfs[k17][i]) ratio = uvlfs[k17+1][i]/uvlfs[k17][i];
    for (k=0; k<UV_BINS; k++) {
      double uv = UV_MIN+(double)k/(double)UV_BPMAG;
      uvlfs_extrapolated[k][i] = uvlfs[k][i];
      if (uv > -17) {
	uvlfs_extrapolated[k][i] = uvlfs[k17][i]*pow(ratio, k-k17);
      }
      uvlf_slopes[k][i] = 0;
      if (k>0 && uvlfs[k-1][i]>0 && uvlfs[k][i]>0) uvlf_slopes[k][i] = -1.0 + log10(uvlfs[k-1][i]/uvlfs[k][i])*(double)UV_BPMAG*2.5;
      total_l2 += uvlfs[k][i]*pow(10, uv/-2.5);
      if (total_l)
	frac_l[k][i] = total_l2 / total_l;
      total_l2_extr += uvlfs_extrapolated[k][i]*pow(10, uv/-2.5);
      if (total_l)
	frac_l_extrapolated[k][i] = total_l2_extr / total_l;
    }    
  }

  int64_t up = num_ms*0.5*(1.0+ONE_SIGMA);
  int64_t dn = num_ms*0.5*(1.0-ONE_SIGMA);
  int64_t up2 = num_ms*0.5*(1.0+TWO_SIGMA);
  int64_t dn2 = num_ms*0.5*(1.0-TWO_SIGMA);

  snprintf(buffer, 1024, "%s/uvlf_z%g.dat", outdir, z);
  FILE *out = check_fopen(buffer, "w");
  fprintf(out, "#M_1500 ND Err+ Err- Err+(2sig) Err-(2sig) Extrapolated Err+ Err- Err+(2sig) Err-(2sig) Frac_Total Err+ Err- Err+(2sig) Err-(2sig) Frac_Total_Extrapolated Err+ Err- Err+(2sig) Err-(2sig) Slope(M-%g) Err+ Err- Err+(2sig) Err-(2sig)\n", 0.5/(double)UV_BPMAG);
  for (k=0; k<UV_BINS; k++) {
    double uv = UV_MIN+(double)k/(double)UV_BPMAG;
    fprintf(out, "%g", uv);
    double *arrays[5] = {uvlfs[k], uvlfs_extrapolated[k], frac_l[k], frac_l_extrapolated[k], uvlf_slopes[k]};
    for (i=0; i<5; i++) {
      double bf = arrays[i][0];
      qsort(arrays[i], num_ms, sizeof(double), sort_double);
      if (i==4) {
	bf = arrays[i][(int)(0.5*num_ms)];
	fprintf(out, " %lg ", bf);
	fprintf(out, " %lg %lg ", arrays[i][up]-bf, bf-arrays[i][dn]);
	fprintf(out, "%lg %lg", arrays[i][up2]-bf, bf-arrays[i][dn2]);
      } else {
	fprintf(out, " %lg ", bf);
	fprintf(out, "%lg %lg ", arrays[i][up]-bf, bf-arrays[i][dn]);
	fprintf(out, "%lg %lg", arrays[i][up2]-bf, bf-arrays[i][dn2]);
      }
    }
    fprintf(out, "\n");
  }
  fclose(out);
}

int main(int argc, char **argv) {
  int64_t i;
  char buffer[2048];
  struct sf_model_allz m = {{0}}, b={{0}};
  enum parsetype types[NUM_PARAMS+1];
  void *data[NUM_PARAMS+1];

  if (argc < 2) {
    printf("Usage: %s mf_bolshoi_planck.dat < parameters.dat\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  load_mf_cache(argv[1], argc);
  
  SHORT_PARSETYPE;
  for (i=0; i<NUM_PARAMS+1; i++) {
    types[i] = F64;
    data[i] = &(m.params[i]);
  }
  data[NUM_PARAMS] = &(CHI2(m));
  while (fgets(buffer, 2048, stdin)) {
    if (stringparse(buffer, data, types, NUM_PARAMS+1)<NUM_PARAMS+1) continue;
    check_realloc_every(ms, sizeof(struct sf_model_allz), num_ms, 1000);
    ms[num_ms] = m;
    num_ms++;
    if (!CHI2(b) || CHI2(m) < CHI2(b)) {
      b=m;
      ms[num_ms-1] = ms[0];
      ms[0] = b;
    }
  }

  check_realloc_s(cs, sizeof(struct sf_model), num_ms);
  mkdir("uvlfs", 0755);
  
  for (i=0; i<UV_BINS; i++) {
    check_realloc_s(uvlfs[i], sizeof(double), num_ms);
    check_realloc_s(uvlfs_extrapolated[i], sizeof(double), num_ms);
    check_realloc_s(uvlf_slopes[i], sizeof(double), num_ms);
    check_realloc_s(frac_l[i], sizeof(double), num_ms);
    check_realloc_s(frac_l_extrapolated[i], sizeof(double), num_ms);
  }

  for (i=8; i<=40; i++) {
    double z = (double)i / (double)2.0;
    calc_models_at_z(z, "uvlfs");
    printf("Calculated %"PRId64" models at z=%f\n", num_ms, z);
  }

  return 0;
}


double mass_to_vmax (double m, double z) {
  double a = 1.0/(1.0+z);
  double a200 = a / 0.3782;
  double m200 = log10((1.0/0.68)*1.115e12 / ((pow(a200, -0.142441) + pow(a200, -1.78959))));
  double v200 = log10(200);
  double exp_vp = (m - m200)/3.0 + v200;
  return exp_vp;
}
