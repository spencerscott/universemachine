#ifndef _FITTER_H_
#define _FITTER_H_

#include "sf_model.h"

void initial_fit(double *params, double *steps, void (*c2f)(struct sf_model_allz *), FILE *output);

#endif /* _FITTER_H_ */
