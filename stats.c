#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <inttypes.h>
#include <assert.h>
#include "check_syscalls.h"
#include "stats.h"
#include "mt_rand.h"

#define ONE_SIGMA 0.341344746
int64_t initialized_rng = 0;

void clear_binstats(struct binstats *bs) {
  int64_t i, nb=bs->num_bins+1;
  if (bs->fullstats) {
    memset(bs->med, 0, sizeof(double)*(nb));
    memset(bs->stdup, 0, sizeof(double)*(nb));
    memset(bs->stddn, 0, sizeof(double)*(nb));
    memset(bs->avgsd, 0, sizeof(double)*(nb));
    if (bs->fullvals) {
      free(bs->fullvals);
      bs->fullvals = NULL;
    }
    for (i=0; i<nb; i++)
      if (bs->bvals[i]) free(bs->bvals[i]);
    memset(bs->bvals, 0, sizeof(STATS_VAL_TYPE *)*(nb));
    memset(bs->alloced_bvals, 0, sizeof(int64_t)*(nb));
  }
  bs->num_fullvals = bs->alloced_fullvals = 0;
  memset(bs->counts, 0, sizeof(int64_t)*(nb));
  memset(bs->avg, 0, sizeof(double)*(nb));
  memset(bs->sd, 0, sizeof(double)*(nb));
  memset(bs->m2, 0, sizeof(double)*(nb));
  memset(bs->keyavg, 0, sizeof(double)*(nb));
}


int64_t binstats_rad_partition(STATS_VAL_TYPE *rad, int64_t left, int64_t right, int64_t pivot_ind) {
  double pivot = rad[pivot_ind];
  STATS_VAL_TYPE tmp;
  int64_t si, i;
#define SWAP(a,b) { tmp = rad[a]; rad[a] = rad[b]; rad[b] = tmp; }
  SWAP(pivot_ind, right-1);
  si = right-2;
  for (i = left; i<si; i++) {
    if (rad[i] > pivot) { SWAP(i, si); si--; i--; }
  }
  if (rad[si] <= pivot) si++;
  SWAP(right-1, si);
  return si;
#undef SWAP
}

double stats_random_unit(void) {
  return(((double)(rand()%(RAND_MAX))/(double)(RAND_MAX)));
}

double binstats_find_perc(STATS_VAL_TYPE *rad, int64_t num_v, int64_t target_ind) {
  int64_t pivot_index, k = target_ind;
  int64_t left = 0, right = num_v;
  assert(num_v>0);
  if (num_v < 2) return rad[0];
  while (1) {
    pivot_index = binstats_rad_partition(rad, left, right, 
                                left + stats_random_unit()*(right-left));
    if (rad[left]==rad[right-1] || pivot_index==(right-1)) {
      //rad[left]==rad[right] implies either everything is the same or that
      // we picked the max element by chance and there are repeated values.
      while (pivot_index>=k && rad[right-1]==rad[pivot_index]) pivot_index--;
      if (k-1 == pivot_index) return rad[k];
      //else pivot_index++;
      //Otherwise, we found a different value that is < the max and can get rid of it
      pivot_index = binstats_rad_partition(rad, left, right, pivot_index);
    }
    if (k == pivot_index) return rad[k];
    if (k < pivot_index) right = pivot_index;
    else left = pivot_index+1;
    if (right-left < 2) return rad[left];
  }
}


struct binstats *init_binstats(double bin_start, double bin_end, double bpunit,
			       int64_t fullstats, int64_t logarithmic)
{
  struct binstats *bs = NULL;
  check_realloc_s(bs, sizeof(struct binstats), 1);
  memset(bs, 0, sizeof(struct binstats));
  bs->bin_start = bin_start;
  bs->bin_end = bin_end;
  bs->bpunit = bpunit;
  bs->num_bins = (bin_end-bin_start)*bpunit + 1.0000001;
  bs->fullstats = fullstats;
  bs->logarithmic = logarithmic;
  bs->rebin_thresh = STATS_DEFAULT_REBIN_THRESH;
  
  int64_t nb = bs->num_bins+1;
  check_realloc_s(bs->keyavg, sizeof(double), (nb));
  check_realloc_s(bs->avg, sizeof(double), (nb));
  check_realloc_s(bs->m2, sizeof(double), (nb));
  check_realloc_s(bs->sd, sizeof(double), (nb));

  if (fullstats) {
    check_realloc_s(bs->med, sizeof(double), (nb));
    check_realloc_s(bs->stdup, sizeof(double), (nb));
    check_realloc_s(bs->stddn, sizeof(double), (nb));
    check_realloc_s(bs->avgsd, sizeof(double), (nb));
    check_realloc_s(bs->bvals, sizeof(STATS_VAL_TYPE *), (nb));
    check_realloc_s(bs->alloced_bvals, sizeof(int64_t *), (nb));
    memset(bs->bvals, 0, sizeof(STATS_VAL_TYPE *)*(nb));
  }

  check_realloc_s(bs->counts, sizeof(int64_t), (nb));
  clear_binstats(bs);
  return bs;
}

void free_bin_names(char ***names, int64_t num_bins) {
  int64_t i;
  if (!names) return;
  if (*names) {
    for (i=0; i<num_bins+1; i++) 
      if (names[0][i]) free(names[0][i]);
    free(names[0]);
    names[0] = NULL;
  }
}

void free_binstats(struct binstats *bs) {
  clear_binstats(bs);
  free(bs->avg);
  free(bs->keyavg);
  free(bs->sd);
  free(bs->m2);
  free(bs->counts);
  if (bs->bvals) free(bs->bvals);
  if (bs->alloced_bvals) free(bs->alloced_bvals);
  if (bs->fullvals) free(bs->fullvals);
  if (bs->med) free(bs->med);
  if (bs->stdup) free(bs->stdup);
  if (bs->stddn) free(bs->stddn);
  free_bin_names(&(bs->names), bs->num_bins);
  free(bs);
}

void set_bin_names(struct binstats *bs, char **names) {
  free_bin_names(&(bs->names), bs->num_bins);
  check_realloc_s(bs->names, sizeof(char *), bs->num_bins+1);
  memset(bs->names, 0, sizeof(char *)*(bs->num_bins+1));
  for (int64_t i=0; (i<bs->num_bins+1) && names[i]; i++)
    bs->names[i] = check_strdup(names[i]);
}

int64_t _update_binstats(struct binstats *bs, int64_t bin, double keyval, double val) {
  if (bin < 0 || bin > bs->num_bins) bin = bs->num_bins;
  bs->counts[bin]++;
  bs->keyavg[bin] += (keyval - bs->keyavg[bin])/(double)bs->counts[bin];
  double delta = val - bs->avg[bin];
  bs->avg[bin] += delta / (double)bs->counts[bin];
  bs->m2[bin] += delta*(val - bs->avg[bin]);
  bs->sd[bin] = bs->m2[bin] /(double)bs->counts[bin];
  return bin;
}


void _rebin_fullvals(struct binstats *bs) {
  int64_t i, total=0;
  for (i=0; i<bs->num_bins+1; i++)
    if (bs->counts[i] > bs->alloced_bvals[i])
      check_realloc_s(bs->bvals[i], sizeof(STATS_VAL_TYPE), bs->counts[i]);
    
  for (i=0; i<bs->num_fullvals; i++) {
    int64_t bin = bs->fullvals[i].bin;
    assert(bin >= 0 && bin < bs->num_bins+1);
    bs->bvals[bin][bs->alloced_bvals[bin]] = bs->fullvals[i].val;
    bs->alloced_bvals[bin]++;
  }

  for (i=0; i<bs->num_bins+1; i++) {
    assert(bs->alloced_bvals[i]>=bs->counts[i]);
    total+=bs->counts[i];
  }

  bs->num_fullvals = 0; //Don't free memory, because it will probably be reused soon...
  if (bs->rebin_thresh < total/10) //Increase rebin thresh so that up to 20% of total mem is used
    bs->rebin_thresh = total/10;
}

void add_to_binstats(struct binstats *bs, double key, double val) {
  double keyval = key;
  int64_t bin;
  if (bs->logarithmic) {
    if (key <= 0) keyval = -1000;
    else keyval = log10(key);
  }
  bin = (keyval-bs->bin_start)*bs->bpunit;
  bin = _update_binstats(bs, bin, keyval, val);
  if (bs->fullstats) {
    if (bs->alloced_bvals[bin]>=bs->counts[bin]) {
      bs->bvals[bin][bs->counts[bin]-1] = val;
    } else {
      check_realloc_bold(bs->fullvals, sizeof(struct fullval), 
			 bs->alloced_fullvals, bs->num_fullvals+1);
      bs->fullvals[bs->num_fullvals].bin = bin;
      bs->fullvals[bs->num_fullvals].val = val;
      bs->num_fullvals++;
      if (bs->num_fullvals >= bs->rebin_thresh) _rebin_fullvals(bs);
    }
  }
} 

void binstats_prealloc(struct binstats *bs, int64_t vals, int64_t prewrite) {
  if (!bs->fullstats) return;
  if (vals < bs->alloced_fullvals) return;
  check_realloc_s(bs->fullvals, sizeof(struct fullval), vals);
  bs->alloced_fullvals = vals;
  if (prewrite) memset(bs->fullvals+bs->num_fullvals, 0, 
		       sizeof(struct fullval)*(vals-bs->num_fullvals));
}


//Not the best for cache misses, but simple and faster than qsort.
void sort_fullvals_by_bin(struct binstats *bs) {
  if (!bs->fullstats) return;
  int64_t *offsets = NULL, *new_offsets = NULL;
  check_realloc_s(offsets, sizeof(int64_t), bs->num_bins+2);
  check_realloc_s(new_offsets, sizeof(int64_t), bs->num_bins+1);

  int64_t i, bin=0;
  offsets[0] = 0;
  for (i=1; i<bs->num_bins+2; i++) offsets[i] = offsets[i-1]+bs->counts[i-1];
  memcpy(new_offsets, offsets, sizeof(int64_t)*(bs->num_bins+1));

  for (bin=0; bin<=bs->num_bins; bin++) { //Not necessary to sort last bin, but do want to double-check it...
    for (i=new_offsets[bin]; i<offsets[bin+1]; i++) {
      int64_t tbin = bs->fullvals[i].bin;
      if (tbin==bin) continue;
      assert(tbin > bin);
      struct fullval tmp = bs->fullvals[new_offsets[tbin]];
      bs->fullvals[new_offsets[tbin]] = bs->fullvals[i];
      bs->fullvals[i] = tmp;
      new_offsets[tbin]++;
      i--;
    }
  }
  free(offsets);
  free(new_offsets);
}

void calc_jackknife_errs(struct binstats *bs) {
  int64_t i, j;
  if (!bs->fullstats) {
    fprintf(stderr, "Binstats structure needs to be set as fullstats before calculating jackknife errors!\n");
    exit(1);
  }

  _rebin_fullvals(bs);
  for (i=0; i<bs->num_bins; i++) {
    if (!bs->counts[i])
      bs->avgsd[i] = 0;
    else if (bs->counts[i] < 2)
      bs->avgsd[i] = bs->bvals[i][0];
    else {
      double sd = 0;
      for (j=0; j<bs->counts[i]; j++) {
	double mj = (bs->avg[i] * bs->counts[i] - bs->bvals[i][j])/((double)bs->counts[i]-1);
	double diff = mj - bs->avg[i];
	sd += diff*diff;
      }
      sd = (bs->counts[i]-1)*sd / ((double)(bs->counts[i]));
      bs->avgsd[i] = sqrt(sd);
    }
  }
}

void calc_median(struct binstats *bs) {
  int64_t i;
  if (!bs->fullstats) {
    fprintf(stderr, "Binstats structure needs to be set as fullstats before calculating median!\n");
    exit(1);
  }
  _rebin_fullvals(bs);
  for (i=0; i<bs->num_bins; i++) {
    if (!bs->counts[i])
      bs->med[i] = bs->stdup[i] = bs->stddn[i] = 0;
    else {
      int64_t nmed = bs->counts[i]*0.5, nup = bs->counts[i]*(0.5+ONE_SIGMA), ndn = bs->counts[i]*(0.5-ONE_SIGMA);
      bs->med[i] = binstats_find_perc(bs->bvals[i], bs->counts[i], nmed);
      bs->stdup[i] = bs->stddn[i] = 0;
      assert(bs->med[i] == bs->bvals[i][nmed]);
      if (nmed < bs->counts[i])
	bs->stdup[i] = binstats_find_perc(bs->bvals[i]+nmed, bs->counts[i]-nmed, nup-nmed) - bs->med[i];
      if (ndn < nmed)
	bs->stddn[i] = bs->med[i] - binstats_find_perc(bs->bvals[i], nmed, ndn);
    }
  }
  calc_jackknife_errs(bs);
}

void print_avgs(struct binstats *bs, FILE *output) {
  int64_t i;
  for (i=0; i<bs->num_bins; i++) {
    if (!bs->counts[i]) continue;
    double key = bs->keyavg[i];
    if (bs->logarithmic) key = pow(10, key);
    if (bs->names && bs->names[i]) fprintf(output, "%s ", bs->names[i]);
    else fprintf(output, "%g ", key);
    fprintf(output, "%g %g #%"PRId64"\n", bs->avg[i], sqrt(bs->sd[i]),
	    bs->counts[i]);
  }
}

void print_medians(struct binstats *bs, FILE *output) {
  int64_t i;
  calc_median(bs);
  for (i=0; i<bs->num_bins; i++) {
    if (!bs->counts[i]) continue;
    double key = bs->keyavg[i];
    if (bs->logarithmic) key = pow(10, key);
    if (bs->names && bs->names[i]) fprintf(output, "%s ", bs->names[i]);
    else fprintf(output, "%g ", key);
    fprintf(output, "%g %g %g #avg: %g; avg_err: %g; sd: %g; counts: %"PRId64"\n",
	    bs->med[i], bs->stdup[i], bs->stddn[i], bs->avg[i], bs->avgsd[i], sqrt(bs->sd[i]), bs->counts[i]);
  }
}

void print_keyname(struct binstats *bs, int64_t bin, FILE *output) {
  double key = bs->keyavg[bin];
  if (bs->logarithmic) key = pow(10, key);
  if (bs->names && bs->names[bin]) fprintf(output, "%s", bs->names[bin]);
  else fprintf(output, "%g", key);
}

void print_5stats(struct binstats *bs, int64_t bin, FILE *output) {
  fprintf(output, " %g %g %g %g %g",
	  bs->avg[bin], bs->avgsd[bin], bs->med[bin], bs->stdup[bin], bs->stddn[bin]);
}


void clear_correl(struct correl_stats *cs) {
  if (cs->cvals) free(cs->cvals);
  memset(cs, 0, sizeof(struct correl_stats));
}

void free_correl(struct correl_stats *cs) {
  if (!cs) return;
  if (cs->cvals) free(cs->cvals);
  free(cs);
}


struct correl_stats *init_correl(int64_t bootstrap_samples) {
  struct correl_stats *cs = NULL;
  check_realloc_s(cs, sizeof(struct correl_stats), 1);
  memset(cs, 0, sizeof(struct correl_stats));
  cs->resamples = bootstrap_samples;
  if (!initialized_rng) r250_init(87L);
  return cs;
}

void add_to_correl(struct correl_stats *cs, double x, double y) {
  cs->counts++;
  double delta = x - cs->xavg;
  cs->xavg += delta / (double) cs->counts;
  cs->m2x += delta*(x-cs->xavg);
  cs->sdx = cs->m2x / (double) cs->counts;

  delta = y - cs->yavg;
  cs->yavg += delta / (double) cs->counts;
  cs->m2y += delta*(y-cs->yavg);
  cs->sdy = cs->m2y / (double) cs->counts;

  check_realloc_bold(cs->cvals, sizeof(struct correlval),
		     cs->alloced, cs->counts);
  cs->cvals[cs->counts-1].v[0] = x;
  cs->cvals[cs->counts-1].v[1] = y;
}


double _correl(struct correl_stats *cs) {
  int64_t i;
  double xavg=0, yavg=0, x2=0, y2=0, xy=0;
  if (!cs->counts) return 0;
  for (i=0; i<cs->counts; i++) {
    xavg += cs->cvals[i].v[0];
    yavg += cs->cvals[i].v[1];
  }
  xavg /= (double)cs->counts;
  yavg /= (double)cs->counts;
  for (i=0; i<cs->counts; i++) {
    double dx = cs->cvals[i].v[0] - xavg;
    double dy = cs->cvals[i].v[1] - yavg;
    x2 += dx*dx;
    y2 += dy*dy;
    xy += dy*dx;
  }
  xy /= (double)cs->counts;
  double ysd = sqrt(y2/(double)cs->counts);
  double xsd = sqrt(x2/(double)cs->counts);
  if (!ysd || !xsd) return 0;
  return (xy / (ysd*xsd));
}

#define CMP(x,y) if ((x) < (y)) return -1; if ((x) > (y)) return 1; return 0;

int _correl_sort_x(const void *a, const void *b) {
  const struct correlval *c = a;
  const struct correlval *d = b;
  CMP(c->v[0], d->v[0]);
}

int _correl_sort_y(const void *a, const void *b) {
  const struct correlval *c = a;
  const struct correlval *d = b;
  CMP(c->v[1], d->v[1]);
}

void _correl_to_rank(struct correl_stats *cs) {
  int64_t i;
  qsort(cs->cvals, cs->counts, sizeof(struct correlval), _correl_sort_x);
  for (i=0; i<cs->counts; i++) cs->cvals[i].v[0] = i;
  qsort(cs->cvals, cs->counts, sizeof(struct correlval), _correl_sort_y);
  for (i=0; i<cs->counts; i++) cs->cvals[i].v[1] = i;
}

int _correl_sort_samples(const void *a, const void *b) {
  const double *c = a;
  const double *d = b;
  CMP(*c, *d);
}

void _bootstrap_correl(struct correl_stats *cs, double *rho, double *rho_up, double *rho_dn, int64_t rank) {
  *rho = *rho_up = *rho_dn = 0;
  if (!cs->counts) return;
  if (!cs->resamples && !rank) {
    *rho = _correl(cs);
    return;
  }

  struct correl_stats c2 = {0};
  c2.counts = cs->counts;
  check_realloc_s(c2.cvals, sizeof(struct correlval), cs->counts);
  memcpy(c2.cvals, cs->cvals, sizeof(struct correlval)*cs->counts);
  if (rank) _correl_to_rank(&c2);
  *rho = _correl(&c2);
  if (!cs->resamples) { free(c2.cvals); return; }

  int64_t i, j;
  double *rho_samples = NULL;
  check_realloc_s(rho_samples, sizeof(double), cs->resamples);
  for (i=0; i<cs->resamples; i++) {
    for (j=0; j<cs->counts; j++) {
      c2.cvals[j] = cs->cvals[r250()%cs->counts];
    }
    if (rank) _correl_to_rank(&c2);
    rho_samples[i] = _correl(&c2);
  }
  free(c2.cvals);

  qsort(rho_samples, cs->resamples, sizeof(double), _correl_sort_samples);
  *rho_up = rho_samples[(int64_t)((0.5+ONE_SIGMA)*cs->resamples)] - *rho;
  *rho_dn = *rho - rho_samples[(int64_t)((0.5-ONE_SIGMA)*cs->resamples)];
  free(rho_samples);
}


void calc_correl(struct correl_stats *cs) {
  _bootstrap_correl(cs, &cs->rho, &cs->rho_up, &cs->rho_dn, 0);
  cs->rho_se = 1;
  if (cs->counts > 2) cs->rho_se = sqrt(1.0-cs->rho*cs->rho) / sqrt(cs->counts-2);
}

void calc_rank_correl(struct correl_stats *cs) {
  _bootstrap_correl(cs, &cs->rank_rho, &cs->rank_rho_up, &cs->rank_rho_dn, 1);
  cs->rank_rho_se = 1;
  if (cs->counts > 2) cs->rank_rho_se = sqrt(1.0-cs->rank_rho*cs->rank_rho) / sqrt(cs->counts-2);
}

void print_correl(struct correl_stats *cs, FILE *output) {
  calc_correl(cs);
  fprintf(output, "%g %g %g #SE: %g #counts: %"PRId64"\n", 
	  cs->rho, cs->rho_up, cs->rho_dn, cs->rho_se, cs->counts);
}

void print_rank_correl(struct correl_stats *cs, FILE *output) {
  calc_correl(cs);
  calc_rank_correl(cs);
  fprintf(output, "%g %g %g #SE: %g #Pearson correl: %g %g %g #SE: %g #counts: %"PRId64"\n", 
	  cs->rank_rho, cs->rank_rho_up, cs->rank_rho_dn, cs->rank_rho_se, 
	  cs->rho, cs->rho_up, cs->rho_dn, cs->rho_se, cs->counts);
}

struct binned_correl *init_binned_correl(double bin_start, double bin_end, double bpunit,
					 int64_t bootstrap_samples, int64_t logarithmic)
{
  int64_t i;
  struct binned_correl *bc = NULL;
  check_realloc_s(bc, sizeof(struct binned_correl), 1);
  memset(bc, 0, sizeof(struct binned_correl));
  bc->bin_start = bin_start;
  bc->bin_end = bin_end;
  bc->bpunit = bpunit;
  bc->num_bins = (bin_end-bin_start)*bpunit + 1.0000001;
  bc->resamples = bootstrap_samples;
  bc->logarithmic = logarithmic;

  check_realloc_s(bc->keyavg, sizeof(double), (bc->num_bins+1));
  check_realloc_s(bc->cstats, sizeof(struct correl_stats), bc->num_bins+1);
  memset(bc->cstats, 0, sizeof(struct correl_stats)*(bc->num_bins+1));
  for (i=0; i<bc->num_bins+1; i++) bc->cstats[i].resamples = bc->resamples;
  return bc;
}

void clear_binned_correl(struct binned_correl *bc) {
  int64_t i;
  for (i=0; i<bc->num_bins+1; i++)
    if (bc->cstats[i].cvals) free(bc->cstats[i].cvals);
  memset(bc->cstats, 0, sizeof(struct correl_stats)*(bc->num_bins+1));
  for (i=0; i<bc->num_bins+1; i++) bc->cstats[i].resamples = bc->resamples;
}

void free_binned_correl(struct binned_correl *bc) {
  int64_t i;
  if (!bc) return;
  for (i=0; i<bc->num_bins+1; i++)
    if (bc->cstats[i].cvals) free(bc->cstats[i].cvals);
  free_bin_names(&(bc->names), bc->num_bins);
  free(bc);
}

void add_to_binned_correl(struct binned_correl *bc, double key, double x, double y) {
  double keyval = key;
  int64_t bin;
  if (bc->logarithmic) {
    if (key <= 0) keyval = -1000;
    else keyval = log10(key);
  }
  bin = (keyval-bc->bin_start)*bc->bpunit;
  if (bin < 0 || bin > bc->num_bins) bin = bc->num_bins;
  bc->keyavg[bin] += (keyval - bc->keyavg[bin])/(double)(bc->cstats[bin].counts+1);
  add_to_correl(bc->cstats+bin, x, y);
}

void set_bin_names_correl(struct binned_correl *bc, char **names) {
  free_bin_names(&(bc->names), bc->num_bins);
  check_realloc_s(bc->names, sizeof(char *), bc->num_bins+1);
  memset(bc->names, 0, sizeof(char *)*(bc->num_bins+1));
  for (int64_t i=0; (i<bc->num_bins+1) && names[i]; i++)
    bc->names[i] = check_strdup(names[i]);
}

void print_binned_correl(struct binned_correl *bc, FILE *output, int64_t print_ranked) {
  int64_t i;
  for (i=0; i<bc->num_bins; i++) {
    if (!bc->cstats[i].counts) continue;
    double key = bc->keyavg[i];
    if (bc->logarithmic) key = pow(10, key);
    if (bc->names && bc->names[i]) fprintf(output, "%s ", bc->names[i]);
    else fprintf(output, "%g ", key);
    if (print_ranked) print_rank_correl(bc->cstats+i, output);
    else print_correl(bc->cstats+i, output);
  }
}
