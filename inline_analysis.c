#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <inttypes.h>
#include <math.h>
#include <assert.h>
#include "make_sf_catalog.h"
#include "observations.h"
#include "config_vars.h"
#include "check_syscalls.h"
#include "corr_lib.h"
#include "universe_time.h"

double _pp_calc_corr(int64_t i, int64_t thresh, int64_t subtype, int64_t rbin, double scale, int64_t post) {
  double s=0, c=0;
  float *mcorrs = (post) ? fcorrs_post : fcorrs;
  float *mcounts = (post) ? fcounts_post : fccounts;
  s = mcorrs[i*NUM_CORR_BINS_PER_STEP + thresh*NUM_CORR_TYPES*NUM_BINS + subtype*NUM_BINS + rbin];
  c = mcounts[i*(NUM_CORR_TYPES*NUM_CORR_THRESHES) + thresh*NUM_CORR_TYPES + subtype];
  if (!c) return 0;
  s /= c;

  double d = c; //Density
  if (subtype == 3) { //Cross-correlation: average pair count density: (nq*nsf)/box_size
    float *counts = mcounts + i*(NUM_CORR_TYPES*NUM_CORR_THRESHES) + thresh*NUM_CORR_TYPES;
    d = 1.0*(counts[1]*counts[2]);
    d /= c;
  }
  
  if (!d) return 0;
  double cd1 = pow(10, 2.0*(MIN_DIST+rbin*INV_DIST_BPDEX));
  double cd2 = pow(10, 2.0*(MIN_DIST+(rbin+1)*INV_DIST_BPDEX));
  double bin_area = M_PI*(cd2-cd1);
  double counts_per_length = bin_area * d / (BOX_SIZE*BOX_SIZE*BOX_SIZE);
  double pi_max = CORR_LENGTH_PI;
  if (scale < CORR_HIGHZ_SCALE) pi_max = CORR_LENGTH_PI_HIGHZ;
  if (post) pi_max = POSTPROCESS_CORR_LENGTH_PI;
  double expected_counts = counts_per_length * 2.0 * pi_max;
  return ((s - expected_counts)/counts_per_length);
}


double _pp_calc_xcorr(int64_t i, int64_t thresh, int64_t subtype, int64_t rbin, double scale, int64_t post) {
  double s=0, c=0;
  float *mcorrs = (post) ? fxcorrs_post : fxcorrs;
  float *mcounts = (post) ? fcounts_post : fccounts;

  s = mcorrs[i*NUM_XCORR_BINS_PER_STEP + thresh*NUM_CORR_TYPES*NUM_BINS + subtype*NUM_BINS + rbin];
  c = mcounts[i*(NUM_CORR_TYPES*NUM_CORR_THRESHES) + XCORR_BIN1[thresh]*NUM_CORR_TYPES + XCORR_COUNTS1[subtype]];
  if (!c) return 0;
  s /= c;

  double d = 0; //Density
  float *counts1 = mcounts + i*(NUM_CORR_TYPES*NUM_CORR_THRESHES) + XCORR_BIN1[thresh]*NUM_CORR_TYPES;
  float *counts2 = mcounts + i*(NUM_CORR_TYPES*NUM_CORR_THRESHES) + XCORR_BIN2[thresh]*NUM_CORR_TYPES;
  d = (counts1[XCORR_COUNTS1[subtype]]*counts2[XCORR_COUNTS2[subtype]]);
  d /= c;
  
  if (!d) return 0;
  double cd1 = pow(10, 2.0*(MIN_DIST+rbin*INV_DIST_BPDEX));
  double cd2 = pow(10, 2.0*(MIN_DIST+(rbin+1)*INV_DIST_BPDEX));
  double bin_area = M_PI*(cd2-cd1);
  double counts_per_length = bin_area * d / (BOX_SIZE*BOX_SIZE*BOX_SIZE);
  double pi_max = CORR_LENGTH_PI;
  if (scale < CORR_HIGHZ_SCALE) pi_max = CORR_LENGTH_PI_HIGHZ;
  if (post) pi_max = POSTPROCESS_CORR_LENGTH_PI;
  double expected_counts = counts_per_length * 2.0 * pi_max;
  return ((s - expected_counts)/counts_per_length);
}


void pp_calc_corr(void) {
  int64_t i, j, k, l;
  for (i=0; i<num_scales; i++) {
    for (j=0; j<NUM_CORR_THRESHES; j++) {
      for (k=0; k<NUM_CORR_TYPES; k++) {
	for (l=0; l<NUM_BINS; l++) {
	  int64_t bin = i*NUM_CORR_BINS_PER_STEP + j*NUM_CORR_TYPES*NUM_BINS + k*NUM_BINS + l;
	  int64_t all_bin = i*NUM_CORR_BINS_PER_STEP + j*NUM_CORR_TYPES*NUM_BINS + l;
	  int64_t sf_bin = all_bin + NUM_BINS;
	  int64_t q_bin = sf_bin + NUM_BINS;
	  cfs_post[bin] = _pp_calc_corr(i,j,k,l,scales[i],1);
	  if (k>0 && cfs_post[all_bin]) cf_ratios_post[bin] = cfs_post[bin] / cfs_post[all_bin];
	  if (k==(NUM_CORR_TYPES-1) && cfs_post[sf_bin]) cf_ratios_post[all_bin] = cfs_post[q_bin]/cfs_post[sf_bin];
	}
      }
    }
  }
}

void pp_calc_xcorr(void) {
  int64_t i, j, k, l;
  for (i=0; i<num_scales; i++) {
    for (j=0; j<NUM_XCORR_THRESHES; j++) {
      for (k=0; k<NUM_CORR_TYPES; k++) {
	for (l=0; l<NUM_BINS; l++) {
	  int64_t bin = i*NUM_XCORR_BINS_PER_STEP + j*NUM_CORR_TYPES*NUM_BINS + k*NUM_BINS + l;
	  xcfs_post[bin] = _pp_calc_xcorr(i,j,k,l,scales[i],1);
	}
      }
    }
  }
}

void pp_calc_wl(void) {
  int64_t i, j;
  for (i=0; i<NUM_WL_THRESHES*NUM_WL_TYPES*NUM_WL_ZS; i++) {
    double hcounts = wl_hcounts[i];
    if (hcounts < 1) hcounts = 1;
    double last_total = 0;
    int64_t all_bin = i - (i%NUM_WL_TYPES);
    for (j=0; j<NUM_WL_BINS; j++) {
      double r2 = 1e6*pow(10, WL_MIN_R+j/(double)WL_BPDEX)/h0; //In pc
      double r1 = 1e6*pow(10, WL_MIN_R+(j-1)/(double)WL_BPDEX)/h0; //In pc
      double a = M_PI*(r2*r2-r1*r1);
      double av_r = sqrt(r1*r1+ 0.5*(r2*r2-r1*r1));
      double av_a = M_PI*av_r*av_r;
      double enc_dens = (last_total + wl_pcounts[i*NUM_WL_BINS+j]/2.0)/av_a;
      double avg_dens = wl_pcounts[i*NUM_WL_BINS+j]/a;
      double result = (enc_dens-avg_dens)*WL_PARTICLE_MASS/(h0*hcounts); //In Msun/pc^2
      wl_post[i*NUM_WL_BINS+j] = result;
      double all_val = wl_post[all_bin*NUM_WL_BINS+j];
      wl_post_ratios[i*NUM_WL_BINS+j] = (all_val>0 && result>0) ? result/all_val : 1.0;
      if ((i%NUM_WL_TYPES)==(NUM_WL_TYPES-1)) {
	double sf_val = wl_post_ratios[(all_bin+1)*NUM_WL_BINS+j];
	double q_val = wl_post_ratios[(all_bin+2)*NUM_WL_BINS+j];
	wl_post_ratios[all_bin*NUM_WL_BINS+j] = (sf_val > 0 && q_val > 0) ? q_val / sf_val : 1.0;
      }
      last_total += wl_pcounts[i*NUM_WL_BINS+j];
    }
  }
}


void _pp_calc_median_smhm(float *bin, float z, double m, struct smhm_data *sd) {
  int64_t smhm;
  double avg=0, avg2=0;
  memset(sd, 0, sizeof(struct smhm_data));
  sd->z = z;
  sd->m = m;
  for (smhm=0; smhm<SMHM_SMHM_NBINS; smhm++) {
    double sm = m + ((double)smhm+0.5)/(double)SMHM_SMHM_BPDEX + SMHM_SMHM_MIN;
    sd->counts += bin[smhm];
    avg += sm*bin[smhm];
    avg2 += sm*sm*bin[smhm];
  }

  double counts = sd->counts;
  if (!(sd->counts>0)) counts = 1;
  avg /= counts;
  avg2 /= counts;
  sd->sd = avg2 - avg*avg;
  if (sd->sd > 0) sd->sd = sqrt(sd->sd);
  else sd->sd = 0;
  double sdw = (counts > 1) ? sqrt(counts-1) : 0.1;
  sd->err = 1.253*sd->sd / sdw;

  if (counts < 3) sd->sm = avg;
  else {
    double counts2 = 0;
    for (smhm=0; smhm<SMHM_NBINS; smhm++) {
      counts2 += bin[smhm];
      if (counts2 > sd->counts/2.0) {
	double sm = m + ((double)smhm)/(double)SMHM_SMHM_BPDEX + SMHM_SMHM_MIN;
	sd->sm = sm + (sd->counts/2.0 - (counts2-bin[smhm]))/((double)bin[smhm]*(double)SMHM_SMHM_BPDEX);
	break;
      }
    }
  }
}

void pp_calc_smhm_relationships(void) {
  int64_t n, m, type, fit_m, sm;
  float *raws[SMHM_NUM_TYPES] = {smhm, smhm_cen, smhm_sat, smhm_sf, smhm_q, smhm_true, smhm_true_cen, smhm_true_sat, smhm_true_sf, smhm_true_q, smhm_cen_sf, smhm_cen_q, smhm_true_cen_sf, smhm_true_cen_q, smhm_true_icl, smhm_true_cen_icl};
  float *medians[SMHM_NUM_TYPES] = {smhm_med, smhm_med_cen, smhm_med_sat, smhm_med_sf, smhm_med_q, smhm_true_med, smhm_true_med_cen, smhm_true_med_sat, smhm_true_med_sf, smhm_true_med_q, smhm_med_cen_sf, smhm_med_cen_q, smhm_true_med_cen_sf, smhm_true_med_cen_q, smhm_true_icl_med, smhm_true_cen_icl_med};
  float *scatters[SMHM_NUM_TYPES] = {smhm_scatter, smhm_scatter_cen, smhm_scatter_sat, smhm_scatter_sf, smhm_scatter_q, smhm_true_scatter, smhm_true_scatter_cen, smhm_true_scatter_sat, smhm_true_scatter_sf, smhm_true_scatter_q, smhm_scatter_cen_sf, smhm_scatter_cen_q, smhm_true_scatter_cen_sf, smhm_true_scatter_cen_q, smhm_true_icl_scatter, smhm_true_cen_icl_scatter};
  float *params[SMHM_NUM_TYPES] = {smhm_med_params, smhm_med_cen_params, smhm_med_sat_params, smhm_med_sf_params, smhm_med_q_params, smhm_true_med_params, smhm_true_med_cen_params, smhm_true_med_sat_params, smhm_true_med_sf_params, smhm_true_med_q_params, smhm_med_cen_sf_params, smhm_med_cen_q_params, smhm_true_med_cen_sf_params, smhm_true_med_cen_q_params, smhm_true_icl_med_params, smhm_true_cen_icl_med_params};
  float *fits[SMHM_NUM_TYPES] = {smhm_med_bestfit, smhm_med_cen_bestfit, smhm_med_sat_bestfit, smhm_med_sf_bestfit, smhm_med_q_bestfit, smhm_true_med_bestfit, smhm_true_med_cen_bestfit, smhm_true_med_sat_bestfit, smhm_true_med_sf_bestfit, smhm_true_med_q_bestfit, smhm_med_cen_sf_bestfit, smhm_med_cen_q_bestfit, smhm_true_med_cen_sf_bestfit, smhm_true_med_cen_q_bestfit, smhm_true_icl_med_bestfit, smhm_true_cen_icl_med_bestfit};
  float *ratios[SMHM_NUM_TYPES] = {NULL, smhm_med_cen_ratio, smhm_med_sat_ratio, smhm_med_sf_ratio, smhm_med_q_ratio, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};
  float *ratios_fit[SMHM_NUM_TYPES] = {NULL, smhm_med_cen_bestfit_ratio, smhm_med_sat_bestfit_ratio, smhm_med_sf_bestfit_ratio, smhm_med_q_bestfit_ratio, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

  struct background_smhm bgs[SMHM_NUM_TYPES] = {{{0}}};
  
  struct smhm_data *sd = NULL;
  check_realloc_s(sd, sizeof(struct smhm_data), SMHM_NUM_ZS*SMHM_HM_NBINS);
  int64_t num_sds = 0;

  double w=0, a=0, sw=0, as=0;
  assert(!(SMHM_HM_BPDEX % SMHM_FIT_BPDEX));
  
  for (type=0; type<SMHM_NUM_TYPES; type++) {
    num_sds = 0;
    memset(medians[type], 0, sizeof(float)*SMHM_FIT_NBINS*SMHM_NUM_ZS);
    memset(scatters[type], 0, sizeof(float)*SMHM_FIT_NBINS*SMHM_NUM_ZS);
    for (n=0; n<SMHM_NUM_ZS; n++) {
      for (m=0; m<SMHM_HM_NBINS; m++) {
	float *bin = raws[type] + n*SMHM_NBINS + m*SMHM_SMHM_NBINS;
	double mh = SMHM_HM_MIN+((double)m+0.5) / (double)SMHM_HM_BPDEX;
	_pp_calc_median_smhm(bin, 1.0/smhm_scales[n]-1.0, mh, sd+num_sds);

	
	// Downsampling for fitting
	if (sd[num_sds].counts>5 && sd[num_sds].sd > 1e-3) {
	  w += 1.0/sd[num_sds].err;
	  a += (sd[num_sds].sm - mh)/sd[num_sds].err;
	  sw += 1.0/sqrt(sd[num_sds].counts);
	  as += sd[num_sds].sd/sqrt(sd[num_sds].counts);
	}
	if (!((m+1)%(SMHM_HM_BPDEX / SMHM_FIT_BPDEX))) {
	  fit_m = m / (SMHM_HM_BPDEX / SMHM_FIT_BPDEX);
	  if (w>0) a/=w;
	  else a = 0;
	  medians[type][SMHM_FIT_NBINS*n+fit_m] = a;
	  if (sw>0) as /= sw;
	  else as = 0;
	  scatters[type][SMHM_FIT_NBINS*n+fit_m] = as;
	  w = a = sw = as = 0;
	}

	
	if (type==0 && sd[num_sds].counts > 5) {
	  fit_m = m / (SMHM_HM_BPDEX / SMHM_FIT_BPDEX);
	  int64_t median_bin = ((sd[num_sds].sm - mh) - SMHM_SMHM_MIN)*SMHM_SMHM_BPDEX;
	  for (sm=0; sm<SMHM_SMHM_NBINS; sm++) {
	    int64_t scatter_bin = sm - median_bin + SMHM_SMHM_NBINS/2;
	    if (scatter_bin < 0) scatter_bin = 0;
	    if (scatter_bin > SMHM_SMHM_NBINS-1) scatter_bin = SMHM_SMHM_NBINS-1;
	    smhm_scatter_pdf[SMHM_FIT_NBINS*SMHM_SMHM_NBINS*n+fit_m*SMHM_SMHM_NBINS+scatter_bin] +=
	      smhm[SMHM_FIT_NBINS*SMHM_SMHM_NBINS*n+fit_m*SMHM_SMHM_NBINS+sm];
	  }

	  if (!((m+1)%(SMHM_HM_BPDEX / SMHM_FIT_BPDEX))) { //Renormalize SMHM Scatter
	    double counts = 0;
	    for (sm=0; sm<SMHM_SMHM_NBINS; sm++) counts += smhm_scatter_pdf[SMHM_FIT_NBINS*SMHM_SMHM_NBINS*n+fit_m*SMHM_SMHM_NBINS+sm];
	    if (counts > 0) {
	      counts /= 100.0;
	      for (sm=0; sm<SMHM_SMHM_NBINS; sm++) smhm_scatter_pdf[SMHM_FIT_NBINS*SMHM_SMHM_NBINS*n+fit_m*SMHM_SMHM_NBINS+sm] /= counts;
	    }
	  }
	}

	if (sd[num_sds].counts>5 && sd[num_sds].sd > 1e-3 && mh > SIM_RES_LIMIT_HM)
	  num_sds++;
      }
    }
    
    //MCMC fit
    bgs[type] = fork_smhm_sampler(sd, num_sds, params[type], fits[type], medians[type]);
  }

  for (type=0; type<SMHM_NUM_TYPES; type++) {
    smhm_sampler_result(bgs[type]);
    //Calc ratios
    if (type>0) {
      for (n=0; n<SMHM_NUM_ZS*SMHM_FIT_NBINS; n++) {
	if (medians[0][n] && medians[type][n] && ratios[type]) ratios[type][n] = medians[type][n]-medians[0][n];
	if (fits[0][n] && fits[type][n] && ratios_fit[type]) ratios_fit[type][n] = fits[type][n]-fits[0][n];
	if (type==2 && medians[1][n] && medians[2][n]) {//Satellites
	  smhm_med_cen_sat_ratio[n] = medians[1][n]-medians[2][n];
	  smhm_med_bestfit_cen_sat_ratio[n] = fits[1][n]-fits[2][n];
	}
	if (type==4 && medians[3][n] && medians[4][n]) {//Quiescent
	  smhm_med_sf_q_ratio[n] = medians[3][n]-medians[4][n];
	  smhm_med_bestfit_sf_q_ratio[n] = fits[3][n]-fits[4][n];
	}
      }
    }
  }  
  free(sd);
}

void pp_reduce_bins(float *smf, float *smf_q, float *red_frac) {
  int64_t i, j;
  assert(!(SM_BPDEX % SM_RED_BPDEX));
  double counts=0, counts_q = 0;
  for (i=0; i<SM_NBINS; i++) {
    counts += smf[i];
    counts_q += smf_q[i];
    if (((i+1)%(SM_BPDEX/SM_RED_BPDEX))==0) {
      if (!counts) counts = 1;
      j = i/(SM_BPDEX/SM_RED_BPDEX);
      red_frac[j] = counts_q / counts;
      counts = counts_q = 0;
    }
  }
}

void pp_calc_sfh_stats(void) {
  int64_t i, j, k;
  for (i=0; i<num_scales; i++) {
    int64_t offset = SM_RED_NBINS*i;
    pp_reduce_bins(smf+SM_NBINS*i, smf_q+SM_NBINS*i, sm_fq+offset);
    pp_reduce_bins(smf+SM_NBINS*i, smf_q3+SM_NBINS*i, sm_fq3+offset);
    pp_reduce_bins(smf+SM_NBINS*i, ssfr+SM_NBINS*i, sm_ssfr+offset);
    for (j=0; j<SM_RED_NBINS; j++) {
#define setup_counts(a,b) double a = b[j+offset]; if (!a) a=1;
#define setup_counts_diff(a,b,c) double a = b[j+offset] - c[j+offset]; if (!a) a=1;
      setup_counts(counts, sm_red);
      setup_counts(counts_cen, sm_cen);
      setup_counts_diff(counts_sat, sm_red, sm_cen);
      setup_counts(counts_sat_mw, sm_sat_mw);
      setup_counts(counts_sat_gr, sm_sat_gr);
      setup_counts(counts_sat_cl, sm_sat_cl);
      setup_counts_diff(counts_sf, sm_red, sm_red_q2);
      setup_counts(counts_q, sm_red_q2);
      setup_counts(counts_sat_q_mw, sm_q_sat_mw);
      setup_counts(counts_sat_q_gr, sm_q_sat_gr);
      setup_counts(counts_sat_q_cl, sm_q_sat_cl);
      setup_counts(counts_cen_q, sm_q_cen);
      setup_counts_diff(counts_cen_sf, sm_cen, sm_q_cen);
      
      sm_sat_ratio[j+offset] = (sm_red[j+offset]-sm_cen[j+offset])/counts;
      sm_fq2[j+offset] = sm_red_q2[j+offset]/counts;
      sm_ex_situ_ratio[j+offset] = sm_ex_situ[j+offset]/counts;
      sm_rejuv_ratio[j+offset] = sm_rejuv[j+offset]/counts;
      sm_q_cen_ratio[j+offset] = sm_q_cen[j+offset]/counts_cen;
      sm_q_sat_ratio[j+offset] = sm_q_sat[j+offset]/counts_sat;
      sm_q_sat_mw_ratio[j+offset] = sm_q_sat_mw[j+offset]/counts_sat_mw;
      sm_q_sat_gr_ratio[j+offset] = sm_q_sat_gr[j+offset]/counts_sat_gr;
      sm_q_sat_cl_ratio[j+offset] = sm_q_sat_cl[j+offset]/counts_sat_cl;
      sm_q_cen_sat_diff[j+offset] = (counts_sat) ? (sm_q_sat_ratio[j+offset] - sm_q_cen_ratio[j+offset]) : 0;
      sm_q_cen_sat_mw_diff[j+offset] = (counts_sat_mw) ? (sm_q_sat_mw_ratio[j+offset] - sm_q_cen_ratio[j+offset]) : 0;
      sm_q_cen_sat_gr_diff[j+offset] = (counts_sat_gr) ? (sm_q_sat_gr_ratio[j+offset] - sm_q_cen_ratio[j+offset]) : 0;
      sm_q_cen_sat_cl_diff[j+offset] = (counts_sat_cl) ? (sm_q_sat_cl_ratio[j+offset] - sm_q_cen_ratio[j+offset]) : 0;
      sm_fq_sat_mw_infall_ratio[j+offset] = sm_fq_sat_mw_infall[j+offset]/counts_sat_q_mw;
      sm_fq_sat_gr_infall_ratio[j+offset] = sm_fq_sat_gr_infall[j+offset]/counts_sat_q_gr;
      sm_fq_sat_cl_infall_ratio[j+offset] = sm_fq_sat_cl_infall[j+offset]/counts_sat_q_cl;

      mean_hm[j+offset] = total_hm[j+offset]/counts;
      mean_log_hm[j+offset] = total_log_hm[j+offset]/counts;
      mean_wl_hm[j+offset] = pow(total_wl_hm[j+offset]/counts, 3.0/2.0);
      mean_cen_hm[j+offset] = total_cen_hm[j+offset]/counts_cen;
      mean_cen_log_hm[j+offset] = total_cen_log_hm[j+offset]/counts_cen;
      mean_cen_wl_hm[j+offset] = pow(total_cen_wl_hm[j+offset]/counts_cen, 3.0/2.0);
      mean_cen_sf_hm[j+offset] = total_cen_sf_hm[j+offset]/counts_cen_sf;
      mean_cen_sf_log_hm[j+offset] = total_cen_sf_log_hm[j+offset]/counts_cen_sf;
      mean_cen_sf_wl_hm[j+offset] = pow(total_cen_sf_wl_hm[j+offset]/counts_cen_sf, 3.0/2.0);
      mean_cen_q_hm[j+offset] = total_cen_q_hm[j+offset]/counts_cen_q;
      mean_cen_q_log_hm[j+offset] = total_cen_q_log_hm[j+offset]/counts_cen_q;
      mean_cen_q_wl_hm[j+offset] = pow(total_cen_q_wl_hm[j+offset]/counts_cen_q, 3.0/2.0);
      mean_sat_hm[j+offset] = total_sat_hm[j+offset]/counts_sat;
      mean_sat_log_hm[j+offset] = total_sat_log_hm[j+offset]/counts_sat;
      mean_sat_wl_hm[j+offset] = pow(total_sat_wl_hm[j+offset]/counts_sat, 3.0/2.0);
      mean_sf_hm[j+offset] = total_sf_hm[j+offset]/counts_sf;
      mean_sf_log_hm[j+offset] = total_sf_log_hm[j+offset]/counts_sf;
      mean_sf_wl_hm[j+offset] = pow(total_sf_wl_hm[j+offset]/counts_sf, 3.0/2.0);
      mean_q_hm[j+offset] = total_q_hm[j+offset]/counts_q;
      mean_q_log_hm[j+offset] = total_q_log_hm[j+offset]/counts_q;
      mean_q_wl_hm[j+offset] = pow(total_q_wl_hm[j+offset]/counts_q, 3.0/2.0);

      for (k=0; k<num_scales; k++) {
	double a1 = (k>0) ? 0.5*(scales[k]+scales[k-1]) : 0;
	double a2 = (k<num_scales-1) ? 0.5*(scales[k]+scales[k+1]) : scales[k];
	double dt = scale_to_years(a2) - scale_to_years(a1);
	sm_sfh_ratio[(offset+j)*num_scales+k] = sm_sfh[(offset+j)*num_scales+k]/(counts*dt);
	sm_sfh_sf_ratio[(offset+j)*num_scales+k] = sm_sfh_sf[(offset+j)*num_scales+k]/(counts_sf*dt);
	sm_sfh_q_ratio[(offset+j)*num_scales+k] = sm_sfh_q[(offset+j)*num_scales+k]/(counts_q*dt);
	sm_sfh_cen_ratio[(offset+j)*num_scales+k] = sm_sfh_cen[(offset+j)*num_scales+k]/(counts_cen*dt);
	sm_sfh_sat_ratio[(offset+j)*num_scales+k] = sm_sfh_sat[(offset+j)*num_scales+k]/(counts_sat*dt);
      }

      if (i==num_scales-1) {
	for (k=0; k<num_scales; k++) {
	  int64_t cq = sm_mp_q_counts[j*num_scales+k];
	  if (!cq) cq = 1;
	  int64_t csf = sm_mp_sf_counts[j*num_scales+k];
	  if (!csf) csf = 1;
	  sm_mp_q_qf_ratio[j*num_scales+k] = sm_mp_q_qf[j*num_scales+k]/cq;
	  sm_mp_sf_qf_ratio[j*num_scales+k] = sm_mp_sf_qf[j*num_scales+k]/csf;
	}
      }
    }

    offset = HM_M_NBINS*i;
    for (j=0; j<HM_M_NBINS; j++) {
      setup_counts(counts, hm_dist);
      setup_counts(counts_cen, hm_cen_dist);
      setup_counts_diff(counts_sat, hm_dist, hm_cen_dist);
      setup_counts(counts_sat_mw, hm_sat_mw);
      setup_counts(counts_sat_gr, hm_sat_gr);
      setup_counts(counts_sat_cl, hm_sat_cl);
      setup_counts_diff(counts_sf, hm_dist, hm_q);
      setup_counts(counts_q, hm_q);
      setup_counts(counts_sat_q_mw, hm_q_sat_mw);
      setup_counts(counts_sat_q_gr, hm_q_sat_gr);
      setup_counts(counts_sat_q_cl, hm_q_sat_cl);
      setup_counts(counts_hostm, hostm_dist);
#undef setup_counts
#undef setup_counts_diff
		   
      hm_sat_ratio[j+offset] = (hm_dist[j+offset]-hm_cen_dist[j+offset])/counts;
      hm_ex_situ_ratio[j+offset] = hm_ex_situ[j+offset]/counts;
      hm_rejuv_ratio[j+offset] = hm_rejuv[j+offset]/counts;
      hm_q_ratio[j+offset] = hm_q[j+offset]/counts;
      hm_q_cen_ratio[j+offset] = hm_q_cen[j+offset]/counts_cen;
      hm_q_sat_ratio[j+offset] = hm_q_sat[j+offset]/counts_sat;
      hm_q_sat_mw_ratio[j+offset] = hm_q_sat_mw[j+offset]/counts_sat_mw;
      hm_q_sat_gr_ratio[j+offset] = hm_q_sat_gr[j+offset]/counts_sat_gr;
      hm_q_sat_cl_ratio[j+offset] = hm_q_sat_cl[j+offset]/counts_sat_cl;
      hm_q_cen_sat_diff[j+offset] = (counts_sat) ? (hm_q_sat_ratio[j+offset] - hm_q_cen_ratio[j+offset]) : 0;
      hm_q_cen_sat_mw_diff[j+offset] = (counts_sat_mw) ? (hm_q_sat_mw_ratio[j+offset] - hm_q_cen_ratio[j+offset]) : 0;
      hm_q_cen_sat_gr_diff[j+offset] = (counts_sat_gr) ? (hm_q_sat_gr_ratio[j+offset] - hm_q_cen_ratio[j+offset]) : 0;
      hm_q_cen_sat_cl_diff[j+offset] = (counts_sat_cl) ? (hm_q_sat_cl_ratio[j+offset] - hm_q_cen_ratio[j+offset]) : 0;
      hm_fq_sat_mw_infall_ratio[j+offset] = hm_fq_sat_mw_infall[j+offset]/counts_sat_q_mw;
      hm_fq_sat_gr_infall_ratio[j+offset] = hm_fq_sat_gr_infall[j+offset]/counts_sat_q_gr;
      hm_fq_sat_cl_infall_ratio[j+offset] = hm_fq_sat_cl_infall[j+offset]/counts_sat_q_cl;

      mean_log_sm[j+offset] = total_log_sm[j+offset]/counts;
      mean_log_smhm_ratio[j+offset] = total_log_smhm_ratio[j+offset]/counts;
      mean_log_true_sm[j+offset] = total_log_true_sm[j+offset]/counts;
      mean_log_true_sm_icl[j+offset] = total_log_true_sm_icl[j+offset]/counts;
      mean_log_true_smhm_ratio[j+offset] = total_log_true_smhm_ratio[j+offset]/counts;
      mean_log_true_smhm_icl_ratio[j+offset] = total_log_true_smhm_icl_ratio[j+offset]/counts;
      mean_sm_all_enclosed[j+offset] = total_sm_all_enclosed[j+offset]/counts_hostm;
      mean_sm_central_enclosed[j+offset] = total_sm_central_enclosed[j+offset]/counts_hostm;
      mean_sm_icl_central_enclosed[j+offset] = total_sm_icl_central_enclosed[j+offset]/counts_hostm;
      
      for (k=0; k<num_scales; k++) {
	double a1 = (k>0) ? 0.5*(scales[k]+scales[k-1]) : 0;
	double a2 = (k<num_scales-1) ? 0.5*(scales[k]+scales[k+1]) : scales[k];
	double dt = scale_to_years(a2) - scale_to_years(a1);
	hm_sfh_ratio[(offset+j)*num_scales+k] = hm_sfh[(offset+j)*num_scales+k]/(counts*dt);
	hm_sfh_sf_ratio[(offset+j)*num_scales+k] = hm_sfh_sf[(offset+j)*num_scales+k]/(counts_sf*dt);
	hm_sfh_q_ratio[(offset+j)*num_scales+k] = hm_sfh_q[(offset+j)*num_scales+k]/(counts_q*dt);
	hm_sfh_cen_ratio[(offset+j)*num_scales+k] = hm_sfh_cen[(offset+j)*num_scales+k]/(counts_cen*dt);
	hm_sfh_sat_ratio[(offset+j)*num_scales+k] = hm_sfh_sat[(offset+j)*num_scales+k]/(counts_sat*dt);
      }

      if (i==num_scales-1) {
	for (k=0; k<num_scales; k++) {
	  int64_t cq = mp_q_counts[j*num_scales+k];
	  if (!cq) cq = 1;
	  int64_t csf = mp_sf_counts[j*num_scales+k];
	  if (!csf) csf = 1;
	  mp_q_qf_ratio[j*num_scales+k] = mp_q_qf[j*num_scales+k]/cq;
	  mp_sf_qf_ratio[j*num_scales+k] = mp_sf_qf[j*num_scales+k]/csf;
	}
      }
    }
  }
}


void pp_find_median(float *bin, int64_t num_bins, float *median, float *up, float *dn, float min, float bpunit) {
  double t = 0, t2 = 0;
  double up_perc = 0.5+0.5*erf(1.0/sqrt(2.0));
  double dn_perc = 0.5+0.5*erf(-1.0/sqrt(2.0));
  int64_t i;
  *median = *up = *dn = min;
  for (i=0; i<num_bins; i++) {
    t += bin[i];
    if (!(bin[i]>=0)) return;
  }
  if (!(t>0)) return;
  
  for (i=0; i<num_bins; i++) {
    t2 += bin[i];
    if (t2 > t*0.5 && (*median==min)) {
      *median = (double)i + (bin[i] - (t2-t*0.5))/bin[i];
      *median = min + *median/bpunit;
    }
    if (t2 > t*dn_perc && (*dn==min)) {
      *dn = (double)i + (bin[i] - (t2-t*dn_perc))/bin[i];
      *dn = min + *dn/bpunit;
    }
    if (t2 > t*up_perc && (*up==min)) {
      *up = (double)i + (bin[i] - (t2-t*up_perc))/bin[i];
      *up = min + *up/bpunit;
    }
  }
}

float pp_bin_to_scale(float bin) {
  int64_t i=bin;
  double f = bin-i;
  if (i<0) return scales[0];
  if (i>=num_scales-1) return scales[num_scales-1];
  return (scales[i]+f*(scales[i+1]-scales[i]));
}

void pp_bin_to_dt(float *bin, float scale_now) {
  float scale = pp_bin_to_scale(*bin);
  *bin = 1e-9*(scale_to_years(scale_now)-scale_to_years(scale));
}

void pp_calc_infall_stats(void) {
  int64_t i, j, k;
  for (i=0; i<num_scales; i++) {
    for (j=0; j<SM_RED_NBINS; j++) {
      int64_t offset = i*TS_NBINS*SM_RED_NBINS + j*TS_NBINS;
      float *med = sm_td_sat_mw_infall_med + 3*(i*SM_RED_NBINS+j);
      pp_find_median(sm_td_sat_mw_infall+offset, TS_NBINS, med, med+1, med+2, TS_MIN, TS_BPGYR);
      med = sm_td_sat_gr_infall_med + 3*(i*SM_RED_NBINS+j);
      pp_find_median(sm_td_sat_gr_infall+offset, TS_NBINS, med, med+1, med+2, TS_MIN, TS_BPGYR);
      med = sm_td_sat_cl_infall_med + 3*(i*SM_RED_NBINS+j);
      pp_find_median(sm_td_sat_cl_infall+offset, TS_NBINS, med, med+1, med+2, TS_MIN, TS_BPGYR);

      offset = i*num_scales*SM_RED_NBINS + j*num_scales;
      med = sm_ti_sat_mw_infall_med + 3*(i*SM_RED_NBINS+j);
      //84th and 16th percentiles reversed by conversion to delay times.
      pp_find_median(sm_ti_sat_mw_infall+offset, num_scales, med, med+2, med+1, 0, 1);
      med = sm_ti_sat_gr_infall_med + 3*(i*SM_RED_NBINS+j);
      pp_find_median(sm_ti_sat_gr_infall+offset, num_scales, med, med+2, med+1, 0, 1);
      med = sm_ti_sat_cl_infall_med + 3*(i*SM_RED_NBINS+j);
      pp_find_median(sm_ti_sat_cl_infall+offset, num_scales, med, med+2, med+1, 0, 1);
    }
    for (k=0; k<SM_RED_NBINS*3; k++) {
      pp_bin_to_dt(sm_ti_sat_mw_infall_med+i*SM_RED_NBINS*3+k, scales[i]);
      pp_bin_to_dt(sm_ti_sat_gr_infall_med+i*SM_RED_NBINS*3+k, scales[i]);
      pp_bin_to_dt(sm_ti_sat_cl_infall_med+i*SM_RED_NBINS*3+k, scales[i]);
    }
  }

  for (i=0; i<SMHM_NUM_ZS; i++) {
    for (j=0; j<SM_RED_NBINS; j++) {
      int64_t offset = i*SM_RED_NBINS*SSFR_SSFR_NBINS + j*SSFR_SSFR_NBINS;
      float *med = sm_ssfr_sat_mw_infall_med + 3*(i*SM_RED_NBINS+j);
      pp_find_median(sm_ssfr_sat_mw_infall + offset, SSFR_SSFR_NBINS, med, med+1, med+2, SSFR_SSFR_MIN, SSFR_SSFR_BPDEX);
      med = sm_ssfr_sat_gr_infall_med + 3*(i*SM_RED_NBINS+j);
      pp_find_median(sm_ssfr_sat_gr_infall + offset, SSFR_SSFR_NBINS, med, med+1, med+2, SSFR_SSFR_MIN, SSFR_SSFR_BPDEX);
      med = sm_ssfr_sat_cl_infall_med + 3*(i*SM_RED_NBINS+j);
      pp_find_median(sm_ssfr_sat_cl_infall + offset, SSFR_SSFR_NBINS, med, med+1, med+2, SSFR_SSFR_MIN, SSFR_SSFR_BPDEX);
    }
  }
}

void pp_calc_sfr_ratios(void) {
  int64_t i;
  memset(uv_csfr_ratio, 0, sizeof(float)*num_scales);
  for (i=0; i<num_scales; i++) {
    if (!(csfr[i]>0)) continue;
    uv_csfr_ratio[i] = csfr_uv[i] / csfr[i];
  }

  memset(uv_sfr_av_sfr, 0, sizeof(float)*num_scales*UV_SFR_NBINS);
  memset(uv_sfr_17, 0, sizeof(float)*num_scales);
  for (i=0; i<num_scales*UV_SFR_NBINS; i++)
    if (uv_sfr_counts[i])
      uv_sfr_av_sfr[i] = uv_sfr_tot_sfr[i]/uv_sfr_counts[i];
  double f = (-17.0 - UV_SFR_MIN)*UV_SFR_BPMAG;
  f -= 0.5;
  int64_t b = f;
  f -= b;
  for (i=0; i<num_scales; i++) {
    int64_t nb = i*UV_SFR_NBINS+b;
    uv_sfr_17[i] = uv_sfr_av_sfr[nb] + f*(uv_sfr_av_sfr[nb+1]-uv_sfr_av_sfr[nb]);
  }
}

void inline_postprocessing(void) {
  if (!POSTPROCESSING_MODE) return;
  pp_calc_corr();
  pp_calc_wl();
  pp_calc_smhm_relationships();
  pp_calc_sfh_stats();
  pp_calc_infall_stats();
  pp_calc_sfr_ratios();
}



