#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <stdint.h>
#include <sys/mman.h>
#include "check_syscalls.h"
#include "corr_lib.h"
#include "stringparse.h"
#include "make_sf_catalog.h"
#include "config.h"
#include "config_vars.h"
#include "mt_rand.h"

FILE *logfile; //Unused

#define FAST3TREE_TYPE   struct catalog_galaxy
#define FAST3TREE_PREFIX SAMPLE_CORR
#define FAST3TREE_DIM    2
#include "fast3tree.c"


int main(int argc, char **argv)
{
  FILE *in=NULL;
  char buffer[1024];
  int64_t i, j;

  if (argc < 6) {
    fprintf(stderr, "Usage: %s sm_low sm_high pi_max scale corr_galaxies.txt ...\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  float sm_low = atof(argv[1]);
  float sm_high = atof(argv[2]);
  if (sm_low > 15) { sm_low=log10(sm_low); }
  if (sm_high > 15) { sm_high=log10(sm_high); }
  CORR_LENGTH_PI = atof(argv[3]);
  double a = atof(argv[4]);
  float translate_const = -0.49 + 1.07 * ((1.0/a-1.0) - 0.1);


  struct catalog_galaxy *galaxies = NULL;
  struct catalog_galaxy *galaxies2 = NULL;

  clear_corrs();
  int64_t nq=0, na=0, nsf=0;

  for (i=5; i<argc; i++) {
    int64_t ng = 0, num_g2 = 0;

    in = check_fopen(argv[i], "r");
    struct catalog_galaxy tg = {0};
    while (fgets(buffer, 1024, in)) {
      int64_t external;
      if (sscanf(buffer, "%f %f %f %f %f %"PRId64, tg.pos, tg.pos+1, tg.pos+2, &tg.lsm, &tg.lsfr, &external) < 6) continue;
      if (tg.lsm < sm_low || tg.lsm > sm_high) continue;
      translate_sfrs(&tg, translate_const);      
      
      if (!external) {
	check_realloc_every(galaxies, sizeof(struct catalog_galaxy), ng, 1000);
	galaxies[ng] = tg;
	ng++;
      }

      check_realloc_every(galaxies2, sizeof(struct catalog_galaxy), num_g2, 1000);
      galaxies2[num_g2] = tg;
      num_g2++;
    }
    fclose(in);

    init_dists();
    init_corr_tree(galaxies2, num_g2);

    for (j=0; j<ng; j++) {
      na++;
      bin_single_galaxy(galaxies+j);
      if (galaxies[j].lsfr > 0) nsf++;
      else nq++;
    }
  }

  printf("#NA: %"PRId64"; NSF: %"PRId64"; NQ: %"PRId64"\n", na, nsf, nq);
  float vol = pow(250, 3);
  float da = na/vol, dsf = nsf/vol, dq=nq/vol;
  printf("#Vol: %g\n", vol);
  printf("#R C(all-all) C(sf-sf) C(q-q)\n");
  if (na < 1) na = 1;
  if (nq < 1) nq = 1;
  if (nsf < 1) nsf = 1;
  for (j=0; j<NUM_BINS; j++) {
    float a1 = corr_dists[j]*M_PI;
    float a2 = corr_dists[j+1]*M_PI;
    float a = a2-a1;
    float av_r = sqrt(0.5*(a1+a2)/M_PI);
    float raa = 2.0*da*a*CORR_LENGTH_PI;
    float rasf = 2.0*dsf*a*CORR_LENGTH_PI;
    float raq = 2.0*dq*a*CORR_LENGTH_PI;
    printf("%f %e %e %e %e %g %g %g %g\n", av_r, (corr[j]-na*raa)/(na*da*a), (corr_sf[j]-nsf*rasf)/(nsf*dsf*a), (corr_q[j]-nq*raq)/(nq*dq*a), (float)corr[j], da, a, raa, (float)corr_bin_of(av_r*av_r));
  }
  return 0;
}


