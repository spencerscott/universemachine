#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <math.h>
#include <inttypes.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "stringparse.h"
#include "universal_constants.h"
#include "stats.h"
#include "mf_cache.h"

struct sf_model_allz *ms = NULL;
int64_t num_ms = 0;
double *tot_csfrs=NULL, *bright_csfrs=NULL, *csfr_ratio=NULL, *sfr_slopes=NULL, *last_log_sfr=NULL;

struct sf_model *cs = NULL;

int sort_double(const void *a, const void *b) {
  const double *c = a;
  const double *d = b;
  if (*c < *d) return -1;
  if (*d < *c) return 1;
  return 0;
}

double doexp10(double x) {
  return exp(M_LN10*x);
}

double sfr_at_vmp(double lv, struct sf_model c) {
  double vd = lv - c.v_1;
  double vd2 = vd/c.delta;
  return (c.epsilon * (1.0/(doexp10(c.alpha*vd) + doexp10(c.beta*vd)) + c.gamma*exp(-0.5*vd2*vd2)));
}

double max_mass(double z) {
  return 13.5351-0.23712*z+2.0187*exp(-z/4.48394); //evolving from 1e-9 @ z=0 to 1e-7 @ z=10
}

double mass_to_vmax(double m, double z);

void calc_models_at_z(double z, double label, FILE *fcsfrs) {
  //double sfr_lim = 0;
  int64_t j,i;
  double a = 1.0/(1.0+z);
  for (i=0; i<num_ms; i++) {
    cs[i] = calc_sf_model(ms[i], a);
    double scatter = sqrt(cs[i].sig_sf*cs[i].sig_sf + cs[i].sig_sf_uncorr*cs[i].sig_sf_uncorr);
    cs[i].obs_sfr_offset = cs[i].obs_sfr_offset_lin*exp(pow((scatter*log(10)),2.0)/2.0); //Scatter correction
  }

  double a4 = (z>4) ? a : (0.2);
  double kfuv = 5.1e-29*(1.0+exp(-20.79*a4+0.98));
  double sfr_thresh = log10(2.74117778e27*kfuv);
  double erf_norm = 1.0/(sqrt(2)*0.3);
  double m_max = max_mass(z);
  memset(tot_csfrs, 0, sizeof(double)*num_ms);
  memset(bright_csfrs, 0, sizeof(double)*num_ms);
  memset(csfr_ratio, 0, sizeof(double)*num_ms);
  memset(last_log_sfr, 0, sizeof(double)*num_ms);
  double uv_slope_thresh = sfr_thresh + 0.1;
  for (j=160; j<=300; j++) {
    double m = (double)j/20.0;
    if (m > m_max) break;
    double vmax = mass_to_vmax(m, z);
    double nd = pow(10, mf_cache(a, m));
    for (i=0; i<num_ms; i++) {
      double sfr2 = sfr_at_vmp(vmax, cs[i]);
      //double sfr = log10(sfr2);
      //double sfr3 = sfr;
      //if (sfr < sfr_lim) sfr = sfr_lim;
      double fq = 0.5+0.5*erf((vmax-cs[i].q_lvmp)/(cs[i].q_sig_lvmp*sqrt(2)));
      fq = cs[i].fq_min + (1.0-cs[i].fq_min)*fq;

      double fsf = 1.0 - fq;
      sfr2 *= fsf*cs[i].obs_sfr_offset; //Scatter correction
      double lsfr = (sfr2 > 0) ? log10(sfr2) : -100;
      tot_csfrs[i] += sfr2*nd/20.0;
      bright_csfrs[i] += sfr2*nd/20.0*(0.5+0.5*erf(erf_norm*(lsfr-sfr_thresh)));

      if (last_log_sfr[i] < uv_slope_thresh && lsfr > uv_slope_thresh) {
	double thresh_m = m + (uv_slope_thresh-lsfr)*(1.0/20.0)/(lsfr-last_log_sfr[i]);
	double m1 = thresh_m - 0.1;
	double m2 = thresh_m + 0.1;
	double vmax1 = mass_to_vmax(m1, z);
	double vmax2 = mass_to_vmax(m2, z);
	double sfr1 = sfr_at_vmp(vmax1, cs[i]);
	double sfr2 = sfr_at_vmp(vmax2, cs[i]);
	double fq1 = 0.5+0.5*erf((vmax1-cs[i].q_lvmp)/(cs[i].q_sig_lvmp*sqrt(2)));
	fq1 = cs[i].fq_min + (1.0-cs[i].fq_min)*fq1;
	double fq2 = 0.5+0.5*erf((vmax2-cs[i].q_lvmp)/(cs[i].q_sig_lvmp*sqrt(2)));
	fq2 = cs[i].fq_min + (1.0-cs[i].fq_min)*fq2;
	double fsf1 = 1.0 - fq1;
	double fsf2 = 1.0 - fq2;
	sfr1 *= fsf1*cs[i].obs_sfr_offset; //Scatter correction
	sfr2 *= fsf2*cs[i].obs_sfr_offset; //Scatter correction
	double lsfr1 = (sfr1 > 0) ? log10(sfr1) : -100;
	double lsfr2 = (sfr2 > 0) ? log10(sfr2) : -100;
	double sfr_slope = (lsfr2-lsfr1)*5.0;
	double nd_slope = (mf_cache(a, m2) - mf_cache(a, m1))*5.0;
	if (sfr_slope != 0) sfr_slope = 1.0/sfr_slope;
	sfr_slopes[i] = nd_slope + sfr_slope - 1.0;
      }
      last_log_sfr[i] = lsfr;
    }
  }
  for (i=0; i<num_ms; i++)
    if (bright_csfrs[i]) csfr_ratio[i] = tot_csfrs[i]/bright_csfrs[i];

#define ONE_S_WIDTH(x) (0.5*(x[(int64_t)(num_ms*0.5*(1.0+ONE_SIGMA))] - x[(int64_t)(num_ms*0.5*(1.0-ONE_SIGMA))]))
  int64_t up = num_ms*0.5*(1.0+ONE_SIGMA);
  int64_t dn = num_ms*0.5*(1.0-ONE_SIGMA);
  //int64_t med = num_ms*0.5;
  fprintf(fcsfrs, "%lg %lg ", label, tot_csfrs[0]);
  double bf = tot_csfrs[0];
  qsort(tot_csfrs, num_ms, sizeof(double), sort_double);
  fprintf(fcsfrs, "%lg %lg ", tot_csfrs[up]-bf, bf-tot_csfrs[dn]);
  
  fprintf(fcsfrs, "%lg ", bright_csfrs[0]);
  bf = bright_csfrs[0];
  qsort(bright_csfrs, num_ms, sizeof(double), sort_double);
  fprintf(fcsfrs, "%lg %lg ", bright_csfrs[up]-bf, bf-bright_csfrs[dn]);

  fprintf(fcsfrs, "%lg ", csfr_ratio[0]);
  bf = csfr_ratio[0];
  qsort(csfr_ratio, num_ms, sizeof(double), sort_double);
  fprintf(fcsfrs, "%lg %lg ", csfr_ratio[up]-bf, bf-csfr_ratio[dn]);

  fprintf(fcsfrs, "%lg ", sfr_slopes[0]);
  bf = sfr_slopes[0];
  qsort(sfr_slopes, num_ms, sizeof(double), sort_double);
  fprintf(fcsfrs, "%lg %lg ", sfr_slopes[up]-bf, bf-sfr_slopes[dn]);


  fprintf(fcsfrs, "\n");
  fflush(fcsfrs);
}

int main(int argc, char **argv) {
  int64_t i;
  char buffer[2048];
  struct sf_model_allz m = {{0}}, b={{0}};
  enum parsetype types[NUM_PARAMS+1];
  void *data[NUM_PARAMS+1];

  if (argc < 2) {
    printf("Usage: %s mf_bolshoi_planck.dat < parameters.dat\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  load_mf_cache(argv[1], argc);
  
  SHORT_PARSETYPE;
  for (i=0; i<NUM_PARAMS+1; i++) {
    types[i] = F64;
    data[i] = &(m.params[i]);
  }
  data[NUM_PARAMS] = &(CHI2(m));
  while (fgets(buffer, 2048, stdin)) {
    if (stringparse(buffer, data, types, NUM_PARAMS+1)<NUM_PARAMS+1) continue;
    check_realloc_every(ms, sizeof(struct sf_model_allz), num_ms, 1000);
    ms[num_ms] = m;
    num_ms++;
    if (!CHI2(b) || CHI2(m) < CHI2(b)) {
      b=m;
      ms[num_ms-1] = ms[0];
      ms[0] = b;
    }
  }

  check_realloc_s(cs, sizeof(struct sf_model), num_ms);
  check_realloc_s(bright_csfrs, sizeof(double), num_ms);
  check_realloc_s(tot_csfrs, sizeof(double), num_ms);
  check_realloc_s(csfr_ratio, sizeof(double), num_ms);
  check_realloc_s(last_log_sfr, sizeof(double), num_ms);
  check_realloc_s(sfr_slopes, sizeof(double), num_ms);

  FILE *fcsfrs = check_fopen("integrated_csfrs.dat", "w");
  fprintf(fcsfrs, "#1+z Total_CSFR(Msun/yr) Err+ Err- Bright_CSFR(UV<-17) Err+ Err- Tot/Bright_CSFR Err+ Err-\n");
  for (i=0; i<=402; i++) {
    double l1pz = (double)i / (double)250.0;
    double z = pow(10, l1pz)-1.0;
    calc_models_at_z(z, 1.0+z, fcsfrs);
    printf("Calculated %"PRId64" models at z=%f\n", num_ms, z);
  }

  fclose(fcsfrs);
  return 0;
}


double mass_to_vmax (double m, double z) {
  double a = 1.0/(1.0+z);
  double a200 = a / 0.3782;
  double m200 = log10((1.0/0.68)*1.115e12 / ((pow(a200, -0.142441) + pow(a200, -1.78959))));
  double v200 = log10(200);
  double exp_vp = (m - m200)/3.0 + v200;
  return exp_vp;
}
