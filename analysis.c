#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <time.h>
#include <inttypes.h>
#include <math.h>
#include <assert.h>
#include <sys/mman.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include "config_vars.h"
#include "config.h"
#include "check_syscalls.h"
#include "make_sf_catalog.h"

extern struct catalog_halo *halos;
int64_t nh=0, ng=0;
FILE *logfile; //Unused
float SCALE_NOW = 0;

int64_t CATALOG_MODE = 0;

void analyze(float z);
void analysis_load_boxes(float target);

#define NUM_ZS 12
float zs[NUM_ZS] = {0.1,0.5,1,2,3,4,5,6,7,8,9,10};

//float zs[NUM_ZS] = {0.1,0.26,0.36,0.46,0.58,0.73,0.92, 1.3, 1.79, 2.28, 2.77, 3.57};

int main(int argc, char **argv) {
  int64_t i;

  if (argc < 2) {
    fprintf(stderr, "Usage: %s config.cfg [mode]\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  do_config(argv[1]);
  if (argc > 2) CATALOG_MODE = atol(argv[2]);
  for (i=0; i<NUM_ZS; i++) {
    analysis_load_boxes(1.0/(1.0+zs[i]));
    analyze(zs[i]);
    nh = 0;
  }
  return 0;
}


FILE *setup_output_file(char *prefix, float z) {
  char buffer[1024];
  sprintf(buffer, "%s/analysis", OUTBASE);
  mkdir(buffer, 0755);
  sprintf(buffer, "%s/analysis/%s_z%g.dat", OUTBASE, prefix, z);
  FILE *out = check_fopen(buffer, "w");
  fprintf(out, "#scale: %g\n", SCALE_NOW);
  return out;
}


void output_map(float z, char *prefix, char *prefix2, int64_t *densmap, float low, float high, int64_t nb, float bpdex, float sm_low, float sm_high, int64_t nsmb, float smbpdex) {
  char buffer[1024];
  sprintf(buffer, "%s/analysis/smhm_map_%s_z%g_%s.txt", OUTBASE, prefix, z, prefix2);
  FILE *output = check_fopen(buffer, "w");
  int64_t i,j;
  
  fprintf(output, "#scale: %g\n", SCALE_NOW);
  fprintf(output, "#Num SM bins: %"PRId64"\n", nsmb);
  fprintf(output, "#Num %s bins: %"PRId64"\n", prefix, nb);
  fprintf(output, "#SM bins: %g - %g (%g bpdex)\n", sm_low, sm_high, smbpdex);
  fprintf(output, "#%s bins: %g - %g (%g bpdex)\n", prefix, low, high, bpdex);
  for (i=0; i<nb; i++) {
    float x = low+(double)i/bpdex;
    for (j=0; j<nsmb; j++) {
      int64_t counts = densmap[i*nsmb+j];
      if (!counts) continue;
      float sm = sm_low + (double)j/(smbpdex);
      fprintf(output, "%f %f %"PRId64"\n", x, sm, counts);
    }
  }
  fclose(output);
}

void smhm_dens_map(float z, struct catalog_halo *th, int64_t num_th, char *prefix) {
  int64_t *densmap_m=NULL, *densmap_v=NULL;
  float sm_low = -4.5;
  float sm_high = -0.5;
  float m_low = 9;
  float m_high = 16;
  float v_low = 1.7;
  float v_high = 3.3;
  int64_t mbpdex = 20;
  int64_t vbpdex = 60;
  int64_t smbpdex = 100;
  int64_t nmb = (m_high-m_low)*mbpdex+1;
  int64_t nvb = (v_high-v_low)*vbpdex+1;
  int64_t nsmb = (sm_high-sm_low)*smbpdex + 1;
  check_realloc_s(densmap_m, sizeof(int64_t), nmb*nsmb);
  memset(densmap_m, 0, sizeof(int64_t)*nmb*nsmb);
  check_realloc_s(densmap_v, sizeof(int64_t), nvb*nsmb);
  memset(densmap_v, 0, sizeof(int64_t)*nvb*nsmb);

  int64_t i;
  for (i=0; i<num_th; i++) {
    if (th[i].flags & IGNORE_FLAG) continue;
    double lsm = (th[i].sm > 1) ? log10(th[i].sm/(th[i].mp/h0)) : 0;
    int64_t smb = (lsm-sm_low)*smbpdex;
    if (smb < 0) smb=0;
    if (smb >= nsmb) smb = nsmb-1;
    double lmp = log10(th[i].mp/h0);
    int64_t mb = (lmp-m_low)*mbpdex+50.5;
    mb -= 50;
    if (mb >= 0 && mb < nmb) densmap_m[mb*nsmb + smb]++;
    
    int64_t vb = (th[i].lvmp-v_low)*vbpdex+50.5;
    vb -= 50;
    if (vb >= 0 && vb < nvb) densmap_v[vb*nsmb + smb]++;
  }
  
  output_map(z, "mpeak", prefix, densmap_m, m_low, m_high, nmb, mbpdex, sm_low, sm_high, nsmb, smbpdex);
  output_map(z, "vmpeak", prefix, densmap_v, v_low, v_high, nvb, vbpdex, sm_low, sm_high, nsmb, smbpdex);
  free(densmap_v);
  free(densmap_m);
}

int sort_by_tidal_av(const void *a, const void *b) {
  const struct catalog_halo *c = a;
  const struct catalog_halo *d = b;
  if ((c->flags & ORPHAN_FLAG) && !(d->flags & ORPHAN_FLAG)) return -1;
  if (!(c->flags & ORPHAN_FLAG) && (d->flags & ORPHAN_FLAG)) return 1;
  if (c->rank2 < d->rank2) return -1;
  if (c->rank2 > d->rank2) return 1;
  return 0;
}


int sort_by_delta_vmax(const void *a, const void *b) {
  const struct catalog_halo *c = a;
  const struct catalog_halo *d = b;
  if (c->rank1 < d->rank1) return -1;
  if (c->rank1 > d->rank1) return 1;
  return 0;
}


void calc_smhm(float z) {
  int64_t i;
  FILE *output = setup_output_file("smhm", z);
  fprintf(output, "#HM SM/HM Obs_SM/HM VMP SM SFR SSFR\n");
  for (i=0; i<nh; i++) {
    if (halos[i].flags & IGNORE_FLAG) continue;
    double ssfr = (halos[i].obs_sm > 0) ? halos[i].obs_sfr / halos[i].obs_sm : -15;
    fprintf(output, "%g %g %g %g %g %g %g\n", halos[i].mp/h0, halos[i].sm/(halos[i].mp/h0), halos[i].obs_sm/(halos[i].mp/h0), halos[i].vmp, halos[i].obs_sm, halos[i].obs_sfr, ssfr);
  }
  fclose(output);

  //Output maps:
  //All
  smhm_dens_map(z, halos, nh, "all");

  //Quenched, SF:
  int64_t nq = nh;
  for (i=0; i<nq; i++) {
    if (halos[i].obs_sfr > 1e-11 * halos[i].obs_sm) {
      struct catalog_halo tmp = halos[i];
      halos[i] = halos[nq-1];
      halos[nq-1] = tmp;
      nq--;
      i--;
    }
  }
  smhm_dens_map(z, halos, nq, "q");
  smhm_dens_map(z, halos+nq, nh-nq, "sf");

  //Sat, Cen:
  int64_t ns = nh;
  for (i=0; i<ns; i++) {
    if (halos[i].upid < 0) {
      struct catalog_halo tmp = halos[i];
      halos[i] = halos[ns-1];
      halos[ns-1] = tmp;
      ns--;
      i--;
    }
  }
  smhm_dens_map(z, halos, ns, "sat");
  smhm_dens_map(z, halos+ns, nh-ns, "cen");

  //Sat,Q, Sat,SF
  nq = ns;
  for (i=0; i<nq; i++) {
    if (halos[i].obs_sfr > 1e-11 * halos[i].obs_sm) {
      struct catalog_halo tmp = halos[i];
      halos[i] = halos[nq-1];
      halos[nq-1] = tmp;
      nq--;
      i--;
    }
  }
  smhm_dens_map(z, halos, nq, "sat_q");
  smhm_dens_map(z, halos+nq, ns-nq, "sat_sf");

  //Cen,Q, Cen,SF
  nq = nh;
  for (i=ns; i<nq; i++) {
    if (halos[i].obs_sfr > 1e-11 * halos[i].obs_sm) {
      struct catalog_halo tmp = halos[i];
      halos[i] = halos[nq-1];
      halos[nq-1] = tmp;
      nq--;
      i--;
    }
  }
  nq -= ns;
  smhm_dens_map(z, halos+ns, nq, "cen_q");
  smhm_dens_map(z, halos+ns+nq, nh-nq-ns, "cen_sf");

  //Tidal Force
  qsort(halos, nh, sizeof(struct catalog_halo), sort_by_tidal_av);
  smhm_dens_map(z, halos, nh*0.25, "low_tidal");
  smhm_dens_map(z, halos+(int64_t)(nh*0.25), nh*0.50, "mid_tidal");
  smhm_dens_map(z, halos+(int64_t)(nh*0.75), nh-(int64_t)(nh*0.75), "high_tidal");

  //Delta_Vmax
  qsort(halos, nh, sizeof(struct catalog_halo), sort_by_delta_vmax);
  smhm_dens_map(z, halos, nh*0.25, "low_dvmax");
  smhm_dens_map(z, halos+(int64_t)(nh*0.25), nh*0.50, "mid_dvmax");
  smhm_dens_map(z, halos+(int64_t)(nh*0.75), nh-(int64_t)(nh*0.75), "high_dvmax");
}

void vmp_nd_sm(float z) {
  int64_t i;
  FILE *output = setup_output_file("vmp_nd_sm", z);
  fprintf(output, "#VMP ND SM Sig.\n");
  double vmp[16*5] = {0};
  double sm[16*5] = {0};
  double sm2[16*5] = {0};
  double counts[16*5] = {0};
  for (i=0; i<nh; i++) {
    int64_t vbin = halos[i].lvmp*16;
    if (halos[i].obs_sm < 1) continue;
    vmp[vbin] += halos[i].lvmp;
    double lsm = log10(halos[i].sm); 
    sm[vbin] += lsm;
    sm2[vbin] += lsm*lsm;
    counts[vbin]++;
  }
    
  for (i=0; i<16*5; i++) {
    if (!counts[i]) continue;
    vmp[i] /= counts[i];
    sm[i] /= counts[i];
    sm2[i] /= counts[i];
    double var = sm2[i] - sm[i]*sm[i];
    double sig = 0;
    if (var > 0) sig = sqrt(var);
    fprintf(output, "%g %g %g %g\n", vmp[i], 16.0*counts[i]/pow(BOX_SIZE/h0, 3.0), sm[i], sig);
  }
  fclose(output);
}

void analyze(float z) {
  calc_smhm(z);
  vmp_nd_sm(z);
}

void analysis_load_boxes(float target) {
  char buffer[1024];
  int64_t i, box_num, min_snap, offset, snap, n;
  float scale;

  for (box_num=0; box_num<NUM_BLOCKS; box_num++) {
    num_scales = 0;
    //First load offsets from file:
    snprintf(buffer, 1024, "%s/offsets.box%"PRId64".txt", INBASE, box_num);
    FILE *in = check_fopen(buffer, "r");
    while (fgets(buffer, 1024, in)) {
      if (buffer[0] == '#') {
	if (!strncmp(buffer, "#Total halos: ", 14)) {
	  offset = atol(buffer+14);
	  check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
	  offsets[num_scales] = offset;
	}
	continue;
      }
      n = sscanf(buffer, "%"SCNd64" %f %"SCNd64, &snap, &scale, &offset);
      if (n!=3) continue;
      check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
      check_realloc_every(scales, sizeof(float), num_scales, 100);
      scales[num_scales] = scale;
      offsets[num_scales] = offset;
      num_scales++;
    }
    fclose(in);
    
    min_snap=0;
    for (i=1; i<num_scales; i++)
      if (fabs(target-scales[min_snap])>fabs(target-scales[i])) min_snap = i;
    SCALE_NOW = scales[min_snap];
    
    int64_t to_read = offsets[min_snap+1]-offsets[min_snap];
    //Then load halos
    if (!CATALOG_MODE) {
      snprintf(buffer, 1024, "%s/sfr_catalog_%f.%"PRId64".bin", OUTBASE, scales[min_snap], box_num);
      in = check_fopen(buffer, "r");
      check_realloc_s(halos, sizeof(struct catalog_halo), nh+to_read);
      check_fread(halos+nh, sizeof(struct catalog_halo), to_read, in);
      fclose(in);
      nh += to_read;
    } else {
      snprintf(buffer, 1024, "%s/sfr_catalog_%f.bin", OUTBASE, scales[min_snap]);
      void *data = check_mmap_file(buffer, 'r', &to_read);
      assert(!(to_read % sizeof(struct catalog_halo)));
      nh = to_read / sizeof(struct catalog_halo);
      check_realloc_s(halos, 1, to_read);
      memcpy(halos, data, to_read);
      munmap(data, to_read);
      return;
    }
  }
}


