#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "../check_syscalls.h"
#include "../universe_time.h"
#include "../all_luminosities.h"
#include "../sampler.h"
#include "../mt_rand.h"

#define NUM_FILTERS 13
#define NUM_SCALES 50
#define NUM_WALKERS 128
#define TOTAL_STEPS 100000

#define UNITS_MAGS 0
#define UNITS_NJY  1

float scales[NUM_SCALES+1] = {0};
float stimes[NUM_SCALES+1] = {0};
float sfh[NUM_SCALES+1] = {0};
float dt[NUM_SCALES+1] = {0};

struct lum_point {
  int32_t filter, units;
  double lum, err_u, err_d;
};

struct lum_point *lps = NULL;
int64_t nlps = 0;
int64_t limit_dust = 0;
int64_t plaw_prior = 0;
int64_t two_c_dust = 0;
int64_t two_c_sfh = 0;
int64_t vary_burst_tscale = 0;

double initial_burst_tscale = 0;

#include "sed_common.c"

double lum_chi2(double *params, float **lls) {
  int64_t i;

  if (!vary_burst_tscale) params[5] = initial_burst_tscale;
  if (!two_c_sfh) params[4] = params[5] = 0;
  if (!two_c_dust) params[3] = params[2];

  double total_sm = params[0];
  double plaw = params[1];
  double dust = params[2];
  double dust2 = params[3];
  double burst_f = params[4];
  double burst_t = params[5];
  double Z_offset = params[6];
  
  float auv = 0;
  if (total_sm < 0) return -1;
  if (dust < 0) return -1;
  if (dust > 10) return -1;
  if (plaw < 0) return -1;
  if (plaw > 10 || (plaw > 4 && plaw_prior)) return -1;
  if (burst_f < 0 || burst_f > 0.3) return -1;
  if (dust2 < 0 || dust2 > 10) return -1;
  if (burst_t < 0 || burst_t > (stimes[NUM_SCALES]-stimes[0])) return -1;
  if (Z_offset < -2 || Z_offset > 2) return -1;
  
    //if (limit_dust && dust > 0.1) return -1;
  if (!two_c_sfh || burst_t == 0)
    gen_sfh(total_sm, plaw);
  else
    gen_sfh_two_c(total_sm, plaw, burst_f, burst_t);

  float lums[NUM_FILTERS] = {0};
  lums_of_sfh(lls, sfh, scales, NUM_SCALES, dust, dust2, burst_t, Z_offset, lums, &auv);
  double chi2=0;
  for (i=0; i<nlps; i++) {
    double model = lums[lps[i].filter];
    if (lps[i].units == UNITS_NJY) {
      model = exp(-0.4*M_LN10*model)*3631.0e9;
    }
    double err = model - lps[i].lum;
    double err_h = lps[i].err_u;
    double err_l = lps[i].err_d;
    if (err > err_h) err/=err_h;
    else if (-err > err_l) err/=err_l;
    else {
      double real_err = err_l +
	(err+err_l)/(err_h+err_l) *
	(err_h - err_l);
      err /= real_err;
    }
    chi2 += err*err;
  }
  if (limit_dust) {
    double exp_auv = 2.00938*(0.5+0.5*erf((lums[0]+20.9282)/-2.98685));
    //double exp_auv = 0.848 + 0.6348 * erf((lums[0]+20.487)/(-1.5381));
    chi2 += pow((exp_auv - auv)/0.25, 2);
  }
  /*double exp_flate = 1.0-pow((stimes[NUM_SCALES]-CONST_TIMESCALE-stimes[0])/(stimes[NUM_SCALES]-stimes[0]), plaw);
  double flate_ratio = log10(f_late/exp_flate);
  chi2 += pow(flate_ratio/0.3, 2);*/
  return chi2;
}

int main(int argc, char **argv) {
  enum lum_types lt[NUM_FILTERS] = {L_m1500, L_f435w, L_f606w, L_f775w, L_f814w, L_f850lp, L_f098m, L_f105w, L_f125w, L_f140w, L_f160w, L_irac1, L_irac2};
  if (argc < 3) {
    fprintf(stderr, "Usage: %s z mag_file.dat <units> <bouwens_dust_prior> <plaw prior> <burst_timescale> <2-component dust> <2-component SFH> <vary_burst_tscale> <vary_metallicity>\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  int64_t i,j;
  double z = atof(argv[1]);
  int32_t units = (argc > 3) ? atol(argv[3]) : UNITS_MAGS;
  limit_dust = (argc > 4) ? atol(argv[4]) : 0;
  plaw_prior = (argc > 5) ? atol(argv[5]) : 1;
  initial_burst_tscale = (argc > 6) ? atof(argv[6]) : 0;
  two_c_dust = (argc > 7) ? atol(argv[7]) : 0;
  two_c_sfh = (argc > 8) ? atol(argv[8]) : 0;
  vary_burst_tscale = (argc > 9) ? atol(argv[9]) : 0;
  int64_t vary_metallicity = (argc > 10) ? atol(argv[10]) : 0;
  
  init_time_table(0.307, 0.6777);
  scales[NUM_SCALES] = 1.0/(1.0+z);
  scales[0] = 0;
  stimes[0] = 0;
  double t0 = scale_to_years(scales[NUM_SCALES])-scale_to_years(0);
  stimes[NUM_SCALES] = t0;
  for (i=1; i<NUM_SCALES; i++) {
    double dtmin = 1e6;
    double dtmax = t0;
    double t = scale_to_years(scales[NUM_SCALES]) - pow(dtmin/dtmax, (double)(i)/(double)(NUM_SCALES-1))*dtmax;
    scales[i] = years_to_scale(t);
    stimes[i] = scale_to_years(scales[i]) - scale_to_years(0);
  }

  for (i=0; i<NUM_SCALES+1; i++) {
    float a1 = (i>0) ? 0.5*(scales[i]+scales[i-1]) : 0;
    float a2 = (i<NUM_SCALES) ? 0.5*(scales[i]+scales[i+1]) : scales[i];
    dt[i] = scale_to_years(a2) - scale_to_years(a1);
  }


  float *lum_tables[NUM_FILTERS] = {0};
  load_luminosities("../fsps");
  for (i=0; i<NUM_FILTERS; i++)
    gen_lum_lookup(&(lum_tables[i]), scales, NUM_SCALES, lt[i], (i==0) ? 0 : z);

  FILE *in = check_fopen(argv[2], "r");
  char buffer[1024];
  struct lum_point lp = {0};
  lp.units = units;
  while (fgets(buffer, 1024, in)) {
    if (buffer[0] == '#') continue;
    if (sscanf(buffer, "%d %lf %lf %lf", &lp.filter, &lp.lum, &lp.err_u, &lp.err_d)!=4) continue;
    check_realloc_every(lps, sizeof(struct lum_point), nlps, 6);
    lps[nlps] = lp;
    nlps++;
  }
  fclose(in);

  double params[7] = {10, 2.0, 0.5, 0.5, 0, 0, 0};
  params[5] = initial_burst_tscale/2e7;
  printf("#Initial Chi2: %e\n", lum_chi2(params, lum_tables));
  printf("#Bouwens dust prior: %"PRId64"\n", limit_dust);
  printf("#Plaw prior: %"PRId64"\n", plaw_prior);
  printf("#Burst timescale: %f\n", initial_burst_tscale);
  printf("#2-component dust: %"PRId64"\n", two_c_dust);
  printf("#2-component SFH: %"PRId64"\n", two_c_sfh);
  printf("#Vary burst timescale: %"PRId64"\n", vary_burst_tscale);
  printf("#Vary metallicity: %"PRId64"\n", vary_metallicity);

  struct EnsembleSampler *e = new_sampler(NUM_WALKERS, 7, 2.0);
  if (!vary_burst_tscale) {
    e->static_dim = 1;
    if (initial_burst_tscale == 0) e->static_dim = 3;
  }
  
  else {
    if (!two_c_dust) e->static_dim++;
    if (!two_c_sfh) e->static_dim++;
    if (e->static_dim==2) e->static_dim = 3;
  }

  if (!vary_metallicity) e->static_dim++;
  
  r250_init(87L);
  for (i=0; i<NUM_WALKERS; i++) {
    double wparams[7];
    for (j=0; j<7; j++)
      wparams[j] = params[j] + dr250() - 0.5;
    //wparams[3] /= 10.0;
    //if (limit_dust) wparams[2] /= 10.0;
    if (!vary_metallicity) wparams[6] = 0;
    if (two_c_sfh || two_c_dust) {
      if (wparams[4] < 0) wparams[4] = -wparams[4];
      while (wparams[4] > 0.3) wparams[4] /= 2.0;
    }
    else wparams[4] = 0;
    if (vary_burst_tscale) {
      wparams[5] *= 2e7;
      if (wparams[5] < 0) wparams[5] = 0;
      if (wparams[5] > (stimes[NUM_SCALES]/2.0)) wparams[5] = stimes[NUM_SCALES]/2.0;
    } else {
      wparams[5] = 0;
    }
    init_walker_position(e->w+i, wparams);
  }

  printf("#Total_SM Plaw Dust Dust2 Burst_F Burst_T Z_Offset Chi2\n");
  for (i=0; i<TOTAL_STEPS; i++) {
    struct Walker *w = get_next_walker(e);
    update_walker(e, w, lum_chi2(w->params, lum_tables));
    if (ensemble_ready(e)) {
      update_ensemble(e);
      if (i>50000)
	write_accepted_steps_to_file(e, stdout, 0);
    }
  }
  
  return 0;
}
