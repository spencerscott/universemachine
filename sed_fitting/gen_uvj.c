#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "../check_syscalls.h"
#include "../universe_time.h"
#include "../all_luminosities.h"
#include "../sampler.h"
#include "../mt_rand.h"

#define NUM_FILTERS 4
int64_t NUM_SCALES = 0;

int main(int argc, char **argv) {
  enum lum_types lt[NUM_FILTERS] = {L_m1500, L_Jn_U, L_Jn_V, L_2mass_j};
  if (argc < 2) {
    fprintf(stderr, "Usage: %s Z <snaps.txt>\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  int64_t i;
  double z = 0;
  double max_age = 2e10;
  double Z = atof(argv[1]);
  float *scales = NULL, *stimes = NULL;

  init_time_table(0.307, 0.6777);

  if (argc > 2) {
    FILE *in = check_fopen(argv[2], "r");
    char buffer[1024];
    while (fgets(buffer, 1024, in)) {
      int n;
      float scale;
      if (sscanf(buffer, "%d %f", &n, &scale)!=2) continue;
      check_realloc_every(scales, sizeof(float), NUM_SCALES, 100);
      scales[NUM_SCALES] = scale;
      NUM_SCALES++;
    }
    NUM_SCALES--;
  } else {
    NUM_SCALES = 200;
    check_realloc_s(scales, sizeof(float), NUM_SCALES);
    check_realloc_s(stimes, sizeof(float), NUM_SCALES);
  
    scales[NUM_SCALES] = 1.0/(1.0+z);
    double t0 = scale_to_years(scales[NUM_SCALES])-scale_to_years(0);
    stimes[NUM_SCALES] = t0;

    if (max_age >= t0) scales[0] = 0;
    else scales[0] = years_to_scale(scale_to_years(scales[NUM_SCALES]) - max_age);
    stimes[0] = scale_to_years(scales[0]) - scale_to_years(0);
    stimes[NUM_SCALES] = t0;
    for (i=1; i<NUM_SCALES; i++) {
      double dtmin = 1e6;
      double dtmax = t0 - stimes[0];
      double t = scale_to_years(scales[NUM_SCALES]) - pow(dtmin/dtmax, (double)(i)/(double)(NUM_SCALES-1))*dtmax;
      scales[i] = years_to_scale(t);
      stimes[i] = scale_to_years(scales[i]) - scale_to_years(0);
    }
  }

  float *lum_tables[NUM_FILTERS] = {0};
  load_luminosities("../fsps");
  for (i=0; i<NUM_FILTERS; i++)
    gen_lum_lookup(&(lum_tables[i]), scales, NUM_SCALES, lt[i], (i==0) ? 0 : z);

  for (i=0; i<NUM_SCALES; i++) {
    printf("%f", log10f(1.0+scale_to_years(scales[NUM_SCALES])-scale_to_years(scales[i])));
    for (int64_t j=0; j<NUM_FILTERS; j++) {
      printf(" %f", -2.5*log10f(lookup_lum(lum_tables[j], Z, NUM_SCALES, i, 0)));
    }
    printf("\n");
  }
  return 0;
}
