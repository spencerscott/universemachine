#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <assert.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"

int main(int argc, char **argv) {
  if (argc<2) {
    printf("Usage: %s sfr_list.txt\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  struct catalog_halo tp = {0};
  char buffer[1024];
  FILE *in = check_fopen(argv[1], "r");
  while (fgets(buffer, 1024, in)) {
    if (buffer[0]=='#') continue;
    sscanf(buffer, "%"PRId64" %"PRId64" %"PRId64" %"PRId32" %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
	   &tp.id, &tp.descid, &tp.upid, &tp.flags, &tp.uparent_dist, &tp.pos[0], &tp.pos[1], &tp.pos[2], &tp.pos[3], &tp.pos[4], &tp.pos[5], &tp.m, &tp.v, &tp.mp, &tp.vmp, &tp.r, &tp.rank1, &tp.rank2, &tp.ra, &tp.rarank, &tp.sm, &tp.icl, &tp.sfr, &tp.obs_sm, &tp.obs_sfr);
    tp.m *= 0.68;
    tp.mp *= 0.68;
    fwrite(&tp, sizeof(struct catalog_halo), 1, stdout);
  }
  fclose(in);
  return 0;
}
