#!/usr/bin/perl -w
package Universe::Growth;

our $da = 0.001;
our @delta;
our ($om, $ol) = (0.27, 0.73);

sub init_delta {
    my $steps = 1/$da;
    my $int = 0;
    $delta[0] = 0;
    for (1..$steps) {
	my $a = ($_-0.5)*$da;
	my $anow = $_*$da;
	$int += dadtau2($om, $ol, $a)**(-3/2);
	push @delta, 5*$om/(2*$anow)*sqrt(dadtau2($om, $ol, $anow))
	    *$int*$da;
    }
}

sub set_cosmo($$) {
    ($om, $ol) = @_;
    @delta = ();
}

sub delta {
    init_delta() unless(@delta);
    my ($a) = shift;
    my $bin = int($a/$da);
    if ($a < 0) { return 0; }
    if ($bin > $#delta-1) { $bin = $#delta-1; }
    return ($delta[$bin] + ($a/$da-$bin)*($delta[$bin+1]-$delta[$bin]));
}

sub dadtau2 {
    my ($om, $ol, $a) = @_;
    my $z = 1/$a - 1;
    return (1+($om*$z)+($ol*($a*$a-1)));
}

sub test {
    print $_/10, " ", delta($_/10), "\n" for (0..11);
}

