#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <sys/stat.h>
#include "io_helpers.h"
#include "check_syscalls.h"

int64_t read_params(char *buffer, double *params, int max_n) {
  int num_entries = 0;
  char *cur_pos = buffer, *end_pos;
  double val = strtod(cur_pos, &end_pos);
  while (cur_pos != end_pos && num_entries < max_n) {
    params[num_entries] = val;
    num_entries++;
    cur_pos=end_pos;
    while (*cur_pos==' ' || *cur_pos=='\t' || *cur_pos=='\n') cur_pos++;
    val = strtod(cur_pos, &end_pos);
  }
  return num_entries;
}

int64_t read_params_from_file(char *fn, double *params, int max_n) {
  FILE *input = check_fopen(fn, "r");
  char buffer[2048];
  check_fgets(buffer, 2048, input);
  fclose(input);
  return read_params(buffer, params, max_n);
} 

