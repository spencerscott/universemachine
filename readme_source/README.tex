\documentclass[12pt]{article}
\usepackage{fouriernc}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage[bookmarks=true,colorlinks=true,linkcolor=blue]{hyperref}

\begin{document}
\vspace{-1ex}
\noindent{}The UniverseMachine\\
\noindent{}Most code: Copyright \textcopyright{}2011-2019 Peter Behroozi\\
\noindent{}License: GNU GPLv3\\
\noindent{}Science/Documentation Paper: \url{https://arxiv.org/abs/1806.07893}\\

\tableofcontents
\newcommand{\ttt}[1]{\texttt{#1}}

\section{Overview}

The UniverseMachine applies simple empirical models of galaxy formation to dark matter halo merger trees.  For each model, it generates an entire mock universe, which it then observes in the same way as the real Universe to calculate a likelihood function.  It includes an advanced MCMC algorithm to explore the allowed parameter space of empirical models that are consistent with observations.

\section{Precalculated Data Products}

The latest data release is available at \url{http://www.peterbehroozi.com/data.html}.
The UniverseMachine can generate many data products (e.g., stellar mass functions, stellar mass---halo mass relations, star formation histories, etc.) and the list is growing continuously.  This section describes some of the available data products, which are all in the \texttt{data} subdirectory of the data release tarball.  Except where otherwise specified, errors represent the uncertainties in the model posterior distribution.

\subsection{Observed versus True Stellar Masses and Star Formation Rates}

The UniverseMachine keeps track of two stellar masses -- the ``true'' stellar mass, and the ``observed'' stellar mass.  The true stellar mass is the physically self-consistent stellar mass given by the integral of past star formation minus stellar mass loss.  The observed stellar mass includes systematic offsets as well as scatter that both change as a function of redshift.  Which one is more useful depends on the purpose---those wanting physically self-consistent star formation histories should use the true stellar mass; those wanting to compare with other observations should use the observed stellar mass.  A similar distinction applies to star formation rates, where the observed rates include additional scatter and systematic offsets, but are the closest match to other observations; the true SFRs are by contrast guaranteed to be physically self-consistent with the true stellar masses.

\subsection{Meaning of Values and Uncertainties}

Almost all data products include the bestfit value as well as the 68\% confidence interval from the model posterior space.  The column \texttt{Err+} gives the difference between the 84$^\mathrm{th}$-percentile model and the bestfit model; the column \texttt{Err-} gives the difference between the bestfit model and the 16$^\mathrm{th}$-percentile model.  Hence, if the best-fit value, \texttt{Err+}, and \texttt{Err-} columns are \textit{A}, \textit{B}, and \textit{C}, respectively, the 68\% confidence interval ranges from \textit{A-C} to \textit{A+B}.  Exceptions to this rule are always noted in the data files.

As a general rule, \textit{Bolshoi-Planck} becomes increasingly incomplete for satellite galaxies with peak halo masses $M_\mathrm{peak} < 10^{10.5}$M$_\odot$ and for central galaxies with peak halo masses $M_\mathrm{peak} < 10^{10}$M$_\odot$.  The incompleteness for massive halos varies with redshift; most data files include galaxy/halo counts so that it is clear where the statistics become unreliable.

\subsection{Correlation Functions}

Found in \texttt{data/corrs}.  Correlation functions are in \texttt{corr\_sm*}; the filename gives the observed stellar mass range and the scale factor at which the correlation function was calculated.  Correlation functions for all, star-forming, and quenched galaxies are included, as is the star-forming x quenched cross-correlation function.  Values for, e.g., $\pi_\mathrm{max}$, redshift errors, etc., depend on the parameter file used, but are documented in each file.  Ratios of correlation functions are in \texttt{corr\_ratios\_sm*}.  The meaning of the filename is the same (stellar mass range and the scale factor at which the correlation function was calculated).  Ratios of quenched to star-forming, star-forming to all, quenched to all, and the cross-correlation to all galaxies are included.

\subsection{Cosmic Star Formation Rates}

Found in \texttt{data/csfrs}.  This includes the total observed CSFR, the observed CSFR for galaxies with $M_{1500}<-17$ (AB), and the true CSFR.  For \textit{Bolshoi-Planck}, the  $M_{1500}<-17$ CSFR  is almost identical to the total CSFR, as the simulation becomes increasingly incomplete for  $M_{1500}>-19$.  This also suggests that the ``total'' CSFRs at $z> 8$ are underestimates of the true total CSFRs.

\subsection{Ex-Situ Fractions}

Found in \texttt{data/ex\_situ}.  Ex-situ fractions (i.e., fractions of mass accreted in mergers) as a function of observed stellar mass are in \texttt{ex\_situ\_a*}, where the filename includes the scale factor.  Ex-situ fractions as a function of $M_\mathrm{peak}$ are in \texttt{ex\_situ\_hm\_a*}.

\subsection{Halo Mass Functions}

Found in \texttt{data/hmfs}.  Both the total mass function (including satellites) as well as the satellite fractions are in \texttt{hmf\_a*}, where the filename includes the scale factor.  While central halos are invariant for a given simulation, the satellite fraction can vary as a result of the orphan threshold (see the UniverseMachine paper).

\subsection{Infall and Quenching Distribution Statistics for Satellites}

Found in \texttt{data/infall\_stats}.  The time distributions since satellite first infall (i.e., the 50$^\mathrm{th}$, 84$^\mathrm{th}$, and 16$^\mathrm{th}$ percentiles) as a function of observed satellite stellar mass are in \texttt{infall\_delay\_times\_a*}, where the filename includes the scale factor.  These percentiles refer to the distribution for individual galaxies; each percentile is accompanied by uncertainties across the model posterior distribution.  I.e., the median time since infall is reported for the best-fit model, followed by the 68\% confidence interval on the median across model posterior space, followed by the 84$^\mathrm{th}$ percentile time since infall, followed by the 68\% confidence interval on the 84$^\mathrm{th}$ percentile across model posterior space, and so on.  The time distributions are recorded for satellites of Milky Way--mass hosts, group-mass hosts, and cluster-mass hosts; the definitions for each host mass are given in the file.

Quenching delay times (i.e., the time delay between satellite infall and satellite quenching for quenched satellites) as a function of observed satellite stellar mass are in \texttt{quenching\_delay\_times\_a*}.  As with infall delay times, the 50$^\mathrm{th}$, 84$^\mathrm{th}$, and 16$^\mathrm{th}$ percentiles of the distribution for individual satellites are given, for satellites of Milky Way--mass hosts, group-mass hosts, and cluster-mass hosts.  Similarly, infall SSFR distributions as a function of observed satellite stellar mass are given in \texttt{infall\_ssfrs\_a*}.  Only a restricted set of scale factors are available for the latter to reduce the disk space necessary for postprocessing.

\subsection{Observations and Best-fit Model}

\label{s:obs}

Found in \texttt{data/obs.txt}.  The top line contains the best-fit model, which may be used to generate new catalogs with the \texttt{make\_sf\_catalog} command (\S \ref{s:new_catalogs}).  The remaining lines contain one data point per line, including both the observed value and the modeled value.  The data point type can be one of the following:
\begin{itemize}
\item \texttt{smf}: observed stellar mass function (i.e., galaxy number density); units of comoving Mpc$^{-3}$ dex$^{-1}$.
\item \texttt{uvlf}: M$_{1500,UV}$ luminosity function; units of comoving Mpc$^{-3}$ mag$^{-1}$.
\item \texttt{qf}: quenched fraction as a function of observed stellar mass, using the Moustakas/PRIMUS quenching threshold.
\item \texttt{qf11}: quenched fraction as a function of observed stellar mass, using a threshold of SSFR < $10^{-11}$ yr$^{-1}$.
\item \texttt{qf\_uvj}: UVJ quenched fraction as a function of observed stellar mass.
\item \texttt{ssfr}: average observed specific star formation rate (for all galaxies) as a function of observed stellar mass; units of yr$^{-1}$.
\item \texttt{csfr}: total observed cosmic star formation rate; units of M$_\odot$ yr$^{-1}$ comoving Mpc$^{-3}$.
\item \texttt{csfr\_(uv)}: total observed cosmic star formation rate with $M_{1500}<-17$ (AB); same units as \texttt{csfr}.
\item \texttt{correlation}: projected autocorrelation functions; units of comoving Mpc.
\item \texttt{cross correlation}: projected cross-correlation functions; units of comoving Mpc.
\item \texttt{conformity}: central galactic conformity, currently unused.
\item \texttt{lensing}: weak lensing, currently unused.
\item \texttt{cdens\_fsf}: fraction of star-forming central galaxies, as a function of environment density.
\item \texttt{cdens\_ssfr\_sf}: observed specific star formation rates for star-forming central galaxies, as a function of environmental density; currently unused.
\item \texttt{uvsm}: median stellar mass as a function of $M_{1500}$; units of M$_\odot$.
\item \texttt{irx}: average infrared excess as a function of $M_{1500}$; log$_{10}$ units.
\end{itemize}

The data point subtype is typically ``\texttt{a}'' for ``all galaxies,'' but for correlation functions, it can also be ``\texttt{s}'' for star-forming galaxies and ``\texttt{q}'' for quenched galaxies.  The columns \texttt{Z1} and \texttt{Z2} contain the redshift range of the observation; \texttt{step1} and \texttt{step2} are the corresponding range of simulation snapshot numbers used.  \texttt{SM1} and \texttt{SM2} are the stellar mass bin, except for \texttt{csfr} (where the stellar mass bin is meaningless), \texttt{uvlf} (where the UV magnitude bin is given instead), and \texttt{cdens\_fsf} (where the bin for the number of neighbors is given instead).  \texttt{smb1} and \texttt{smb2} give the internal stellar mass/UV/environment bin indices used by the UniverseMachine.  \texttt{R1} and \texttt{R2} are the range of radii used, which is only relevant for \texttt{correlation} functions.

The observed data point is given by the \texttt{Val} column, with $+/-$ uncertainties given in the \texttt{Err\_h} and \texttt{Err\_l} columns, respectively.  All values and uncertainties are given in log$_{10}$ units, except for quenched and star-forming fractions (\texttt{qf}, \texttt{qf\_uvj}, \texttt{cdens\_fsf}), which are in linear units.  The best-fit model result is given in the \texttt{Model\_Val} column, with the $+/-$ 68\% confidence interval of the model posteriors in the \texttt{MV+} and \texttt{MV-} columns.  The best-fit $\chi^2$ and 68\% range are given in the next three columns.  Here, $\chi^2$ values may be zero if the model result is within the calculation error tolerance of the observed value (to prevent over-fitting).  Correlation functions and other observations that use covariance matrices may not have a direct relation between $\chi^2$ values and model -- observed differences for individual data points.

\subsection{Stellar Mass--Halo Mass Relations}

\subsubsection{Median Measurements from the Simulation}

Direct measurements of the median relations binned on halo peak mass are found in \texttt{data/smhm/median\_raw}.  The median SMHM ratios are in \texttt{smhm\_a*}, and are available for both observed and true stellar masses, as well as subsamples (e.g., centrals, satellites, quenched, star-forming, etc.); the filename includes the scale factor.  The halo mass column gives $\log_{10}(M_\mathrm{peak}/M_\odot)$, the SMHM ratio columns give the median $\log_{10}(M_\ast / M_\mathrm{peak})$, and the error columns give the $+/-$ uncertainties in dex.  Errors on the observed stellar mass ratios should be interpreted as \textit{statistical} errors; errors on the true stellar mass ratios should be interpreted as \textit{statistical+systematic} errors.  Ratios of subsamples (e.g., central galaxies vs. all galaxies) are in \texttt{ratios\_a*}.  As the model currently applies the same offset between observed and true stellar masses to all galaxies, the stellar mass ratios are the same for observed and true stellar masses; hence, no distinction is made in the file.  Finally, measurements of the scatter in $\log_{10}(M_\ast / M_\mathrm{peak})$ (in dex) are in \texttt{smhm\_scatter\_a*}.

\subsubsection{Median Fits}

Fits to the measurements above are found in  \texttt{data/smhm/median\_fits} in pretabulated form for both \texttt{smhm\_a*} and  \texttt{ratios\_a*}; the filename includes the scale factor.  Residuals with the direct measurements are found in \texttt{smhm\_residuals*}; these should be examined if using the fits for a rare population (e.g., high-redshift quiescent galaxies).

The fit parameters listed in the paper are found in \texttt{data/smhm/params}, along with a Python script to generate SMHM ratios at arbitrary redshifts.

\subsubsection{Average Measurements from the Simulation}

Averages of $\log_{10}(M_\ast / M_\mathrm{peak})$ (both for observed and true stellar mass) as a function of peak halo mass are found in \texttt{data/smhm/averages/sm\_averages\_a*}; the filename includes the scale factor.  Average halo masses as a function of observed stellar mass are found in \texttt{data/smhm/averages/hm\_averages\_a*}.  Here, several different averages are available, including the linear average peak halo mass ($\langle M_\mathrm{peak}\rangle$), the log average peak halo mass  ($\langle \log_{10}(M_\mathrm{peak})\rangle$), and the weak lensing-averaged halo mass ($\langle M_\mathrm{peak}^{2/3}\rangle^{3/2}$).  


\subsection{Quenched Fractions}

Found in \texttt{data/qfs}.  Basic quenched fractions according to three different quenching definitions (Moustakas/PRIMUS, SSFR $< 10^{-11}$ yr$^{-1}$, and UVJ) are found as a function of observed stellar mass in \texttt{qf\_a*} and as a function of peak halo mass in \texttt{qf\_hm\_a*}; the filename includes the scale factor.  Statistics for the quenched fraction of all centrals and all satellites, as well as satellites of Milky Way-mass hosts, group-mass hosts, and cluster-mass hosts are found as a function of observed stellar mass in \texttt{qf\_groupstats\_a*} and of peak (satellite) halo mass in \texttt{qf\_hm\_groupstats\_a*}.  The fractions of quenched satellites that were quenched after infall for Milky Way-mass, group-mass, and cluster-mass hosts are found as a function of observed stellar mass in \texttt{qf\_groupstats\_infall\_a*} and of peak (satellite) halo mass in \texttt{qf\_hm\_groupstats\_infall\_a*}.  The fractions of quenched satellites that were quenched \textit{due to} infall (calculated as $f_{q,sat} - f_{q,cen}$) for all satellites as well as satellites of Milky Way-mass hosts, group-mass hosts, and cluster-mass hosts are found as a function of observed stellar mass in \texttt{qf\_quenched\_infall\_a*} and of peak (satellite) halo mass in \texttt{qf\_hm\_quenched\_infall\_a*}. The fractions of galaxies' most-massive progenitors that were quenched as a function of cosmic time for both currently star-forming and currently quenched galaxies are found for bins of observed stellar mass in \texttt{qf\_sm\_histories\_sm*} and for bins of peak halo mass in \texttt{qf\_hm\_histories\_hm*}.  The exact range of stellar masses or halo masses in each bin is detailed in the file header.

\subsection{Rejuvenation Statistics}

Found in \texttt{data/rejuvenation}.  The fractions of galaxies that rejuvenated (i.e., were quenched for at least 300 Myr and then were star-forming for at least 300 Myr thereafter) are found as a function of observed stellar mass in \texttt{rejuv\_a*} and as a function of peak halo mass in \texttt{rejuv\_hm\_a*}; the filename includes the scale factor.  

\subsection{Average Star Formation Histories}

Found in \texttt{data/sfhs}.  Average star formation histories for all galaxies, centrals, satellites, star-forming, and quenched galaxies are found in bins of observed stellar mass in \texttt{sfh\_sm*} and in bins of peak halo mass in \texttt{sfh\_hm*}; the filename includes the mass bin and the scale factor.

\subsection{Stellar Mass Functions and Satellite Fractions}

Found in \texttt{data/smfs}.  The stellar mass function (i.e., galaxy number density) as well as the satellite fraction as a function of observed stellar mass are found in \texttt{smf\_a*}, where the filename includes the scale factor.  The \textit{Bolshoi-Planck} simulation is incomplete for low-mass galaxies and halos.  This incompleteness is significant below $10^{7}$M$_\odot$ at $z=0$ and $10^{8.5}$M$_\odot$ at $z=8$.

\subsection{Average Specific Star Formation Rates}

Found in \texttt{data/ssfrs}.  The average linear ratio of observed SFR to observed stellar mass as a function of observed stellar mass is found in \texttt{ssfr\_a*}, where the filename includes the scale factor.

\subsection{UV Luminosity Functions}

Found for $z\ge4$ in \texttt{data/uvlfs} and $z<4$ in \texttt{data/uvlfs\_uncalibrated}; the directory naming reflects that $z<4$ UV luminosities do not have proper dust calibration in the model and are likely incorrect.  Galaxy number densities as a function of UV magnitude ($M_{1500,AB}$) are found in \texttt{uvlf\_a*}; the filename includes the scale factor.  The \texttt{Bolshoi-Planck} simulation becomes increasingly incomplete for $M_{1500}>-19$.  

\subsection{UV--Stellar Mass Relations}

Found for $z\ge4$ in \texttt{data/uvsm} and $z<4$ in \texttt{data/uvsm\_uncalibrated}; the directory naming reflects that $z<4$ UV luminosities do not have proper dust calibration in the model and are likely incorrect.  Median observed stellar masses as a function of UV magnitude ($M_{1500,AB}$) are found in \texttt{uvsm\_z*}; the filename includes the redshift range.  The \texttt{Bolshoi-Planck} simulation becomes increasingly incomplete for $M_{1500}>-19$.  

\subsection{Weak Lensing}

Found in \texttt{data/weak\_lensing}.  Galaxy-galaxy weak lensing shear predictions are found for all galaxies, star-forming galaxies, and quenched galaxies in \texttt{wl\_sm*}.  The filename includes the scale factor and the lower observed stellar mass threshold; i.e., only galaxies with masses above the threshold in the filename are included.  Ratios of weak lensing predictions for quenched to star-forming, star-forming to all galaxies, and quenched to all galaxies are found in \texttt{wl\_ratios\_sm*}.

\section{Mock Catalogs and Lightcones}

Mock catalogs and lightcones are available for the best-fit model (see \url{http://www.peterbehroozi.com/data.html}), as described below.

\subsection{Halo and Galaxy Properties}

\label{s:hg_properties}

Halo and galaxy properties at every simulation snapshot are available at \texttt{SFR/sfr\_catalog\_*} and \texttt{SFR\_ASCII/sfr\_catalog\_*}; the filename includes the scale factor.  Halo properties include the halo ID (for cross-matching to \textit{Bolshoi-Planck} halo catalogs), descendant ID, parent ID (for satellites), position, velocity, current mass and $v_\mathrm{max}$, peak mass, and $v_\mathrm{max}$ at peak mass.  Galaxy properties include the true stellar mass in the galaxy, true stellar mass in the intrahalo light, observed stellar mass, observed SFR, observed SSFR, true stellar mass / halo mass ratio, and observed UV luminosity (valid for $z>4$).  Both binary (\texttt{SFR}) and text (\texttt{SFR\_ASCII}) versions are available.  The binary version is consecutive \texttt{catalog\_halo} structures; Python and C loaders are provided in the same directory.  See \texttt{halo.h} for the structure definition and \texttt{print\_sm\_catalog.c} in the UniverseMachine source code for a more advanced example of how to read the binary version.  You can also use \href{http://halotools.readthedocs.io/en/latest/}{HaloTools} to load the binary catalogs.

\subsection{Star Formation Histories}

Catalogs with star formation histories are available at specific redshifts (e.g., $z=0, 1, 2$) in \texttt{SFH/sfh\_catalog\_*}; the filename includes the scale factor.  Besides the halo and galaxy properties in \S \ref{s:hg_properties}, these files contain star formation histories for the present-day stellar population in the galaxy (i.e., including all merged progenitors), star formation histories for the present-day population in the intrahalo light, the main progenitor galaxy's stellar mass history, the main progenitor's intrahalo light history, the main progenitor's halo mass history, the main progenitor's SFR history (i.e., excluding any mergers), the main progenitor's $v_\mathrm{Mpeak}$ (i.e., $v_\mathrm{max}$ at peak mass), and the main progenitor's $\Delta$$v_\mathrm{max}$ rank (expressed in units of standard deviations).  These files are split into many pieces (144 for \textit{Bolshoi-Planck}) to make them easier to analyze in parallel.

\subsection{CANDELS Lightcones}

Lightcones for the CANDELS fields (EGS, COSMOS, UDS, GOODS-N/S) are available in \texttt{CANDELS\_Lightcones/survey\_*}.  The filenames include the field, the redshift range, the width of the lightcone in arcminutes (``\texttt{x}''), the height of the lightcone in arcminutes (``\texttt{y}''), and the lightcone index.  Eight lightcones are available for each field---these are separate realizations of each field to aid in estimating sample variance.  These lightcones contain the galaxy sky position (RA, Dec, $z$), the halo ID, lightcone 3D position, velocity, halo mass and $v_\mathrm{max}$, galaxy true/observed stellar mass, intrahalo light, true/observed SFR, observed SSFR, true stellar mass to halo mass ratio, observed UV luminosity (only valid at $z>4$), and UV attenuation (only valid at $z>4$).

\section{Running Basic Analyses}

\subsection{Compiling}

   If you use the GNU C compiler version 4.0 or above on a 64-bit machine,
   compiling should be as simple as typing ``\texttt{make}'' at the command prompt.
   If you use the Intel C compiler, uncomment the lines \texttt{CC=icc} and \texttt{OPT\_FLAGS=-fast}
   in the Makefile before running ``\texttt{make}''.

   The UniverseMachine does not support compiling on 32-bit machines and has not been
   tested with other compilers.  Additionally, it does not support
   non-Unix environments.  (Mac OS X is fine; Windows is not).  If you use the code to convert
   new merger trees to UniverseMachine format, you will need the GNU Scientific Library (GSL) installed;
   to compile this code, you should run ``\texttt{make treereg}''.
   

\subsection{Making New Lightcones}

Lightcones for arbitrary fields can be generated with the \texttt{lightcone} command after compiling.  You will need the binary SFR catalogs (\S \ref{s:hg_properties}), the config file, and the list of snapshots (\texttt{snaps.txt}).  After downloading, you'll have to edit the config file so that \texttt{INBASE} is the directory path where you've downloaded \texttt{snaps.txt} and \texttt{OUTBASE} is the directory where the binary SFR catalogs are located.  Running the \texttt{lightcone} command gives a brief usage statment.  \texttt{z\_low} and \texttt{z\_high} give the redshift range to generate the lightcone, \texttt{x\_arcmin} gives the width of the lightcone in arcminutes, \texttt{y\_arcmin} gives the height of the lightcone in arcminutes, \texttt{samples} gives the number of lightcone realizations to generate, and \texttt{id\_tag} gives optional text to add to the output filename.   \texttt{do\_collision\_test}, if specified as 1, will ensure that the lightcone doesn't overlap with itself.  This is inadvisable except with very small lightcones; e.g., most volumes of interest will be of comparable size to \textit{Bolshoi-Planck}.  \texttt{ra} and \texttt{dec} give the center of the lightcone on the sky; \texttt{theta} gives the additional rotation (in degrees) of the lightcone around this central axis.  Finally, \texttt{rseed} allows specifying the random seed for generating lightcone positions and orientations within the simulation.  This is helpful if you want to generate a lightcone using the same halos for a different UniverseMachine model.  It is also helpful for generating large lightcones in parallel: if several lightcone programs are started with the same random seed and different redshift ranges, they will generate different parts of the same lightcone (provided that \texttt{do\_collision\_test} is 0).

\subsection{Making New Catalogs}

\label{s:new_catalogs}

The script \texttt{scripts/make\_sf\_catalog.pl} will generate new catalogs (as in \S \ref{s:hg_properties}) as well as, optionally, new star formation histories for a specified model.  Run \texttt{perl scripts/make\_sf\_catalog.pl} for usage information.

You will need the base simulation data (\texttt{base/*.*}), the config file, as well as the model parameters (e.g., those in the data release, in \texttt{data/obs.txt}).  You will need to edit the config file so that \texttt{INBASE} is the directory where the base simulation data is located and \texttt{OUTBASE} is the directory to which the binary SFR catalogs (as in \S \ref{s:hg_properties}) and text SFHs (if specified) should be written.  The script will use as many threads as you specify to generate the final catalog; the memory used is about $3$GB per thread for \texttt{Bolshoi-Planck}.  The final SFH catalogs will be in text format, but the final halo/galaxy property catalogs will be in binary format.  To convert the latter to text format, you can use the \texttt{print\_sm\_catalog} command.  For testing purposes, you can also use the \texttt{make\_sf\_catalog} command directly; this will generate a single piece of the catalog at a time.  Run \texttt{make\_sf\_catalog} without options to see usage information.

\section{Running on Different Simulations}

\label{s:new_sims}

Currently, preprocessed simulation files are available for \textit{Bolshoi-Planck}, \textit{VSMDPL}, \textit{SMDPL}, and \textit{MDPL2} (see \url{http://www.cosmosim.org}).  To run on another simulation, you will need to start with trees and catalogs generated by a recent version of \href{https://bitbucket.org/pbehroozi/consistent-trees}{\textsc{Consistent Trees}} (i.e., the catalogs should have a \texttt{DeltaLogVmax} column).  You will then need to do the following steps:
\begin{itemize}
\item In a terminal, print out the first line of one of the \texttt{hlist} catalogs for your simulation (\texttt{head -n1 hlist\_XYZ.list}). 
\item Open \texttt{sims/rhs\_bolshoi.h} in a text editor.  Follow the instructions in the file for how to modify it to match the first (header) line of your hlist.
\item Run ``\texttt{make treereg}'' in the \textsc{UniverseMachine} source directory to generate the tree-processing executables.  This requires \href{https://www.gnu.org/software/gsl/}{GSL} to be installed.
\item Run ``\texttt{perl /path/to/universemachine/scripts/parallel\_fit\_splines.pl <threads>}'' in the directory containing all your hlist files, where \texttt{<threads>} is the number of hlist files that can fit into memory simultaneously.  This will create a directory called \texttt{splines}, with information about the rank ordering of all halos' \texttt{DeltaLogVmax} parameters.
\item Run ``\texttt{/path/to/universemachine/split\_halo\_trees\_phase1 <hlist\_a1.list> <box\_size> <nx> <ny> 1 /path/to/forests.list > boxlist.txt}''  Here, \texttt{<hlist\_a1.list>} is the full name of the $a=1$ halo catalog, \texttt{<box\_size>} is the box length in Mpc$/h$, \texttt{<nx>} and \texttt{<ny>} are the number of pieces in which the simulation volume will be chopped in the x and y directions, and \texttt{/path/to/forests.list} is the path to the \texttt{forests.list} file generated by \textsc{Consistent Trees} (often, \texttt{../trees/forests.list}).
\item Run ``\texttt{perl /path/to/universemachine/scripts/parallel\_split\_halo\_trees.pl <box\_size> <Om> <h> /path/to/scales.txt /path/to/splines  /path/to/boxlist <threads> /path/to/trees/tree\_*.dat}.''  Here, \texttt{<box\_size>} is the box length in Mpc$/h$, \texttt{<Om>} is the value of $\Omega_M$, \texttt{<h>} is the value of $h$, \texttt{/path/to/scales.txt} is the path to the \texttt{scales.txt} file used by \textsc{Consistent Trees} (\texttt{SCALEFILE}), \texttt{/path/to/splines} is the path to the \texttt{splines} directory generated by the \texttt{parallel\_fit\_splines.pl} script, \texttt{/path/to/boxlist} is the path to the \texttt{boxlist.txt} file generated in the previous step, \texttt{<threads>} is the number of trees to read in parallel (each uses about 20 GB of memory), and \texttt{/path/to/trees/tree\_*.dat} is the path to all the tree files generated by \textsc{Consistent Trees} (often, \texttt{../trees/tree\_*.dat}).  This step reads in all the tree files, gravitationally simulates the paths of orphan subhalos (i.e., subhalos that have been potentially destroyed too early in the simulation), reorders the halos into depth-first order, and prints out binary files in the format that \textsc{UniverseMachine} expects.  It will generate a directory called \texttt{catalog\_bin} that contains the binary catalogs, a list of snapshots, and lists of halo counts at each redshift for each binary catalog.
\end{itemize}
The \texttt{catalog\_bin} directory generated above can then be used as the \texttt{INBASE} directory in your config file.  The number of subboxes generated will be the product of \texttt{<nx>} and \texttt{<ny>}; this should be set as \texttt{NUM\_BLOCKS} in your config file.  You will also have to modify your config file to have the correct \texttt{BOX\_SIZE} and cosmology (\texttt{Om}, \texttt{Ol}=$\Omega_\Lambda$, and \texttt{h}).


\section{Parameter Space Exploration}

\subsection{Observational Constraints}

Observational constraints are automatically read from the \texttt{obs/} subdirectory for the UniverseMachine.  You can add new constraints by placing new files in this directory.  All files provided should ideally have the same assumptions employed: specifically, a Chabrier IMF, a Calzetti dust law, and the BC03 SPS model.  If these are not consistent, the output constraints will not necessarily be physically meaningful.

Each file must be in ASCII format, with at least one header line that specifies the file type (i.e., ``\texttt{\#type: XYZ}'').  As of this writing, the allowed file types are: \texttt{smf}, \texttt{uvlf}, \texttt{qf}, \texttt{qf11}, \texttt{qf\_uvj}, \texttt{ssfr}, \texttt{cosmic sfr}, \texttt{cosmic sfr (uv)}, \texttt{correlation}, \texttt{cross correlation}, \texttt{conformity}, \texttt{lensing}, \texttt{cdens fsf}, \texttt{cdens ssfr\_sf}, \texttt{uvsm}, and \texttt{irx}, which indicate the observation type as in Section \ref{s:obs}.  Note that some type names above include spaces, whereas the observation types in Section \ref{s:obs} do not; the type names listed here are the correct ones to use.  The units are the same as listed in Section \ref{s:obs}.  For types other than cosmic SFRs, the file should also include two lines that specify the redshift range (\texttt{\#zlow: z1} and \texttt{\#zhigh: z2}).

The format of the file depends on its type.  Each line contains a separate data point.  For cosmic SFRs, the format is:
\begin{verbatim}
z1 z2 log10(csfr) err+ err-
\end{verbatim}
where \texttt{z1} and \texttt{z2} are the redshift range over which the measurement was performed, and \texttt{err+} and \texttt{err-} are the uncertainties in dex.

For most other files, the format is:
\begin{verbatim}
m1 m2 log10(obs) err+ err-
\end{verbatim}
where \texttt{m1} and \texttt{m2} are the luminosity or stellar mass range (in magnitudes or log$_{10}$ solar masses, respectively), and \texttt{err+} and \texttt{err-} are the errors in dex.  The exception is quenched fractions, where the observation and the errors are assumed to be linear instead of logarithmic.

Correlation functions require special treatment, so if adding/replacing existing data, please contact the authors.

\subsection{Setting Up the Config File}

It is best to start with a sample config file (e.g., \texttt{scripts/sample.cfg}) and modify it so that all desired fields are included.

\subsubsection{Simulation/Data Setup}

\begin{verbatim}
INBASE = /path/to/simulation/data
\end{verbatim}
This should be the directory where all the simulation data files (e.g., \texttt{cat.box*}) are stored.  It does not need to be writable.

\begin{verbatim}
OUTBASE = /path/to/output/directory
\end{verbatim}
This should be a directory where you would like all outputs to be saved.

\begin{verbatim}
NUM_BLOCKS = 144
\end{verbatim}
This should be the total number of blocks into which the simulation data has been divided.  That is, it should be the number of different \texttt{cat.box*} files.  Note that the indexing of the \texttt{cat.box*} files starts from zero, which means that the number of \texttt{cat.box*} files will be one more than the largest filename number (e.g., \texttt{cat.box143.dat}, in this example).

\subsubsection{Box Size/Cosmology}

\begin{verbatim}
BOX_SIZE = 250 #In Mpc/h
Om = 0.307     #Omega_matter
Ol = 0.693     #Omega_lambda
h0 = 0.68      #h0 = H0 / (100 km/s/Mpc)
fb = 0.158     #cosmic baryon fraction
\end{verbatim}
These parameters capture the box length and the cosmology.  The parameters above are appropriate for the \textit{Bolshoi-Planck} simulation; if using a different one, you may have to change these parameters appropriately.

\subsubsection{Parallel Node Setup/MCMC}

If performing fits or MCMC exploration, the UniverseMachine needs to be run on a parallel system with enough memory to store all the simulation data files at once.  For MCMC exploration, it is preferable to use as many nodes as possible.

\begin{verbatim}
NUM_NODES = 6           #Total number of nodes used
\end{verbatim}
This specifies the total number of nodes used in the calculation.

\begin{verbatim}
BLOCKS_PER_NODE = 24           #Parallel tasks per node
\end{verbatim}
This specifies how many parallel tasks are performed per node.  Often, \texttt{BLOCKS\_PER\_NODE} will be the number of cores per node.  However, it does not need to be identical, which may be a good idea if, for example, the memory per node is not sufficient.  Each node should have memory at least 1.3 x \texttt{BLOCKS\_PER\_NODE} x \texttt{B}, where \texttt{B} is the average size of a single \texttt{cat.box} input file.  

The UniverseMachine will generate an integer number of universes in parallel; in addition, each node can only work on one universe at a time.  Hence, \texttt{BLOCKS\_PER\_NODE} must divide \texttt{NUM\_BLOCKS}, and the product of \texttt{NUM\_NODES} x \texttt{BLOCKS\_PER\_NODE} must be divisible by \texttt{NUM\_BLOCKS}.  E.g., if \texttt{NUM\_BLOCKS} = 144 and each node has 24 processors, then it would be appropriate to set \texttt{BLOCKS\_PER\_NODE} = 24, and to set \texttt{NUM\_NODES} to be a multiple of 6 (because 24x6 = 144).

\subsection{Finding a Best-Fit Solution}

The UniverseMachine interfaces with \texttt{scipy} to find best-fitting solutions.  Currently, finding the best-fit solution is not parallelized beyond generating a single universe at a time, so \texttt{NUM\_NODES}  should be set to \texttt{NUM\_BLOCKS} / \texttt{BLOCKS\_PER\_NODE} in your config file.  To perform fits, the following config options should be specified:
\begin{verbatim}
FITTING_MODE=1
EXTERNAL_FITTING_MODE=1
\end{verbatim}

You should compile the UniverseMachine code by running ``\texttt{make}'' in the UniverseMachine directory.  You can then start the master fitting process by running (from the UniverseMachine directory):
\begin{verbatim}
python minimize.py /path/to/um_config.cfg > fit_log.txt
\end{verbatim}
This will generate an \texttt{auto-um.cfg} file in your \texttt{OUTBASE} directory.  You should then run the following command once on each node:
\begin{verbatim}
./box_server /path/to/OUTBASE/auto-um.cfg
\end{verbatim}
This is best accomplished using the computing system's distributed execution command, typically \texttt{srun}, \texttt{mpirun}, \texttt{ibrun}, or similar.

If successful, the best-fitting results will be saved in \texttt{fit\_log.txt}.  You can find an example script and config file in \texttt{scripts/um\_fitting.sh} and \texttt{scripts/um\_fit.cfg}.

\subsection{MCMC Analysis}

You should compile the UniverseMachine code by running ``\texttt{make}'' in the UniverseMachine directory.  You can then start the MCMC process by running (from the UniverseMachine directory):
\begin{verbatim}
./master_server /path/to/um_config.cfg > server.log 2>&1 
\end{verbatim}
This will generate an \texttt{auto-um.cfg} file in your \texttt{OUTBASE} directory.  You should then run the following command once on each node:
\begin{verbatim}
./box_server /path/to/OUTBASE/auto-um.cfg
\end{verbatim}
This is best accomplished using the computing system's distributed execution command, typically \texttt{srun}, \texttt{mpirun}, \texttt{ibrun}, or similar.  You can find an example script and config file in \texttt{scripts/um\_mcmc.sh} and \texttt{scripts/sample.cfg}.

During the fitting process, the master server will keep a log file in \texttt{OUTBASE/master\_server.log}.  You can view this to check the progress of loading the data files, burning in, and performing MCMC exploration.  Burn-in steps are saved in \texttt{OUTBASE/burn\_in\_steps.dat}, and the final MCMC steps are stored in \texttt{OUTBASE/mcmc\_steps.dat}.

By default, the MCMC algorithm uses 100 MCMC walkers to explore parameter space.  (Note that this number is independent from the number of universes generated simultaneously).  This number can be adjusted by setting \texttt{NUM\_WALKERS} to a different number in the config file.


\end{document}
