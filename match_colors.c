#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <assert.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "stringparse.h"
#include "inthash.h"

double BOX_SIZE = 250;
double h = 0.678;

int64_t print_nonmatched = 0;

#define SDSS_DATA_FIELDS 11
struct sdss_color {
  float sm, ssfr, nd, r, gr_color, ri_color, size;
  int64_t dr7id;
};

struct sdss_color *colors = NULL;
int64_t num_colors = 0;
struct inthash *concentrations = NULL;
void load_concentrations(char *filename);

int sort_cat_by_sm(const void *a, const void *b);
int sort_colors_by_sm(const void *a, const void *b);
int sort_colors_by_ssfr(const void *a, const void *b);
void match_colors(struct catalog_halo *p, int64_t num_p, int64_t cumulative, struct sdss_color *c, int64_t num_c);

int main(int argc, char **argv) {
  int64_t i, new_length;
  if (argc<5) {
    printf("Usage: %s sfr_list.bin sdss_colors.txt hlist.list orphans.list <box_size=250> <h=0.678>\n", argv[0]);
    exit(1);
  }
  
  struct catalog_halo *o = check_mmap_file(argv[1], 'r', &new_length);
  int64_t num_p = new_length / sizeof(struct catalog_halo);
  struct catalog_halo *p = NULL;
  check_realloc_s(p, sizeof(struct catalog_halo), num_p);
  memcpy(p, o, new_length);
  munmap(o, new_length);
  qsort(p, num_p, sizeof(struct catalog_halo), sort_cat_by_sm);
  
  FILE *in = check_fopen(argv[2], "r");
  char buffer[1024];
  struct sdss_color s = {0};
  SHORT_PARSETYPE;
  enum parsetype t[SDSS_DATA_FIELDS] = {K, K, K, F, F, F, F, F, F, F, D64}; //RA Dec Z SM SSFR ND R G-R R-I Size DR7id
  void *data[SDSS_DATA_FIELDS] = {NULL, NULL, NULL, &s.sm, &s.ssfr, &s.nd, &s.r, &s.gr_color, &s.ri_color, &s.size, &s.dr7id};
  while (fgets(buffer, 1024, in)) {
    if (stringparse(buffer, data, t, SDSS_DATA_FIELDS) != SDSS_DATA_FIELDS) continue;
    check_realloc_every(colors, sizeof(struct sdss_color), num_colors, 1000);
    colors[num_colors]=s;
    num_colors++;
  }
  fclose(in);
  qsort(colors, num_colors, sizeof(struct sdss_color), sort_colors_by_sm);


  concentrations = new_inthash();
  load_concentrations(argv[3]);
  load_concentrations(argv[4]);



  printf("#ID DescID UPID Flags Uparent_Dist X Y Z VX VY VZ M V MP VMP R Rank1 Rank2 RA RARank SM ICL SFR obs_SM obs_SFR SSFR SM/HM obs_UV Concentration SDSS_SM SDSS_SSFR SDSS_PetrosianR SDSS_Petrosian_GR_color SDSS_Petrosian_RI_color SDSS_size SDSS_DR7PhotoObjID\n");
  printf("#Columns:\n");
  printf("#ID: Unique halo ID\n");
  printf("#DescID: ID of descendant halo (or -1 at z=0).\n");
  printf("#UPID: -1 for central halos, otherwise, ID of largest parent halo\n");
  printf("#Flags: Ignore\n");
  printf("#Uparent_Dist: Ignore\n");
  printf("#X Y Z: halo position (comoving Mpc/h)\n");
  printf("#VX VY VZ: halo velocity (physical peculiar km/s)\n");
  printf("#M: Halo mass (Bryan & Norman 1998 virial mass, Msun)\n");
  printf("#V: Halo vmax (physical km/s)\n");
  printf("#MP: Halo peak historical mass (BN98 vir, Msun)\n");
  printf("#VMP: Halo vmax at the time when peak mass was reached.\n");
  printf("#R: Halo radius (BN98 vir, comoving kpc/h)\n");
  printf("#Rank1: halo rank in Delta_vmax (see UniverseMachine paper)\n");
  printf("#Rank2, RA, RARank: Ignore\n");
  printf("#SM: True stellar mass (Msun)\n");
  printf("#ICL: True intracluster stellar mass (Msun)\n");
  printf("#SFR: True star formation rate (Msun/yr)\n");
  printf("#Obs_SM: observed stellar mass, including random & systematic errors (Msun)\n");
  printf("#Obs_SFR: observed SFR, including random & systematic errors (Msun/yr)\n");
  printf("#SSFR: observed SSFR\n");
  printf("#Obs_UV: Observed UV Magnitude (M_1500 AB)\n");
  printf("#SMHM: SM/HM ratio\n");
  printf("#Concentration: NFW halo concentration\n");
  printf("#SDSS_SM: Stellar mass of closest SDSS galaxy [log10 Msun, Chabrier]\n");
  printf("#SDSS_SSFR: Specific star formaion rate of closest SDSS galaxy [log10 yr^-1]\n");
  printf("#SDSS_Petrosian_R: SDSS Petrosian R-band bagnitude [absolute mag]\n");
  printf("#SDSS_Petrosian_GR_color: SDSS Petrosian G-R color\n");
  printf("#SDSS_Petrosian_RI_color: SDSS Petrosian R-I color\n");
  printf("#SDSS_size: SDSS petrosian 50%% radius for R-band [kpc].\n");
  printf("#SDSS_DR7PhotoObjID: SDSS DR7 Photo Object ID for cross-matching with other properties.\n");
  printf("#ID DescID UPID Flags Uparent_Dist X Y Z VX VY VZ M V MP VMP R Rank1 Rank2 RA RARank SM ICL SFR obs_SM obs_SFR SSFR SM/HM obs_UV Concentration SDSS_SM SDSS_SSFR SDSS_PetrosianR SDSS_Petrosian_GR_color SDSS_Petrosian_RI_color SDSS_size SDSS_DR7PhotoObjID\n");
  
  for (i=0; i<num_p; ) {
    struct catalog_halo *tp = p+i;
    int64_t di = 10;
    if (tp->obs_sm < 6e11) di = 30;
    if (tp->obs_sm < 3e11) di = 100;
    if (tp->obs_sm < 1e11) di = 300;
    if (tp->obs_sm < 6e10) di = 1000;
    match_colors(tp, di, i+di, colors, num_colors);
    i += di;
  }
  return 0;
}



int sort_cat_by_sm(const void *a, const void *b) {
  const struct catalog_halo *c = a;
  const struct catalog_halo *d = b;
  if ((c->flags & IGNORE_FLAG) && !(d->flags & IGNORE_FLAG)) return 1;
  if (!(c->flags & IGNORE_FLAG) && (d->flags & IGNORE_FLAG)) return -1;
  if (c->obs_sm > d->obs_sm) return -1;
  if (c->obs_sm < d->obs_sm) return 1;
  return 0;
}

int sort_cat_by_ssfr(const void *a, const void *b) {
  const struct catalog_halo *c = a;
  const struct catalog_halo *d = b;
  if ((c->flags & IGNORE_FLAG) && !(d->flags & IGNORE_FLAG)) return 1;
  if (!(c->flags & IGNORE_FLAG) && (d->flags & IGNORE_FLAG)) return -1;
  if (c->obs_sfr*d->obs_sm > c->obs_sm*d->obs_sfr) return -1;
  if (c->obs_sfr*d->obs_sm < c->obs_sm*d->obs_sfr) return 1;
  return 0;
}

int sort_colors_by_sm(const void *a, const void *b) {
  const struct sdss_color *c = a;
  const struct sdss_color *d = b;
  if (c->sm > d->sm) return -1;
  if (c->sm < d->sm) return 1;
  return 0;
}

int sort_colors_by_ssfr(const void *a, const void *b) {
  const struct sdss_color *c = a;
  const struct sdss_color *d = b;
  if (c->ssfr > d->ssfr) return -1;
  if (c->ssfr < d->ssfr) return 1;
  return 0;
}



void load_concentrations(char *filename) {
  char buffer[2048];
  FILE *in = check_fopen(filename, "r");
  SHORT_PARSETYPE;
  int64_t id;
  float r, rs;
  struct parse_format pf[3] = {{1,D64,&id}, {11,F,&r}, {12,F,&rs}};
  while (fgets(buffer, 2048, in)) {
    if (stringparse_format(buffer, pf, 3) != 3) continue;
    double concentration = 0;
    if (rs>0) concentration = r / rs;
    ih_setdouble(concentrations, id, concentration);
    //double c2 = ih_getdouble(concentrations, id);
    //printf("%"PRId64" %lf %lf\n", id, concentration, c2);
  }
  fclose(in);
}

void match_colors(struct catalog_halo *p, int64_t num_p, int64_t cumulative, struct sdss_color *c, int64_t num_c) {
  double volume = pow(BOX_SIZE/h, 3);
  int64_t i, j, c_min=-1, c_max=-1;
  double total_c = 0;
  struct sdss_color cnull = {0}, *tc;
  int64_t no_info = 0;
  for (i=0; i<num_c; i++) {
    total_c += c[i].nd*volume;
    if (total_c > cumulative-num_p+0.5 && c_min < 0) c_min = i;
    if (total_c > cumulative) {
      c_max = i+1;
      break;
    }
  }

  if (i==num_c) {
    no_info = 1;
    tc = &cnull;
    if (!print_nonmatched) return;
  } else {
    qsort(c+c_min, c_max-c_min, sizeof(struct sdss_color), sort_colors_by_ssfr);
    qsort(p, num_p, sizeof(struct catalog_halo), sort_cat_by_ssfr);
    tc = c+c_min;
  }
  total_c = 0;
  j = 0;
  for (i=0; i<num_p; i++) {
    struct catalog_halo *tp = p+i;
    float ssfr = 0;
    if (tp->sm > 0)
      ssfr = tp->obs_sfr / tp->obs_sm;
    float smhm = tp->sm*0.68 / tp->mp;
    if ((tp->flags & IGNORE_FLAG)) break;
    float concentration = ih_getdouble(concentrations, tp->id);
    printf("%"PRId64" %"PRId64" %"PRId64" %"PRId32" %f %f %f %f %f %f %f %.3e %f %.3e %f %f %f %f %f %f %.3e %.3e %.3e %.3e %.3e %.3e %.3e %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3e %"PRId64"\n",
	   tp->id, tp->descid, tp->upid, tp->flags, tp->uparent_dist, tp->pos[0], tp->pos[1], tp->pos[2], tp->pos[3], tp->pos[4], tp->pos[5], tp->m/0.68, tp->v, tp->mp/0.68, tp->vmp, tp->r, tp->rank1, tp->rank2, tp->ra, tp->rarank, tp->sm, tp->icl, tp->sfr, tp->obs_sm, tp->obs_sfr, ssfr, smhm, tp->obs_uv, concentration, tc->sm, tc->ssfr, tc->r, tc->gr_color, tc->ri_color, tc->size, tc->dr7id);
    if (!no_info) {
      total_c++;
      while (total_c > tc->nd*volume) { total_c -= tc->nd*volume; if (c_min+j==num_c-1) { no_info=1; tc = &cnull; break; } j++; tc = c+c_min+j; }
    }
  }
  if (!no_info) qsort(c+c_min, c_max-c_min, sizeof(struct sdss_color), sort_colors_by_sm);
}
