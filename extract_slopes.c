#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <sys/stat.h>
#include <stdarg.h>
#include "mt_rand.h"
#include "universe_time.h"
#include "distance.h"
#include "check_syscalls.h"
#include "mcmc_data.h"
#include "config.h"
#include "config_vars.h"
#include "version.h"
#include "universal_constants.h"
#include "sf_model.h"


int sort_floats(const void *a, const void *b) {
  const float *c = a;
  const float *d = b;
  if (*c<*d) return -1;
  if (*c>*d) return 1;
  return 0;
}

double uv_to_sfr(double uv) {
  return 0.16*pow(10,((-17.0-uv)/2.5));
}

double integrate_power(double nd0, double l0, double lum_range, double slope) {
  double tot = 0;
  int64_t i;
  for (i=1; i<(lum_range*20); i++) {
    double mag = l0 + (double)i/(20.0);
    double sfr = uv_to_sfr(mag);
    double nd = nd0 * pow(10, (-1.0*(slope+1.0)*(mag-l0)/2.5));
    tot += sfr*nd/20.0;
  }
  return tot;
}

int main(int argc, char **argv) {
  int64_t i, j;
  char buffer[1024];
  if (argc < 3) {
    fprintf(stderr, "%s", INFO_STRING);
    fprintf(stderr, "Usage: %s um.cfg  /paths/to/model_outputs ...\n", argv[0]);
    exit(EXIT_FAILURE);
  }  
  
  do_config(argv[1]);
  if (!POSTPROCESSING_MODE) {
    fprintf(stderr, "[Error] POSTPROCESSING_MODE must be enabled to extract smhm fits.\n");
    exit(EXIT_FAILURE);
  }

  struct mcmc_data *m = load_mcmc_data(argv[2]);

  snprintf(buffer, 1024, "%s/post_slopes.dat", OUTBASE);
  FILE *out = check_fopen(buffer, "w");
  snprintf(buffer, 1024, "%s/post_slopes_2sigma.dat", OUTBASE);
  FILE *out2 = check_fopen(buffer, "w");
  
  float *uv_slopes = NULL;
  float *tot_ratio_10 = NULL;
  float *tot_ratio_12 = NULL;
  float *tot_ratio_14 = NULL;
  float *sm_slopes = NULL;
  int64_t num_files = argc-2;

  for (i=2; i<argc; i++) {
    struct mcmc_data *m = load_mcmc_data(argv[i]);
    if (!uv_slopes) {
      check_calloc_s(uv_slopes, sizeof(float), m->num_scales*num_files);
      check_calloc_s(tot_ratio_10, sizeof(float), m->num_scales*num_files);
      check_calloc_s(tot_ratio_12, sizeof(float), m->num_scales*num_files);
      check_calloc_s(tot_ratio_14, sizeof(float), m->num_scales*num_files);
      check_calloc_s(sm_slopes, sizeof(float), m->num_scales*num_files);
    }

    for (j=0; j<m->num_scales; j++) {
      int64_t bin = j*num_files+i;
      int64_t uv_bin = j*UV_NBINS+(-17.55-UV_MIN)*UV_BPMAG;
      int64_t sm_bin = j*SM_NBINS+(7-SM_MIN)*SM_BPDEX;
      if (m->uvlf[uv_bin] && m->uvlf[uv_bin+UV_BPMAG/2]) {
	uv_slopes[bin] = -2.0*log10(m->uvlf[uv_bin+UV_BPMAG/2]/m->uvlf[uv_bin])*2.5-1.0;
	double conv_factor = UV_BPMAG;
	//double nd0, double l0, double lum_range, double slope) {
	double tot10 = integrate_power(m->uvlf[uv_bin+UV_BPMAG/2]*conv_factor, -17, 7, uv_slopes[bin]);
	double tot12 = integrate_power(m->uvlf[uv_bin+UV_BPMAG/2]*conv_factor, -17, 5, uv_slopes[bin]);
	double tot14 = integrate_power(m->uvlf[uv_bin+UV_BPMAG/2]*conv_factor, -17, 3, uv_slopes[bin]);
	tot_ratio_10[bin] = (tot10+m->csfr_uv[j])/(m->csfr[j]);
	tot_ratio_12[bin] = (tot12+m->csfr_uv[j])/(m->csfr[j]);
	tot_ratio_14[bin] = (tot14+m->csfr_uv[j])/(m->csfr[j]);
      }
      if (m->smf[sm_bin] && m->smf[sm_bin+SM_BPDEX/2])
	sm_slopes[bin] = 2.0*log10(m->smf[sm_bin+SM_BPDEX/2]/m->smf[sm_bin])-1.0; 
    }
  }

  fprintf(out, "#Scale UV_Bestfit_Slope UV_Median_Slope Err+ Err- SM_Bestfit_Slope SM_Median_Slope Err+ Err- UV-10/Tot_CSFR(Bestfit) Median Err+ Err- UV-12/Tot_CSFR(Bestfit) Median Err+ Err- UV-14/Tot_CSFR(Bestfit) Median Err+ Err-\n");
  fprintf(out2, "#Scale UV_Bestfit_Slope UV_Median_Slope Err+(2sig) Err-(2sig) SM_Bestfit_Slope SM_Median_Slope Err+(2sig) Err-(2sig) UV-10/Tot_CSFR(Bestfit) Median Err+(2sig) Err-(2sig) UV-12/Tot_CSFR(Bestfit) Median Err+(2sig) Err-(2sig) UV-14/Tot_CSFR(Bestfit) Median Err+(2sig) Err-(2sig)\n");
  for (j=0; j<m->num_scales; j++) {
    int64_t bin = j*num_files;
    int64_t med = bin+0.5*num_files;
    int64_t low = bin+0.5*(1.0-ONE_SIGMA)*num_files;
    int64_t high = bin+0.5*(1.0+ONE_SIGMA)*num_files;
    int64_t low2 = bin+0.5*(1.0-TWO_SIGMA)*num_files;
    int64_t high2 = bin+0.5*(1.0+TWO_SIGMA)*num_files;
    float uv_bestfit = uv_slopes[bin];
    float sm_bestfit = sm_slopes[bin];
    float tot10_bestfit = tot_ratio_10[bin];
    float tot12_bestfit = tot_ratio_12[bin];
    float tot14_bestfit = tot_ratio_14[bin];
    qsort(uv_slopes+bin, num_files, sizeof(float), sort_floats);
    qsort(sm_slopes+bin, num_files, sizeof(float), sort_floats);
    qsort(tot_ratio_10+bin, num_files, sizeof(float), sort_floats);
    qsort(tot_ratio_12+bin, num_files, sizeof(float), sort_floats);
    qsort(tot_ratio_14+bin, num_files, sizeof(float), sort_floats);
    fprintf(out, "%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n", m->scales[j],
	    uv_bestfit, uv_slopes[med], uv_slopes[high]-uv_slopes[med], uv_slopes[med]-uv_slopes[low],
	    sm_bestfit, sm_slopes[med], sm_slopes[high]-sm_slopes[med], sm_slopes[med]-sm_slopes[low],
	    tot10_bestfit, tot_ratio_10[med], tot_ratio_10[high]-tot_ratio_10[med], tot_ratio_10[med]-tot_ratio_10[low],
	    tot12_bestfit, tot_ratio_12[med], tot_ratio_12[high]-tot_ratio_12[med], tot_ratio_12[med]-tot_ratio_12[low],
	    tot14_bestfit, tot_ratio_14[med], tot_ratio_14[high]-tot_ratio_14[med], tot_ratio_14[med]-tot_ratio_14[low]);	   
    fprintf(out2, "%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n", m->scales[j],
	    uv_bestfit, uv_slopes[med], uv_slopes[high2]-uv_slopes[med], uv_slopes[med]-uv_slopes[low2],
	    sm_bestfit, sm_slopes[med], sm_slopes[high2]-sm_slopes[med], sm_slopes[med]-sm_slopes[low2],
	    tot10_bestfit, tot_ratio_10[med], tot_ratio_10[high2]-tot_ratio_10[med], tot_ratio_10[med]-tot_ratio_10[low2],
	    tot12_bestfit, tot_ratio_12[med], tot_ratio_12[high2]-tot_ratio_12[med], tot_ratio_12[med]-tot_ratio_12[low2],
	    tot14_bestfit, tot_ratio_14[med], tot_ratio_14[high2]-tot_ratio_14[med], tot_ratio_14[med]-tot_ratio_14[low2]);	   
  }

  fclose(out);
  fclose(out2);
  return 0;
}
