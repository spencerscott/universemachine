#include <stdio.h>
#include <stdlib.h>
#include "sf_model.h"
#include "io_helpers.h"
#include "check_syscalls.h"

int main(int argc, char **argv) {
  FILE *in;
  if (argc < 2) in = stdin;
  else in = check_fopen(argv[1], "r");

  char buffer[2048];
  struct sf_model_allz m;
  while (fgets(buffer, 2048, in)) {
    read_params(buffer, m.params, NUM_PARAMS);
    int64_t n = test_invalid(m);
    if (n)
      printf("%"PRId64" %s", n, buffer);
  }
  return 0;
}
