#include <stdio.h>
#include <stdlib.h>
#include "../inet/pload.h"
#include "../check_syscalls.h"

int main(void) {
  char *data = NULL;
  FILE *in = check_fopen("out", "r");
  clear_ploads(".");
  check_realloc_s(data, 1024*1024, sizeof(char));
  check_fread(data, 1024, 1024, in);
  fclose(in);
  printf("loaded file.\n");
  data = pload("out", 0, 100, data, 1024*1024, NULL);
  printf("sent file %d.\n", data[1000000]);
  return 0;
}
