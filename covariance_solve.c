#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "mt_rand.h"
#include "linalg.h"
#include "jacobi_dim.h"
#include "io_helpers.h"

struct sf_model_allz *prev_points = NULL;
int64_t npp = 0;

int sort_by_chi2(const void *a, const void *b) {
  const struct sf_model_allz *c = a;
  const struct sf_model_allz *d = b;
  if (CHI2(*c)<CHI2(*d)) return -1;
  if (CHI2(*c)>CHI2(*d)) return 1;
  return 0;
}

int solve_for_covariance(char *filename, double *orth_matrix, double *eigenvalues) {
  char buffer[2048];
  int64_t total_read = 0;
  int64_t i = 0, j=0, k=0;
  
  FILE *in = check_fopen(filename, "r");
  struct sf_model_allz point = {{0}};
  while (fgets(buffer, 2048, in)) {
    int64_t np = 0;
    np = read_params(buffer, point.params, NUM_PARAMS+2);
    if (np < (NUM_PARAMS+1)) break;
    if (np==(NUM_PARAMS+1)) {
      CHI2(point) = point.params[NUM_PARAMS];
      INVALID(point) = 0;
    }
    check_realloc_every(prev_points, sizeof(struct sf_model_allz), npp, 1000);
    prev_points[npp] = point;
    npp++;
  }
  fclose(in);
  qsort(prev_points, npp, sizeof(struct sf_model_allz), sort_by_chi2);

#undef NUM_PARAMS
#define NUM_PARAMS 2
  
  double *chi2_matrix = NULL;
  double *answers = NULL;
  int64_t tot = (NUM_PARAMS*(NUM_PARAMS+1))/2;
  check_calloc_s(chi2_matrix, sizeof(double), tot*(tot+1));
  check_calloc_s(answers, sizeof(double), tot);
  for (i=0; i<tot; i++) {
    int64_t index = dr250()*(0.68*npp)+0.16*npp;
    if (CHI2(prev_points[index])==CHI2(prev_points[index-1]) || INVALID(prev_points[index])) {i--; continue; }
    INVALID(prev_points[index]) = 1;
     for (j=0; j<NUM_PARAMS; j++)
      prev_points[index].params[j] -= prev_points[0].params[j];
    int64_t mat_elem = 0;
    for (j=0; j<NUM_PARAMS; j++) {
      for (k=j; k<NUM_PARAMS; k++) {
	chi2_matrix[i*(tot+1)+mat_elem] = prev_points[index].params[j]*prev_points[index].params[k];
	if (k!=j) chi2_matrix[i*(tot+1)+mat_elem]*=2.0;
	mat_elem++;
      }
    }
    chi2_matrix[i*(tot+1)+tot] = CHI2(prev_points[index]) - CHI2(prev_points[0]);
    //printf("%.12g\n", CHI2(prev_points[index]));
  }

  for (i=0; i<tot; i++) chi2_matrix[i*(tot+1)+i] += 1e-9; //Make non-singular even when full parameter space is not explored.

  for (j=0; j<tot; j++) {
    for (k=0; k<tot+1; k++) {
      printf("%g ", chi2_matrix[j*(tot+1)+k]);
    }
    printf("\n");
  }
  
  int64_t res = solve_lineq(tot, chi2_matrix, answers);
  free(chi2_matrix);
  if (!res) { free(answers); return 0; }  //Couldn't solve... :(

  double *cov_matrix = NULL;
  check_calloc_s(cov_matrix, sizeof(double), NUM_PARAMS*NUM_PARAMS);
  int64_t mat_elem = 0;
  /*  for (i=0; i<tot; i++) {
    printf("Answers[%"PRId64"]: %g\n", i, answers[i]);
    }*/
  for (j=0; j<NUM_PARAMS; j++) {
    for (k=j; k<NUM_PARAMS; k++) {
      cov_matrix[k*NUM_PARAMS+j] =
	cov_matrix[j*NUM_PARAMS+k] = answers[mat_elem];
      //printf("In[%"PRId64"][%"PRId64"]: %g\n", j, k, answers[mat_elem]);
      mat_elem++;
    }
  }
    
  jacobi_decompose_dim(cov_matrix, eigenvalues, orth_matrix, NUM_PARAMS);

  for (j=0; j<NUM_PARAMS; j++) {
    printf("Eig[%"PRId64"]: %g\n", j, eigenvalues[j]);
  }
  for (i=0; i<NUM_PARAMS; i++) {
    if (eigenvalues[i]>0) eigenvalues[i] = 1.0/eigenvalues[i];
  }

  free(answers);
  free(cov_matrix);

  return 1;
}


#include "sampler.h"
int main(int argc, char **argv) {
  if (argc < 2) {
    fprintf(stderr, "Usage: %s mcmc_steps.dat\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  r250_init(87L);
  struct EnsembleSampler *e = NULL;
  e = new_sampler(684, NUM_PARAMS, 1.4);
  e->mode = 1;
  sampler_init_averages(e);
  int64_t res = solve_for_covariance(argv[1], e->orth_matrix, e->eigenvalues);
  printf("Solving result: %"PRId64"\n", res);
  
  /*  int64_t i;
  FILE *out = check_fopen("random_steps.dat", "w");
  struct Walker w = {0};
  check_calloc_s(w.params, sizeof(double), (e->dim+1)*2);
  for (i=0; i<100000; i++) {
    sampler_random_step(e, &w);
    for (int64_t j=0; j<e->dim; j++)
      fprintf(out, "%.12e ", w.params[j]);
    fprintf(out, "\n");
  }
  free(w.params);
  fclose(out);*/
  return 0;
}
