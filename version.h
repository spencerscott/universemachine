#ifndef _VERSION_H_
#define _VERSION_H_

#define UMACHINE_VERSION "1.0"
#define UMACHINE_COPYRIGHT "Copyright (C) 2015-2019, Peter Behroozi; license: GPLv3 as specified in LICENSE file.\n"
#define MTRAND_COPYRIGHT "Exception: Random Number Generator Copyright (C) 2004, Makoto Matsumoto and Takuji Nishimura; license specified in mt_rand.c.\n"

#define INFO_STRING "UniverseMachine, v" UMACHINE_VERSION "\n" UMACHINE_COPYRIGHT MTRAND_COPYRIGHT

#endif /* _VERSION_H_ */
