#ifndef _MAKE_SF_CATALOG_H_
#define _MAKE_SF_CATALOG_H_
#include <inttypes.h>
#include "sf_model.h"
#include "bin_definitions.h"

#ifdef SF_CATALOG_LIB
#define NO_CATALOG_MAIN
#endif /*SF_CATALOG_LIB*/

#define HALO_NUM_RANDS 4

#include "halo.h"

struct shmem_struct {
  float **data;
  int64_t length;
};

void write_colors(int64_t n);

void load_box(int64_t chunk_num);
void translate_descids(void);
void calc_extra_info(void);
void gen_erfcache(void);
void calc_sm_histories(int64_t n);
float rank_to_sfr(float rank, float sfr_q, float sig_q, float sfr_sf, float sig_sf, float fq);
float cached_rank(float x);
void write_catalog(int64_t n);
void calc_sf_model_catalog(struct sf_model_allz f, int64_t n);
void clear_observations(void);
void calculate_model(struct sf_model_allz model);
void sf_catalog_client(char *hostname, char *port, int64_t chunk_num);
void allocate_shared_memory(int64_t server);
void calc_ssfr_threshes(int64_t n);
double sfr_at_vmp(double lv, struct sf_model c);
void gen_and_free_luminosities(void);
void load_offsets_and_scales(int64_t chunk_num);
void shared_data_to_file(struct sf_model_allz m, FILE *out);
float fraction_above_thresh(float x, float thresh, float scatter);
float fraction_below_thresh(float x, float thresh, float scatter);
void smooth_nds(int64_t n);

extern int64_t box_id;

extern float *shared_data;
extern int64_t num_scales, shared_data_length, *galaxy_offsets, *offsets;
extern float *scales, *bounds, *max_bounds;
extern float log_quenched_ssfr;
extern struct catalog_galaxy *galaxies;
#define shared(a,b) extern float *a;
#include "shared.template.h"
#include "pshared.template.h"
#undef shared

#endif /* _MAKE_SF_CATALOG_H_ */
