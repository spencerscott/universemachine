#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <inttypes.h>
#include "bin_definitions.h"

double max_dist = 0, min_dist=0;
float corr_dists[NUM_BINS+1];

double max_conf_dist = 0, min_conf_dist=0;
float conf_dists[NUM_CONF_BINS+1];

extern int64_t dens_bin_edge(int64_t b);
extern int64_t dens_bin_of(int64_t nn);
extern int64_t corr_bin_of(float dist);
extern int64_t conf_bin_of(float dist);

void init_dists(void) {
  int64_t i;
  if (max_dist) return;
  max_dist = pow(10, 2.0*MAX_DIST);
  min_dist = pow(10, 2.0*MIN_DIST);
  for (i=0; i<NUM_BINS+1; i++)
    corr_dists[i] = pow(10, 2.0*(MIN_DIST+(double)i/(double)DIST_BPDEX));
}

void init_conf_dists(void) {
  int64_t i;
  for (i=0; i<NUM_CONF_BINS+1; i++)
    conf_dists[i] = pow(10, 2.0*(MIN_CONF_DIST+(double)i/(double)DIST_CONF_BPDEX));
}

float moustakas_translate_const(float scale) {
  float z = 1.0/scale - 1.0;
  return -0.49 + 1.07*(z-0.1);
}

