#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include "../sampler.h"
#include "../mt_rand.h"

#define NDIM 3

#define ASPECT 0.01
#define R 0.5

double chi2(double *pos) {
  int64_t i;
  double pos2[NDIM];
  memcpy(pos2, pos, NDIM*sizeof(double));
  //double x = R*pos2[0] + R*pos2[1];
  //double y = R*pos2[0] - R*pos2[1];
  //y /= ASPECT;
  //pos2[0] = x;
  //pos2[1] = y;
  double s=0;
  s += (100.0*(pow(pos[1]-pos[0]*pos[0], 2)) + 10.0*(pow(pos[2]-pos[0]*pos[0]*pos[0], 2)) + pow(1-pos[0], 2))/10.0;
  //  for (i=2; i<NDIM; i++) s += pos2[i]*pos2[i];
  return s;
}



int main(void) {
  int64_t i, j;
  struct EnsembleSampler *e = new_sampler(100, NDIM, 2.0);
  for (i=0; i<e->num_walkers; i++) {
    double pos[NDIM];
    for (j=0; j<NDIM; j++) pos[j] = dr250()*0.01;
    //printf("%f %f %f\n", pos[0], pos[1], pos[2]);
    init_walker_position(e->w+i, pos);
  }
  for (i=0; i<100000; i++) {
    struct Walker *w = get_next_walker(e);
    update_walker(e, w, chi2(w->params));
    if (ensemble_ready(e)) {
      update_ensemble(e);
      write_accepted_steps_to_file(e, stdout, 0);
    }
  }  
  fprintf(stderr, "Acceptance fraction: %f\n", acceptance_fraction(e));
  return 0;
}
